﻿using UnityEngine;
using System.Collections;

public class LoadHeightSelectionScene : MonoBehaviour {

	public C_IdleTimeReseter c_IdleTimeReseter;
	public C_ModularProgram_UI c_ModularProgram_UI;

	//TestingPurpose

	// Use this for initialization
	void OnEnable () {
		StartCoroutine (StartLoading());
	}

	IEnumerator StartLoading(){
		yield return new WaitForSeconds (0.05f);
		c_ModularProgram_UI.f_ActivateModularPages (1);
		c_IdleTimeReseter.f_ResetIdleTime ();

	}

}
