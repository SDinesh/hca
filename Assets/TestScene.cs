﻿using UnityEngine;
using System.Collections;

public class TestScene : MonoBehaviour {

	public C_IdleTimeReseter c_IdleTimeReseter;
	public C_DataBase_Manager c_DataBase_Manager;
	public C_ModularProgram_UI c_ModularProgram_UI;

	//TestingPurpose

	// Use this for initialization
	void OnEnable () {
		c_ModularProgram_UI.f_ActivateModularPages (0);
		c_DataBase_Manager.f_GetData (547);
		c_IdleTimeReseter.f_ResetIdleTime ();

	}

}
