﻿using UnityEngine;
using System.Collections;

public class C_CubeRotation : MonoBehaviour {

	public GameObject g_Target;
	
	// Update is called once per frame
	void Update () {
	
		transform.RotateAround (g_Target.transform.position, Vector3.up, 0.5f);
	}
}
