﻿using UnityEngine;
using System.Collections;

public class C_MainMenuManager : MonoBehaviour {


	public GameObject g_MixAndMatchDiningProgramBtn;
	public GameObject g_OffersButton;
	public GameObject g_SignatureSofaBtn;
	public GameObject g_SleepProgramBtn;

	public C_UI_ButtonHandler c_UI_ButtonHandler;
	public C_ModularProgram_UI c_ModularProgram_UI;

	void Start(){
	}


	void OnEnable () 
	{

		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Entrance) 
		{
			g_MixAndMatchDiningProgramBtn.SetActive (false);
			g_SignatureSofaBtn.SetActive (false);
			g_SleepProgramBtn.SetActive (false);
			g_OffersButton.SetActive (true);
		} 
		else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) 
		{
			g_MixAndMatchDiningProgramBtn.SetActive (true);
			g_SignatureSofaBtn.SetActive (false);
			g_SleepProgramBtn.SetActive (false);
			g_OffersButton.SetActive (false);
		}else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) 
		{
			g_MixAndMatchDiningProgramBtn.SetActive (false);
			g_SignatureSofaBtn.SetActive (true);
			g_SleepProgramBtn.SetActive (false);
			g_OffersButton.SetActive (false);
		}
		else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) 
		{
			g_MixAndMatchDiningProgramBtn.SetActive (false);
			g_SignatureSofaBtn.SetActive (false);
			g_SleepProgramBtn.SetActive (true);
			g_OffersButton.SetActive (false);
		}
		LoadDirectlyNextScreen ();
	}
	
	void LoadDirectlyNextScreen(){
		c_UI_ButtonHandler.f_OnServicesButtonClick (3);
		c_ModularProgram_UI.f_ModularProgramBtn ();
	}

}
