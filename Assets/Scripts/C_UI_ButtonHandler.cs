﻿using UnityEngine;
using System.Collections;

public class C_UI_ButtonHandler : MonoBehaviour 
{
	private static C_UI_ButtonHandler instance;
	public static C_UI_ButtonHandler Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_UI_ButtonHandler> ();

			return instance;
		}
	}

	public GameObject g_SelectProductPanel;
	public GameObject g_DiningPanel;
	public GameObject g_LivingPanel;
	public GameObject g_SleepingPanel;
	public GameObject g_AssetPreviewUI;
	public GameObject g_AssetPreviewManger;
	public GameObject g_LoadingPanel;

	public C_WayFinderManager g_WayFinderManager;

	void Start(){
//		f_OnSelectAreaButtonClick (4);
	}


	public void f_OnScreenSaverClick()
	{
		C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		C_ApplicationManager.instance.f_ManageApplicationPage (E_ApplicationPage.LanguageSelect);
	}

	public void f_SelectLanguage(int l_LanguageCode)
	{
		C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		C_ApplicationManager.instance.f_ManageApplicationPage (E_ApplicationPage.MainMenuPage);
	}

	public void f_OnMainUICloseButtonClick()
	{
		C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		if (g_WayFinderManager.g_IsNavigationActive) {
			return;
		}
		C_ApplicationManager.instance.f_SetActiveServices (0);
		//C_ModularProgram_UI.Instance.Youtube.SetActive (false);
		C_ApplicationManager.instance.f_ManageApplicationPage (E_ApplicationPage.MainMenuPage);
	}

	public void f_OnAssetPreviewBackButtonClick()
	{
		g_DiningPanel.SetActive(false);
		g_LivingPanel.SetActive(false);
		g_SleepingPanel.SetActive(false);
		g_AssetPreviewUI.SetActive(false);
		g_AssetPreviewManger.SetActive(false);

		g_SelectProductPanel.SetActive (true);
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) 
		{
			//g_DiningPanel.SetActive(true);
			g_SelectProductPanel.SetActive (false);
			C_ModularProgram_UI.Instance.g_ModularPanel.SetActive (true);
			C_ModularProgram_UI.Instance.g_DiningPanel_Modular.SetActive (true);
		}

		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) 
		{
			g_SelectProductPanel.SetActive (false);
			C_ModularProgram_UI.Instance.g_ModularPanel.SetActive (true);
			C_ModularProgram_UI.Instance.g_SigCombo_2.SetActive (true);
		}

		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) 
		{
			g_SleepingPanel.SetActive(true);
		}
			
	}

	public void f_OnServicesButtonClick(int l_ServiceID)
	{
		C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		if (g_WayFinderManager.g_IsNavigationActive) {
			return;
		}
		C_ApplicationManager.instance.f_ManageApplicationPage (E_ApplicationPage.MainPageUI);
		C_ApplicationManager.instance.f_SetActiveServices (l_ServiceID);
		if (l_ServiceID == 2) {
			System.Diagnostics.Process.Start("C:\\Configs\\Exe\\Hstart-Catalogue.exe");
			f_OnMainUICloseButtonClick ();
			g_LoadingPanel.SetActive (true);
			StartCoroutine ("E_LoadingPanel");
		}
		else if (l_ServiceID == 6) {
			//System.Diagnostics.Process.Start("C:\\Program Files (x86)\\KioWare Client Platform\\Homecentre\\KioWare Client.exe");
			System.Diagnostics.Process.Start("C:Configs\\Exe\\Hstart-Homecentre.exe");
			f_OnMainUICloseButtonClick ();
			g_LoadingPanel.SetActive (true);
			StartCoroutine ("E_LoadingPanel");
		}
		else if (l_ServiceID == 5) {
			//System.Diagnostics.Process.Start("C:\\Program Files (x86)\\KioWare Client Platform\\Homecentre\\KioWare Client.exe");
			System.Diagnostics.Process.Start("C:Configs\\Exe\\Hstart-Shukran.exe");
			f_OnMainUICloseButtonClick ();
			g_LoadingPanel.SetActive (true);
			StartCoroutine ("E_LoadingPanel");
		}


	}

	public void f_OnSocialMediaButtonClick(int l_ID)
	{
		C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		C_ApplicationManager.instance.f_ManageApplicationPage (E_ApplicationPage.MainPageUI);

		switch (l_ID) 
		{
		case 1:
			//C_ApplicationManager.instance.f_InitSocialMedia (E_SocialMedia.Facebook);
			System.Diagnostics.Process.Start ("C:Configs\\Exe\\Hstart-Facebook.exe");
			f_OnServicesButtonClick (7);
			g_LoadingPanel.SetActive (true);
			StartCoroutine ("E_LoadingPanel");
			break;

		case 2:
			//C_ApplicationManager.instance.f_InitSocialMedia (E_SocialMedia.Twitter);
			System.Diagnostics.Process.Start("C:Configs\\Exe\\Hstart-Twitter.exe");
			f_OnServicesButtonClick (7);
			g_LoadingPanel.SetActive (true);
			StartCoroutine ("E_LoadingPanel");
			break;

		case 3:
			//C_ApplicationManager.instance.f_InitSocialMedia (E_SocialMedia.Instagram);
			System.Diagnostics.Process.Start("C:Configs\\Exe\\Hstart-Instagram.exe");
			f_OnServicesButtonClick (7);
			g_LoadingPanel.SetActive (true);
			StartCoroutine ("E_LoadingPanel");
			break;

		case 4:
			//C_ApplicationManager.instance.f_InitSocialMedia (E_SocialMedia.Youtube);
			System.Diagnostics.Process.Start("C:Configs\\Exe\\Hstart-Youtube.exe");
			f_OnServicesButtonClick (7);
			g_LoadingPanel.SetActive (true);
			StartCoroutine ("E_LoadingPanel");
			break;
		}

	}


	public void f_OnSelectAreaButtonClick(int l_ID)
	{
		C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		switch (l_ID) 
		{
		case 1:
			C_GlobalVariables.instance.g_SelectedArea = E_Area.Entrance; 
			break;

		case 2:
			C_GlobalVariables.instance.g_SelectedArea = E_Area.Dining;
			break;

		case 3:
			C_GlobalVariables.instance.g_SelectedArea = E_Area.Living;
			break;

		case 4:
			C_GlobalVariables.instance.g_SelectedArea = E_Area.Sleeping;
			break;
		}

		C_ApplicationManager.instance.g_IsSplashScreenActive = true;
		C_ApplicationManager.instance.f_ManageApplicationPage (E_ApplicationPage.SplashScreen);
		C_ApplicationManager.instance.g_SplashScreenLifeSpan = Time.time + 3;

	}

	IEnumerator E_LoadingPanel(){
		yield return new WaitForSeconds (3.0f);
		g_LoadingPanel.SetActive (false);
	}

}
