﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using System.IO;
using Excel;
using System.Data;
using ArabicSupport;
using UnityEngine.UI;

public class C_ExcelParser : MonoBehaviour {

	private static C_ExcelParser _instance;
	public static C_ExcelParser instance
	{
		get
		{
			if(_instance == null)
			{
				GameObject l_GameManagerObj = GameObject.Find("ExcelParser");
				_instance = l_GameManagerObj.GetComponent<C_ExcelParser>();
			}
			return _instance;
		}
	}
	//----------------------------------------------------------------------------

	public C_WayFinderManager g_WayFinderManager;
	// Use this for initialization

	FileStream g_WayfinderExcelSheetAddress;
	HSSFWorkbook g_Workbook;
	ISheet g_Sheet;
	IRow g_Row;


	string[,] g_WayFindingExcelData;
	int g_WayFindingExcelDataRowCount;
	//public Text Devlog;
	void Start ()
	{
		g_WayfinderExcelSheetAddress = new FileStream(Application.streamingAssetsPath + "/ExcelSheets/Wayfinding_Hierarchy.xls", FileMode.Open, FileAccess.Read, FileShare.None);

		g_WayFindingExcelData = new string[1000, 4];
		g_WayFindingExcelDataRowCount = 0;

		f_RetrieveWayfinderExcelData ();

	}

	void f_RetrieveWayfinderExcelData()
	{
		

		using (g_WayfinderExcelSheetAddress)
		{
			g_Workbook = new HSSFWorkbook(g_WayfinderExcelSheetAddress);
		}

		g_Sheet = g_Workbook.GetSheet("Living");
		for (int row = 0; row <= g_Sheet.LastRowNum; row++)
		{
			g_Row = g_Sheet.GetRow(row);
			for (int cells = 0; cells < 4; cells++)
			{
				g_WayFindingExcelData [g_WayFindingExcelDataRowCount, cells] = g_Row.GetCell (cells).ToString ().ToLower();
			}
			g_WayFindingExcelDataRowCount++;
		}

		g_Sheet = g_Workbook.GetSheet("Dining");
		for (int row = 0; row <= g_Sheet.LastRowNum; row++)
		{
			g_Row = g_Sheet.GetRow(row);
			for (int cells = 0; cells < 4; cells++)
			{
				g_WayFindingExcelData [g_WayFindingExcelDataRowCount, cells] = g_Row.GetCell (cells).ToString ().ToLower();
			}
			g_WayFindingExcelDataRowCount++;
		}

		g_Sheet = g_Workbook.GetSheet("Sleeping");
		for (int row = 0; row <= g_Sheet.LastRowNum; row++)
		{
			g_Row = g_Sheet.GetRow(row);
			for (int cells = 0; cells < 4; cells++)
			{
				g_WayFindingExcelData [g_WayFindingExcelDataRowCount, cells] = g_Row.GetCell (cells).ToString ().ToLower();
			}
			g_WayFindingExcelDataRowCount++;
		}

		g_Sheet = g_Workbook.GetSheet("Decorating");
		for (int row = 0; row < g_Sheet.LastRowNum; row++)
		{
			g_Row = g_Sheet.GetRow(row);
			for (int cells = 0; cells < 4; cells++)
			{
				g_WayFindingExcelData [g_WayFindingExcelDataRowCount, cells] = g_Row.GetCell (cells).ToString ().ToLower();

			}
			g_WayFindingExcelDataRowCount++;
		}

		g_Sheet = g_Workbook.GetSheet("Playing");
		for (int row = 0; row <= g_Sheet.LastRowNum; row++)
		{
			g_Row = g_Sheet.GetRow(row);
			for (int cells = 0; cells < 4; cells++)
			{
				g_WayFindingExcelData [g_WayFindingExcelDataRowCount, cells] = g_Row.GetCell (cells).ToString ().ToLower();
				//Debug.Log (g_Row.GetCell (cells).ToString ().ToLower ());
			}
			g_WayFindingExcelDataRowCount++;
		}



		/*
		for (int row = 0; row <= g_WayFindingExcelDataRowCount; row++)
		{

			for (int cells = 0; cells < 4; cells++)
			{
				print (g_WayFindingExcelData [row, cells]);

			}

		}*/

	}


	public void f_SearchWayfinderExcelData(string l_String)
	{
		if (l_String == " " || l_String == "") {
			return;}

		string[] l_StringInput = l_String.Split (' ');
		int l_WordCount = l_StringInput.Length;

		bool l_OneWordFound = false;
		bool l_TwoWordFound = false;
		bool l_ThreeWordFound = false;

		string l_OneWordFoundInArea = "";
		string l_TwoWordFoundInArea = "";
		string l_ThreeWordFoundInArea = "";

		string l_OneWordFoundInSubArea = "";
		string l_TwoWordFoundInSubArea = "";
		string l_ThreeWordFoundInSubArea = "";

		/*for(int i=0;i<l_StringInput.Length;i++)
		{
			print (l_StringInput [i]);
		}*/

		for (int row = 0; row <= g_WayFindingExcelDataRowCount; row++)
		{

			if (l_WordCount == 1 && g_WayFindingExcelData [row, 3] != null && g_WayFindingExcelData [row, 3].Contains (l_StringInput [0].ToLower()) && !l_OneWordFound) 
				{
					l_OneWordFoundInArea = g_WayFindingExcelData [row, 0];
					l_OneWordFoundInSubArea = g_WayFindingExcelData [row, 1];
					l_OneWordFound = true;
				}
			
			if (l_WordCount >= 2 && g_WayFindingExcelData [row, 3] != null &&   g_WayFindingExcelData [row, 3].Contains (l_StringInput [0].ToLower()) &&
				g_WayFindingExcelData [row, 3].Contains (l_StringInput [1].ToLower()) && !l_TwoWordFound) 
				{
					l_TwoWordFoundInArea = g_WayFindingExcelData [row, 0];
					l_TwoWordFoundInSubArea = g_WayFindingExcelData [row, 1];
					l_TwoWordFound = true;
				}
			
			if (l_WordCount >= 3 && g_WayFindingExcelData [row, 3] != null &&   g_WayFindingExcelData [row, 3].Contains (l_StringInput [0].ToLower()) &&
				g_WayFindingExcelData [row, 3].Contains (l_StringInput [1].ToLower()) && g_WayFindingExcelData [row, 3].Contains (l_StringInput [2].ToLower()) && !l_ThreeWordFound) 
				{
					l_ThreeWordFoundInArea = g_WayFindingExcelData [row, 0];
					l_ThreeWordFoundInSubArea = g_WayFindingExcelData [row, 1];
					l_ThreeWordFound = true;
				}
		



		}

		if (l_ThreeWordFound) 
		{
			f_OnUserInputFound (l_ThreeWordFoundInArea);
			C_StoreMapUIManager.Instance.PopUpPanel.SetActive (false);
		} 
		else if (l_TwoWordFound) 
		{
			f_OnUserInputFound (l_TwoWordFoundInArea);
			C_StoreMapUIManager.Instance.PopUpPanel.SetActive (false);
		} 
		else if (l_OneWordFound) 
		{
			f_OnUserInputFound (l_OneWordFoundInArea);
			C_StoreMapUIManager.Instance.PopUpPanel.SetActive (false);
		} 
		else 
		{
			C_StoreMapUIManager.Instance.PopUpPanel.SetActive (true);
		}
	}

	void f_OnUserInputFound(string l_Area)
	{
		if (l_Area == "living") {
			g_WayFinderManager.f_ManageAreaSelection (E_Area.Living);
		} else if (l_Area == "dining") {
			g_WayFinderManager.f_ManageAreaSelection (E_Area.Dining);
		} else if (l_Area == "sleeping") {
			g_WayFinderManager.f_ManageAreaSelection (E_Area.Sleeping);
		} else if (l_Area == "decorating") {
			g_WayFinderManager.f_ManageAreaSelection (E_Area.Decorating);
		} else if (l_Area == "playing") {
			g_WayFinderManager.f_ManageAreaSelection (E_Area.Playing);
		}
	}
}
