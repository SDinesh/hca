﻿using UnityEngine;
using System.Collections;

public class C_ModularProgram_UI : MonoBehaviour {
	
	private static C_ModularProgram_UI instance;
	public static C_ModularProgram_UI Instance{
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_ModularProgram_UI> ();

			return instance;
		}
	}

	[Header("Modular References")]
	public GameObject g_ModularPanel;
	public GameObject g_DiningPanel_Modular;
	public GameObject g_LivingPanel_Modular;
	public GameObject g_SleepingPanel_Modular;
	public GameObject g_PreviewUI;
	public GameObject g_CompositionUI;
	public GameObject g_PreviewManager;
	public GameObject g_CompositionManager;

	[Header("Select Product References")]
	public GameObject g_SelectProductPanel;
	public GameObject g_DiningPanel_Product;
	public GameObject g_LivingPanel_Product;
	public GameObject g_SleepingPanel_Product;
	public GameObject g_SigCombo_1;
	public GameObject g_SigCombo_2;
	public GameObject g_SignatureComboPanel;
	public GameObject g_CreateYourOwnPanel;
	public GameObject g_DiningCombinations;
	public GameObject g_SingleProducts;

    public GameObject infinityWardrobeObj;

	bool isSignature;

	void Start()
	{
		//f_ActivateModularPages (0);
	}

	public void f_ModularProgramBtn()
	{
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
			g_DiningPanel_Modular.SetActive (true);
			g_LivingPanel_Modular.SetActive (false);
			g_SleepingPanel_Modular.SetActive (false);
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			g_DiningPanel_Modular.SetActive (false);
			g_LivingPanel_Modular.SetActive (true);
			g_SleepingPanel_Modular.SetActive (false);
			g_SigCombo_1.SetActive (true);
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			g_DiningPanel_Modular.SetActive (false);
			g_LivingPanel_Modular.SetActive (false);
			g_SleepingPanel_Modular.SetActive (true);
		}
	}

	public void f_SelectProduct(string l_Value)
	{
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
			g_ModularPanel.SetActive (false);
			g_SelectProductPanel.SetActive (true);
			g_DiningPanel_Product.SetActive (true);
			if (string.Compare ("Blend", l_Value) == 0) {
				g_SingleProducts.SetActive (true);
				g_DiningCombinations.SetActive (false);
			} else if (string.Compare ("Combo", l_Value) == 0){
				g_SingleProducts.SetActive (false);
				g_DiningCombinations.SetActive (true);
			}
			g_LivingPanel_Product.SetActive (false);
			g_SleepingPanel_Product.SetActive (false);
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			g_DiningPanel_Product.SetActive (false);
			g_LivingPanel_Product.SetActive (false);
			g_SleepingPanel_Product.SetActive (false);
			if (string.Compare ("Sig1", l_Value) == 0) {
				g_SigCombo_1.SetActive (false);
				g_SigCombo_2.SetActive (true);
			} else if (string.Compare ("Sig2", l_Value) == 0) {
				g_LivingPanel_Product.SetActive (true);
				g_SelectProductPanel.SetActive (true);
				g_ModularPanel.SetActive (false);
			}
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			g_DiningPanel_Product.SetActive (false);
			g_LivingPanel_Product.SetActive (false);
			g_SleepingPanel_Product.SetActive (true);
		}
	}

	public void f_LivingSelection(bool l_isSignature)
	{
		isSignature = l_isSignature;
		if (isSignature) {
			g_SignatureComboPanel.SetActive (true);
			g_CreateYourOwnPanel.SetActive (false);
		} else {
			//g_CreateYourOwnPanel.SetActive (true);
			f_ActivateModularPages(1);
			g_SignatureComboPanel.SetActive (false);
		}
	}
	public void f_ActivateModularPages(int l_Value)
	{
		C_ApplicationManager.instance.f_DisableAllPages ();
		C_ApplicationManager.instance.g_MainPageUI.SetActive (true);
		if (l_Value == 0) {
			g_PreviewUI.SetActive (true);
			g_PreviewManager.SetActive (true);
			C_GlobalVariables.instance.g_View = E_View.Preview;
		} else if (l_Value == 1) {
			g_CompositionUI.SetActive (true);
			g_CompositionManager.SetActive (true);
			C_GlobalVariables.instance.g_View = E_View.Compose;

			if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
				if (C_StellarBed_Manager.Instance.g_SleepingType == 0) {
					C_StellarBed_Manager.Instance.f_Activate (0);
				} else if (C_StellarBed_Manager.Instance.g_SleepingType == 1) {
					C_StellarBed_Manager.Instance.f_Activate (1);
				} else if (C_StellarBed_Manager.Instance.g_SleepingType == 2) {
				    
                    infinityWardrobeObj.SetActive(true);
                    g_CompositionUI.SetActive (false);
                }
			}
		}
	}

	public void f_DisablePages()
	{
		g_ModularPanel.SetActive (false);
		g_SelectProductPanel.SetActive (false);
		g_SigCombo_1.SetActive (false);
		g_SigCombo_2.SetActive (false);
		g_PreviewUI.SetActive (false);
		g_CompositionUI.SetActive (false);
		g_PreviewManager.SetActive (false);
		g_CompositionManager.SetActive (false);
		g_CreateYourOwnPanel.SetActive (false);
		g_SignatureComboPanel.SetActive (false);
		g_SingleProducts.SetActive (false);
		g_DiningCombinations.SetActive (false);
		C_GlobalVariables.instance.g_View = E_View.None;
	}

	public GameObject Youtube;

	public void f_ActivateYoutube()
	{
		Youtube.SetActive (true);
	}


}
