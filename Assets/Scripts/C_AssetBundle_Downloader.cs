﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class C_AssetBundle_Downloader : MonoBehaviour {

	private static C_AssetBundle_Downloader instance;
	public static C_AssetBundle_Downloader Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_AssetBundle_Downloader> ();

			return instance;
		}
	}

	[Header("Info")]
	public string g_AssetName;
	public string g_AssetURL;
	public int downloadProgress;
	public GameObject g_Model;
	public GameObject l_Asset;

	[Header("Dev Set Values")]
	int g_Version;


	[Header("References")]
	public GameObject g_AssetHolder;

	void Start()
	{
		g_Version = 1;
	}

	public void f_StartDownloadingAssetBundle() {
		for (int l_i = 0; l_i < C_AssetPreviewManager.Instance.g_SelectedAssetArray.Length; l_i++) {
			if (string.Compare(C_AssetPreviewManager.Instance.g_SelectedAssetArray [l_i].name,g_AssetName) == 0) {
				C_AssetPreviewManager.Instance.g_SelectedAssetArray [l_i].SetActive (true);
				C_AssetPreviewManager.Instance.g_SelectedAsset = C_AssetPreviewManager.Instance.g_SelectedAssetArray [l_i];
			}
		}
	}

	IEnumerator DownloadAndCache () {
		Caching.CleanCache ();

		// Wait for the Caching system to be ready
		while (!Caching.ready)
			yield return null;

		// Load the AssetBundle file from Cache if it exists with the same version or download and store it in the cache
		using (WWW www = WWW.LoadFromCacheOrDownload ("file:///" + g_AssetURL, g_Version)) {
			while (!www.isDone) {
				yield return null;
			}
			if (www.error != null) {
				throw new Exception ("WWW download had an error:" + www.error);
			}

			AssetBundle l_Bundle = www.assetBundle;


			if (g_AssetName == "") {
				g_Model = Instantiate (l_Bundle.mainAsset) as GameObject;
				g_Model.transform.parent = g_AssetHolder.transform;

				Debug.Log ("Bundle Instantiated with no name");
			} else {
				g_Model = Instantiate (l_Bundle.LoadAsset (g_AssetName)) as GameObject;
				g_Model.transform.parent = g_AssetHolder.transform;
				Debug.Log ("Bundle Instantiated with name:" + g_Model.name);
				//C_AssetPreviewManager.Instance.g_SelectedAsset = g_Model;
			}
			// Unload the AssetBundles compressed contents to conserve memory
			l_Bundle.Unload (false);
		} // memory is freed from the web stream (www.Dispose() gets called implicitly)

	}
}
