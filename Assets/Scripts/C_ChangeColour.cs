﻿using UnityEngine;
using System.Collections;

public class C_ChangeColour : MonoBehaviour {



	public GameObject[] g_SofaModels = new GameObject[100];
	public GameObject[] SofaParts = new GameObject[100];
	public Material[] g_SofaMaterial = new Material[10];


	public GameObject[] g_SofaModels_Compose = new GameObject[100];
	public GameObject[] SofaParts_Compose = new GameObject[100];

	// Use this for initialization
	public void f_SelectGameObject () {
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			SofaParts = new GameObject[100];
			g_SofaModels = GameObject.FindGameObjectsWithTag ("Sofa");
			int l_i = 0;
			foreach (GameObject o in g_SofaModels) {
				if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1806 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1805 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1807 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1808 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1809 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1810 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1811 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1812 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1813 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1814 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1815 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1816 (Instance)") == 0) {
					SofaParts [l_i] = o;
					l_i++;
				}
			}
		}
	}

	public void f_ChangePaintBtn(int l_Colour)
	{
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			if (l_Colour == 0) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 1) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 2) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 3) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 4) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 5) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 6) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 7) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 8) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 9) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 10) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 11) {
				for (int l_i = 0; l_i < SofaParts.Length; l_i++) {
					if (SofaParts [l_i] != null)
						SofaParts [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			}
		} 
	}

	// ---- Compose ---

	public void f_SelectGameObject_Compose () {
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			SofaParts_Compose = new GameObject[100];
			g_SofaModels_Compose = GameObject.FindGameObjectsWithTag ("Sofa");
			int l_i = 0;
			foreach (GameObject o in g_SofaModels_Compose) {
				if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1806 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1805 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1807 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1808 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1809 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1810 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1811 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1812 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1813 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1814 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1815 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				} else if (string.Compare (o.GetComponent<MeshRenderer> ().materials [0].name, "F_AV_UKR_1816 (Instance)") == 0) {
					SofaParts_Compose [l_i] = o;
					l_i++;
				}
			}
		}
	}
	public void f_ChangePaintBtn_Compose(int l_Colour)
	{
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			if (l_Colour == 0) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 1) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 2) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 3) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 4) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 5) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 6) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 7) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 8) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 9) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 10) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			} else if (l_Colour == 11) {
				for (int l_i = 0; l_i < SofaParts_Compose.Length; l_i++) {
					if (SofaParts_Compose [l_i] != null)
						SofaParts_Compose [l_i].GetComponent<MeshRenderer> ().material = g_SofaMaterial [l_Colour];
				}
			}
		} 
	}

	public void f_SelectObjectDining_Compose(){
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
		}
	}
}
