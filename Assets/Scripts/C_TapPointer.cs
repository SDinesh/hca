﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class C_TapPointer : MonoBehaviour {



	public RectTransform g_RectTransform;
	public RectTransform g_RingTransform;
	public Image g_RingImg;

	Vector3 g_StartScale = Vector3.zero;
	Vector3 g_TargetScale = Vector3.zero;
	Color g_RingColor;




	void Awake()
	{
		g_RingColor = g_RingImg.color;
		g_StartScale = g_RectTransform.localScale;
		g_TargetScale = new Vector3 (1.5f,1.5f,1.5f);
		g_RingTargetScale = new Vector3 (2.5f,2.5f,2.5f);
	}

	void OnEnable () {

		g_RingTransform.gameObject.SetActive (false);
		g_RingImg.color = g_RingColor;
		g_RectTransform.localScale 	= g_StartScale;
		g_LerpVal 					= 0;
		g_StartVal 					= g_StartScale;
		g_EndVal 					= g_TargetScale;
		indicator = 1;
		g_StartHandAnimation ();
		print (">>>>>");
	
	}
	

	void g_StartHandAnimation()
	{
		
		StartCoroutine (e_HandAnimation(1.0f));
	}



	float g_LerpVal = 0;
	Vector3 g_StartVal = Vector3.zero;
	Vector3 g_EndVal = Vector3.zero;
	int indicator =0;

	IEnumerator e_HandAnimation(float l_Delay)
	{
		
		yield return new WaitForSeconds (l_Delay);

		while(g_LerpVal<1)
		{
			
			g_RectTransform.localScale = Vector3.Lerp(g_StartVal,g_EndVal,g_LerpVal);
			g_LerpVal += 5 * Time.deltaTime;
			if(g_LerpVal>=1)
			{
				g_LerpVal					= 0;

				if(indicator == 1)
				{
					g_StartVal 					= g_TargetScale;
					g_EndVal 					= g_StartScale;
					indicator = -1;
					g_RingTransform.gameObject.SetActive (true);
					f_RingAnimation();
				}

				else if(indicator == -1)
				{
					g_StartVal 					=g_StartScale;
					g_EndVal 					= g_TargetScale;
					indicator = 1;

					yield return new WaitForSeconds (1.5f);
					g_RingTransform.gameObject.SetActive (false);

				}
			}
			yield return null;
		}


	}



	void f_RingAnimation()
	{
		g_RingTransform.localScale = new Vector3 (0.1f, 0.1f, 0.1f);
		g_RinglerpVal = 0;
		g_RingColorlerpVal = 0;
		g_RingColor.a = 1;
		g_TargetRingColor = g_RingColor;
		g_TargetRingColor.a = 0;
		g_RingImg.color = g_RingColor;
		StartCoroutine (e_RingAnimation());
	}


	float g_RinglerpVal = 0;
	float g_RingColorlerpVal = 0;
	Color g_TargetRingColor;
	Vector3 g_RingTargetScale = Vector3.zero;
	IEnumerator e_RingAnimation()
	{
		while(g_RinglerpVal<1)
		{
			g_RingTransform.localScale = Vector3.Lerp(new Vector3(0.1f,0.1f,0.1f),g_RingTargetScale,g_RinglerpVal);
			g_RingImg.color = Vector4.Lerp(g_RingImg.color,g_TargetRingColor,g_RingColorlerpVal);
			g_RinglerpVal += 1.5f * Time.deltaTime;
			g_RingColorlerpVal += 0.45f * Time.deltaTime;
			if(g_RinglerpVal>=1)
			{
				yield break;
			}
			yield return null;
		}
	}







}
