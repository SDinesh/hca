﻿using UnityEngine;
using System.Collections;

public class C_WardrobeSnapping : MonoBehaviour {

	public bool isSnapped;
	public GameObject g_NeighbouringObj;

	// Update is called once per frame
	void Update () {
		if (g_NeighbouringObj == null) {
			isSnapped = false;
		}
	
		if(!isSnapped){
			transform.parent.gameObject.transform.position = Vector3.MoveTowards (transform.parent.gameObject.transform.position, new Vector3 (-1.07f, 0.0f, 0.0f),0.01f);
		}
	}

	void OnTriggerEnter(Collider l_Collider){
		isSnapped = true;
		g_NeighbouringObj = l_Collider.gameObject;
	}

	void OnTriggerExit(Collider l_Collider){
		isSnapped = false;
	}
}
