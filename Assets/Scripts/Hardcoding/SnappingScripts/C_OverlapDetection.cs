﻿using UnityEngine;
using System.Collections;

public class C_OverlapDetection : MonoBehaviour {

	public GameObject g_SnappedObj;
	bool is_Overlapping = false;
	public Transform g_SnapPoint;


	void OnTriggerEnter(Collider l_GameObject){
		if (g_SnappedObj == null) {
			g_SnappedObj = l_GameObject.gameObject;
			Debug.Log (transform.parent.gameObject.transform.parent.name);
			if (string.Compare (g_SnappedObj.name, transform.parent.gameObject.transform.parent.name) == 0) {
				g_SnappedObj = null;
				is_Overlapping = false;
			} else {
				g_SnappedObj = l_GameObject.gameObject;
				is_Overlapping = true;
				C_AssetCompositionManager.Instance.is_InputEnabled = false;
			}
		}
	}

	void OnTriggerExit(Collider l_GameObject){
		if (g_SnappedObj != null) {
			if (string.Compare (g_SnappedObj.transform.name, l_GameObject.name) == 0) {
				g_SnappedObj = null;
				is_Overlapping = false;
				C_AssetCompositionManager.Instance.is_InputEnabled = true;
				if (string.Compare (C_AssetCompositionManager.Instance.g_SelectedAsset.name, transform.parent.gameObject.transform.parent.name) != 0)
				C_AssetCompositionManager.Instance.g_SelectedAsset = null;
			}
		}
	}

	void Update(){
		if (is_Overlapping && g_SnappedObj != null) {
			f_SetAnchor ();
		}
	}

	void f_SetAnchor(){
		g_SnappedObj.transform.position = g_SnapPoint.position;
	}
}
