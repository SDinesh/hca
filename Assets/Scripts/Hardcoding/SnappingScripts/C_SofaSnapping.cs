﻿using UnityEngine;
using System.Collections;

public class C_SofaSnapping : MonoBehaviour {

	public GameObject g_SnappedObj;
	bool is_Overlapping = false;
	public Transform[] g_SnapPoints;

	void OnTriggerEnter(Collider l_GameObject){

		if (g_SnappedObj == null) {
			g_SnappedObj = l_GameObject.gameObject;
			is_Overlapping = true;
			C_AssetCompositionManager.Instance.is_InputEnabled = false;
		}
	}

	void OnTriggerExit(Collider l_GameObject){
		if (g_SnappedObj != null) {
			if (string.Compare (g_SnappedObj.transform.name, l_GameObject.name) == 0) {
				g_SnappedObj = null;
				is_Overlapping = false;
				C_AssetCompositionManager.Instance.is_InputEnabled = true;
				C_AssetCompositionManager.Instance.g_SelectedAsset = null;
			}
		}
	}

	void Update(){
		if (is_Overlapping && g_SnappedObj != null) {
			f_SetAnchor ();
		}
	}

	void f_SetAnchor(){
		//g_SnappedObj.transform.position = g_SnapPoint.position;
	}
}
