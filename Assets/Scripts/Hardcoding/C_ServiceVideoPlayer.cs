﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class C_ServiceVideoPlayer : MonoBehaviour {

	private static C_ServiceVideoPlayer instance;
	public static C_ServiceVideoPlayer Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_ServiceVideoPlayer> ();

			return instance;
		}
	}

	public RenderHeads.Media.AVProVideo.MediaPlayer g_MediaPlayer;

	/*public MovieTexture[] ServicesVidoes;
	int VideoCounter;


	public void f_PlayVideo(int l_Video){
		if (!ServicesVidoes [l_Video].isPlaying) {
			GetComponent<RawImage> ().texture = ServicesVidoes [l_Video] as MovieTexture;
			ServicesVidoes [l_Video].Play ();
			VideoCounter = l_Video;
			//Invoke("f_CloseVideoPanel", ServicesVidoes [l_Video].duration);
		}
	}

	public void f_CloseVideoPanel(){
		C_ServicesVideoPlayController.Instance.f_CloseBtn ();
	}

	void OnDisable(){
		ServicesVidoes [VideoCounter].Stop ();
	}*/

	public void f_PlayVideo(int l_Video)
	{
		
		switch(l_Video)
		{
		case 1:
			g_MediaPlayer.m_VideoPath ="C:\\avpro\\Services\\1.mp4";
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
			break;

		case 2:
			g_MediaPlayer.m_VideoPath ="C:\\avpro\\Services\\2.mp4";
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
			break;

		case 3:
			g_MediaPlayer.m_VideoPath ="C:\\avpro\\Services\\3.mp4";
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
			break;

		case 4:
			g_MediaPlayer.m_VideoPath ="C:\\avpro\\Services\\4.mp4";
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
			break;

		case 5:
			g_MediaPlayer.m_VideoPath ="C:\\avpro\\Services\\5.mp4";
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
			break;

		case 6:
			g_MediaPlayer.m_VideoPath ="C:\\avpro\\Services\\6.mp4";
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
			break;

		case 7:
			g_MediaPlayer.m_VideoPath ="C:\\avpro\\Services\\7.mp4";
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
			break;

		case 8:
			g_MediaPlayer.m_VideoPath ="C:\\avpro\\Services\\8.mp4";
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
			break;

		case 9:
			g_MediaPlayer.m_VideoPath ="C:\\avpro\\Services\\9.mp4";
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
			break;

		case 10:
			g_MediaPlayer.m_VideoPath ="C:\\avpro\\Services\\10.mp4";
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
			break;
		}
	}





}
