﻿using UnityEngine;
using System.Collections;

public class C_AssetPreviewUI : MonoBehaviour {

	private static C_AssetPreviewUI instance;
	public static C_AssetPreviewUI Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_AssetPreviewUI> ();

			return instance;
		}
	}

	public C_CustomButton g_360View;
	public C_CustomButton g_Color;
	public C_CustomButton g_Compose;


	// Use this for initialization
	void OnEnable () {
		
		f_ResetAllBtns ();
		g_360View.f_OnPointerUp ();
	
	}


	public void f_ResetAllBtns()
	{
		g_360View.f_ResetButton ();
		g_Color.f_ResetButton ();
		g_Compose.f_ResetButton ();
	}
	

}
