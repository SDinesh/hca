﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class C_CustomButton : MonoBehaviour {

	// Use this for initialization

	public Image g_ButtonBG;
	public Image g_ButtonNumberBG;
	public Text g_ButtonLabel;
	public Text g_ButtonNumber;

	public Color g_ButtonDefaultColor;
	public Color g_ButtonColorOnPress;
	public Color g_TextDefaultColor;
	public Color g_TextColorOnPress;
	public Color g_NumTextDefaultColor;
	public Color g_NumTextColorOnPress;
	public Color g_ButtonNumberBGDefaultColor;
	public Color g_ButtonNumberBGColorOnPress;


	public void f_OnPointerDown()
	{
		if(C_AssetPreviewUI.Instance != null)
		C_AssetPreviewUI.Instance.f_ResetAllBtns ();
		if(C_AssetCompositionUI.Instance != null)
		C_AssetCompositionUI.Instance.f_ResetAllBtns ();
		//C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		if(C_WayFinderManager.Instance !=null){
			if (!C_WayFinderManager.Instance.g_Navigator.activeSelf) {
				g_ButtonBG.color = g_ButtonColorOnPress;
				g_ButtonLabel.color = g_TextColorOnPress;
				g_ButtonNumberBG.color = g_ButtonNumberBGColorOnPress;
				g_ButtonNumber.color = g_NumTextColorOnPress;
			}
		}
	}

	public void f_OnPointerUp()
	{
		g_ButtonBG.color = g_ButtonColorOnPress;
		g_ButtonLabel.color = g_TextColorOnPress;
		g_ButtonNumberBG.color = g_ButtonNumberBGDefaultColor;
		g_ButtonNumber.color = g_NumTextDefaultColor;
	}

	public void f_ResetButton()
	{
		g_ButtonBG.color = g_ButtonDefaultColor;
		g_ButtonLabel.color = g_TextDefaultColor;
		g_ButtonNumber.color = g_NumTextDefaultColor;
		g_ButtonNumberBG.color = g_ButtonNumberBGDefaultColor;
	}
}
