﻿using UnityEngine;
using System.Collections;

public class C_AssetCompositionUI : MonoBehaviour {

	private static C_AssetCompositionUI instance;
	public static C_AssetCompositionUI Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_AssetCompositionUI> ();

			return instance;
		}
	}

	public C_CustomButton g_Compose;
	public C_CustomButton g_Color;
	public C_CustomButton g_Texture;

	// Use this for initialization
	void OnEnable () {

		f_ResetAllBtns ();
		//g_Compose.f_OnPointerUp ();

	}


	public void f_ResetAllBtns()
	{
		g_Compose.f_ResetButton ();
		g_Color.f_ResetButton ();
		g_Texture.f_ResetButton ();
	}

}
