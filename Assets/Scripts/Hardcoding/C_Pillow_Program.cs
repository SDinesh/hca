﻿using UnityEngine;
using System.Collections;

public class C_Pillow_Program : MonoBehaviour {

	private static C_Pillow_Program instance;
	public static C_Pillow_Program Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_Pillow_Program> ();

			return instance;
		}
	}


	public GameObject[] g_PillowPanels;
	public GameObject g_Pillow;

	public void f_BeddingBtn(){
		C_ApplicationManager.instance.f_DisableAllPages ();
		g_Pillow.SetActive (true);
		g_PillowPanels [0].SetActive (true);
		C_ApplicationManager.instance.g_MainPageUI.SetActive (true);
	}

	public void f_NextBtn(int l_Val)
	{
		f_DisableAll ();
		g_PillowPanels [l_Val].SetActive (true);

	}

	public void f_DisableAll(){
		for (int l_i = 0; l_i < g_PillowPanels.Length; l_i++) {
			g_PillowPanels [l_i].SetActive (false);
		}
	}
}
