﻿using UnityEngine;
using System.Collections;

public class C_Colours_Hardcoded : MonoBehaviour {

	private static C_Colours_Hardcoded instance;
	public static C_Colours_Hardcoded Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_Colours_Hardcoded> ();

			return instance;
		}
	}

	public GameObject TblColourHandler;
	public GameObject ChairColourHandler;

	public Texture[] TableTopTextures;
	public Material[] TableTopMats;

	public Texture[] ChairsTextures;
	public Material[] ChairMats;

	public Texture[] SofaTextures;
	public Material[] SofaMats;

	public GameObject ChairColourHandlerBlend;
	public GameObject ChairColourHandlerBerkley;
	public GameObject ChairColourHandlerGaily;
	public GameObject ChairColourHandlerClaire;
	public GameObject ChairColourHandlerHexa;
	public GameObject ChairColourHandlerParlin;

	void OnEnable(){
		f_DisableAll ();
	}

	public void f_ChangeColourRectTableTop(int l_Val){
		if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "squaretabletop") == 0) {
			if (l_Val == 0) {
				TableTopMats [0].mainTexture = TableTopTextures [l_Val];
			} else if (l_Val == 1) {
				TableTopMats [0].mainTexture = TableTopTextures [l_Val];
			} else if (l_Val == 2) {
				TableTopMats [0].mainTexture = TableTopTextures [l_Val];
			} else if (l_Val == 3) {
				TableTopMats [0].mainTexture = TableTopTextures [l_Val];
			} else if (l_Val == 4) {
				TableTopMats [0].mainTexture = TableTopTextures [l_Val];
			}
		} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "roundtabletop") == 0) {
			if (l_Val == 0) {
				TableTopMats [1].mainTexture = TableTopTextures [l_Val];
			} else if (l_Val == 1) {
				TableTopMats [1].mainTexture = TableTopTextures [l_Val];
			} else if (l_Val == 2) {
				TableTopMats [1].mainTexture = TableTopTextures [l_Val];
			} else if (l_Val == 3) {
				TableTopMats [1].mainTexture = TableTopTextures [l_Val];
			} else if (l_Val == 4) {
				TableTopMats [1].mainTexture = TableTopTextures [l_Val];
			}
		}
	}

	public void f_ChangeColourChair(int l_Val){
		if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "clairechair") == 0) {
			if (l_Val == 5) {
				ChairMats [8].mainTexture = ChairsTextures [l_Val];
				ChairMats [7].mainTexture = ChairsTextures [l_Val];
				ChairMats [6].mainTexture = ChairsTextures [l_Val];
			} else if (l_Val == 7) {
				ChairMats [8].mainTexture = ChairsTextures [l_Val];
				ChairMats [7].mainTexture = ChairsTextures [l_Val];
				ChairMats [6].mainTexture = ChairsTextures [l_Val];
			} 
		} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "gailychair") == 0) {
			if (l_Val == 3) {
				ChairMats [2].mainTexture = ChairsTextures [l_Val];
			} else if (l_Val == 4) {
				ChairMats [2].mainTexture = ChairsTextures [l_Val];
			}
		} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "hexachair") == 0) {
			if (l_Val == 2) {
				ChairMats [3].mainTexture = ChairsTextures [l_Val];
			} else if (l_Val == 1) {
				ChairMats [3].mainTexture = ChairsTextures [l_Val];
			} else if (l_Val == 3) {
				ChairMats [3].mainTexture = ChairsTextures [l_Val];
			} else if (l_Val == 6) {
				ChairMats [3].mainTexture = ChairsTextures [l_Val];
			}
		} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "blendchair") == 0) {
			if (l_Val == 0) {
				ChairMats [1].mainTexture = ChairsTextures [l_Val];
			} else if (l_Val == 1) {
				ChairMats [1].mainTexture = ChairsTextures [l_Val];
			} else if (l_Val == 2) {
				ChairMats [1].mainTexture = ChairsTextures [l_Val];
			} 
		} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "berkeleychair") == 0) {
			if (l_Val == 8) {
				ChairMats [0].mainTexture = ChairsTextures [l_Val];
			} else if (l_Val == 1) {
				ChairMats [0].mainTexture = ChairsTextures [l_Val];
			} 
		} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "parlinchair") == 0) {
			if (l_Val == 5) {
				ChairMats [4].mainTexture = ChairsTextures [l_Val];
				ChairMats [5].mainTexture = ChairsTextures [l_Val];
			} else if (l_Val == 1) {
				ChairMats [4].mainTexture = ChairsTextures [l_Val];
				ChairMats [5].mainTexture = ChairsTextures [l_Val];
			} else if (l_Val == 2) {
				ChairMats [4].mainTexture = ChairsTextures [l_Val];
				ChairMats [5].mainTexture = ChairsTextures [l_Val];
			}
		}
	}

	public void f_ChangeColourSofa(int l_Val){
		if (l_Val == 0) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		} else if (l_Val == 1) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		} else if (l_Val == 2) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		} else if (l_Val == 3) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		} else if (l_Val == 4) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 5) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 6) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 7) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 8) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 9) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 10) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}
	}


	public GameObject[] Chair_Compose = new GameObject[100];
	public GameObject[] Table_Compose = new GameObject[10];
	public Texture[] StellarBedTextures;

	public Material[] BedMats;

	public void f_SelectAssest_Compose(){
		Chair_Compose = GameObject.FindGameObjectsWithTag ("Chair");
		Table_Compose = GameObject.FindGameObjectsWithTag ("Table");
		//StellarBed_Compose = GameObject.FindGameObjectsWithTag ("Bed");
	}

	public void f_ChangeColourChair_Compose(int l_Val){
		if (l_Val == 0) {
			for (int l_i = 0; l_i < Chair_Compose.Length; l_i++) {
				Chair_Compose[l_i].GetComponent<MeshRenderer> ().material = ChairMats [l_Val];
			}
		} else if (l_Val == 1) {
			for (int l_i = 0; l_i < Chair_Compose.Length; l_i++) {
				Chair_Compose[l_i].GetComponent<MeshRenderer> ().material = ChairMats [l_Val];
			}
		} else if (l_Val == 2) {
			for (int l_i = 0; l_i < Chair_Compose.Length; l_i++) {
				Chair_Compose[l_i].GetComponent<MeshRenderer> ().material = ChairMats [l_Val];
			}
		} else if (l_Val == 3) {
			for (int l_i = 0; l_i < Chair_Compose.Length; l_i++) {
				Chair_Compose[l_i].GetComponent<MeshRenderer> ().material = ChairMats [l_Val];
			}
		}
	}

	public void f_ChangeColourTable_Compose(int l_Val){
		if (l_Val == 0) {
			for (int l_i = 0; l_i < Table_Compose.Length; l_i++) {
				Table_Compose[l_i].GetComponent<MeshRenderer> ().material = TableTopMats [l_Val];
			}
		} else if (l_Val == 1) {
			for (int l_i = 0; l_i < Table_Compose.Length; l_i++) {
				Table_Compose[l_i].GetComponent<MeshRenderer> ().material = TableTopMats [l_Val];
			}
		} else if (l_Val == 2) {
			for (int l_i = 0; l_i < Table_Compose.Length; l_i++) {
				Table_Compose[l_i].GetComponent<MeshRenderer> ().material = TableTopMats [l_Val];
			}
		}
	}

	public void f_ChangeColourBed_Compose(int l_Val){
		if (l_Val == 0) {
			for (int l_i = 0; l_i < BedMats.Length; l_i++) {
				BedMats [l_i].mainTexture = StellarBedTextures [l_Val];
				BedMats[l_i].SetFloat("_Glossiness",0.2f);
			}
		} else if (l_Val == 1) {
			for (int l_i = 0; l_i < BedMats.Length; l_i++) {
				BedMats [l_i].mainTexture = StellarBedTextures [l_Val];
				BedMats[l_i].SetFloat("_Glossiness",0.2f);
			}
		} else if (l_Val == 2) {
			for (int l_i = 0; l_i < BedMats.Length; l_i++) {
				BedMats [l_i].mainTexture = StellarBedTextures [l_Val];
				Debug.Log (BedMats [l_i].name);
				if(string.Compare(BedMats[l_i].name,"Stellar Hydraulic Base") != 0)
				BedMats[l_i].SetFloat("_Glossiness",0.75f);
			}
		} else if (l_Val == 3) {
			for (int l_i = 0; l_i < BedMats.Length; l_i++) {
				BedMats [l_i].mainTexture = StellarBedTextures [l_Val];
				BedMats[l_i].SetFloat("_Glossiness",0.2f);
			}
		}
	}

	public void f_ChangeColourSofa_Compose(int l_Val){
		if (l_Val == 0) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		} else if (l_Val == 1) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		} else if (l_Val == 2) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		} else if (l_Val == 3) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		} else if (l_Val == 4) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 5) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 6) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 7) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 8) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 9) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}else if (l_Val == 10) {
			for (int l_i = 0; l_i < SofaMats.Length; l_i++) {
				SofaMats [l_i].mainTexture = SofaTextures [l_Val];
			}
		}
	}

	public void f_DisableAll(){
		ChairColourHandlerBlend.SetActive (false);
		ChairColourHandlerHexa.SetActive (false);
		ChairColourHandlerBerkley.SetActive (false);
		ChairColourHandlerClaire.SetActive (false);
		ChairColourHandlerParlin.SetActive (false);
		ChairColourHandlerGaily.SetActive (false);
		TblColourHandler.SetActive (false);
		ChairColourHandler.SetActive (false);
	}
}
