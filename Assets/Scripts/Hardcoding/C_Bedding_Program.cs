﻿using UnityEngine;
using System.Collections;

public class C_Bedding_Program : MonoBehaviour {
	private static C_Bedding_Program instance;
	public static C_Bedding_Program Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_Bedding_Program> ();

			return instance;
		}
	}


	public GameObject[] g_BeddingPanels;
	public GameObject g_Bedding;

	public void f_BeddingBtn(){
		C_ApplicationManager.instance.f_DisableAllPages ();
		g_Bedding.SetActive (true);
		g_BeddingPanels [0].SetActive (true);
		C_ApplicationManager.instance.g_MainPageUI.SetActive (true);
	}

	public void f_NextBtn(int l_Val)
	{
		f_DisableAll ();
		g_BeddingPanels [l_Val].SetActive (true);

	}

	public void f_DisableAll(){
		for (int l_i = 0; l_i < g_BeddingPanels.Length; l_i++) {
			g_BeddingPanels [l_i].SetActive (false);
		}
	}
}
