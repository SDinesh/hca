﻿using UnityEngine;
using System.Collections;

public class C_TowelsColorButtonUI : MonoBehaviour {

	private static C_TowelsColorButtonUI instance;
	public static C_TowelsColorButtonUI Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_TowelsColorButtonUI> ();

			return instance;
		}
	}

	public C_CustomButton g_Aristocrat;
	public C_CustomButton g_Supreme;
	public C_CustomButton g_Indulgence;

	// Use this for initialization
	void OnEnable () {

		f_ResetAllBtns ();
		//g_Compose.f_OnPointerUp ();

	}


	public void f_ResetAllBtns()
	{
		g_Aristocrat.f_ResetButton ();
		g_Supreme.f_ResetButton ();
		g_Indulgence.f_ResetButton ();
	}

}
