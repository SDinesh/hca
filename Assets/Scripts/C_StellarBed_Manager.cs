﻿using UnityEngine;
using System.Collections;

public class C_StellarBed_Manager : MonoBehaviour {

	private static C_StellarBed_Manager instance;
	public static C_StellarBed_Manager Instance{
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_StellarBed_Manager> ();

			return instance;
		}
	}

	public GameObject Stellar;
	public GameObject WardrobeContinental;
	public GameObject WardrobeInfinity;
	public GameObject buttonBar;
	public GameObject Compose;
	public GameObject CompseCamoff;
	public GameObject PreivewCam;
	public GameObject Color;
	public GameObject SelectColour;
	public GameObject SelectSizeBtn;

	public GameObject g_SelectProductsPanel;
	public GameObject g_SelectStellarBedCombosPanel;

	public GameObject g_StellarBedSelect;
	public GameObject g_ContinentalSelect;
	public GameObject g_InfinitiSelect;

	public GameObject g_Preview360;
	public GameObject g_PreviewColour;
	public GameObject g_PreviewRotateHolder;
	public GameObject g_DoorsBtnHolder;
	public GameObject PreviewMakeYourOwnCompose;

	public int g_SleepingType;

	public bool isRotatable;
	public void Start(){
		isRotatable = true;
	}
	public void f_Activate(int l_val)
	{
		if (l_val == 0) {
			buttonBar.SetActive (false);
			Compose.SetActive (false);
			CompseCamoff.SetActive (false);
			Color.SetActive (false);
			SelectColour.SetActive (true);
			PreivewCam.SetActive (true);
			SelectSizeBtn.SetActive (true);
			C_Continental_Wardrobe.Instance.g_ContinentalDoorsBtnHolder.SetActive (false);
			C_AssetCompositionManager.Instance.g_SelectedCustomizationOption = E_CustomizationOption.None;
		} else if (l_val == 1) {
			buttonBar.SetActive (false);
			Compose.SetActive (false);
			CompseCamoff.SetActive (false);
			Color.SetActive (false);
			SelectColour.SetActive (false);
			PreivewCam.SetActive (true);
			SelectSizeBtn.SetActive (false);
			C_Continental_Wardrobe.Instance.g_ContinentalDoorsBtnHolder.SetActive (true);
			C_AssetCompositionManager.Instance.g_SelectedCustomizationOption = E_CustomizationOption.None;
		}else if (l_val == 2) {
			buttonBar.SetActive (false);
			Compose.SetActive (false);
			CompseCamoff.SetActive (false);
			Color.SetActive (false);
			SelectColour.SetActive (false);
			PreivewCam.SetActive (true);
			SelectSizeBtn.SetActive (false);
			C_Continental_Wardrobe.Instance.g_ContinentalDoorsBtnHolder.SetActive (false);
			C_AssetCompositionManager.Instance.g_SelectedCustomizationOption = E_CustomizationOption.None;
		}
	}

	void f_DisableSelect(){
		g_StellarBedSelect.SetActive (false);
		g_ContinentalSelect.SetActive (false);
		g_InfinitiSelect.SetActive (false);
		Stellar.SetActive (false);
		WardrobeInfinity.SetActive (false);
		WardrobeContinental.SetActive (false);
	}

	public void f_ActivateSelectBedProducts(int l_Val){
		f_DisableSelect ();
		g_SelectProductsPanel.SetActive (true);
		g_SelectStellarBedCombosPanel.SetActive (true);
		g_SleepingType = l_Val;
		if (l_Val == 0) {
			Stellar.SetActive (true);
			g_StellarBedSelect.SetActive (true);
			g_DoorsBtnHolder.SetActive (false);
			PreviewMakeYourOwnCompose.SetActive (true);
		} else if (l_Val == 1) {
			g_ContinentalSelect.SetActive (true);
			g_DoorsBtnHolder.SetActive (true);
			WardrobeContinental.SetActive (true);
			//PreviewMakeYourOwnCompose.SetActive (false);
		} else if (l_Val == 2) {
			g_InfinitiSelect.SetActive (true);
			g_DoorsBtnHolder.SetActive (true);
            PreviewMakeYourOwnCompose.SetActive (true);
			WardrobeInfinity.SetActive (true);
            C_StellarBed_Manager.Instance.g_SleepingType = 2;
		}
	}

	public void f_DisablePreviewBtns(int l_Val){
		if (l_Val == 0) {
			g_Preview360.SetActive (false);
			g_PreviewColour.SetActive (false);
			isRotatable = false;
		}
	}

	public void f_ManageBedComboColours(int l_Val){
		if (l_Val == 0) {
			for (int l_i = 0; l_i < C_Colours_Hardcoded.Instance.BedMats.Length; l_i++) {
				C_Colours_Hardcoded.Instance.BedMats [l_i].mainTexture = C_Colours_Hardcoded.Instance.StellarBedTextures [0];
				C_Colours_Hardcoded.Instance.BedMats [l_i].SetFloat ("_Glossiness", 0.2f);
			}
		} else if (l_Val == 1) {
			for (int l_i = 0; l_i < C_Colours_Hardcoded.Instance.BedMats.Length; l_i++) {
				C_Colours_Hardcoded.Instance.BedMats [l_i].mainTexture = C_Colours_Hardcoded.Instance.StellarBedTextures [3];
				C_Colours_Hardcoded.Instance.BedMats [l_i].SetFloat ("_Glossiness", 0.2f);
			}
		}
	}
}
