﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class C_StoreMapUIManager : MonoBehaviour {

	private static C_StoreMapUIManager instance;
	public static C_StoreMapUIManager Instance{
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_StoreMapUIManager> ();

			return instance;
		}
	}

	public GameObject g_NavigationUI;
	public GameObject g_SimpleUI;
	public GameObject g_GetMeThereBtn;
	public Image g_MapImage;
	public InputField g_InputField;
	public GameObject g_VirtualKeyboard;
	public GameObject g_MapIndicator;
	public Text g_MapIndicatorAreaText;
	public C_CustomButton[] g_MapImages ;
	public C_WayFinderManager g_WayFinderManager;
	public Animator KeyboardAnimator;
	public GameObject Decoration;
	public GameObject Dining;
	public GameObject DreamRoom;
	public GameObject Living;
	public GameObject Playing;
	public GameObject Sleeping;
	public GameObject PopUpPanel;

	public GameObject DiningIndicator;
	public GameObject LivingIndicator;
	public GameObject SleepingIndicator;

	// Use this for initialization
	void OnEnable ()
	{
		f_Reset_Images ();
		for(int i=0;i<9;i++)
		{
			g_MapImages [i].f_ResetButton();
		}
		g_GetMeThereBtn.SetActive (false);

		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Entrance) 
		{
			g_NavigationUI.SetActive (true);
			g_SimpleUI.SetActive (false);
		} 
		else 
		{
			g_NavigationUI.SetActive (false);
			g_SimpleUI.SetActive (true);
			if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
				DiningIndicator.SetActive (true);
				LivingIndicator.SetActive (false);
				SleepingIndicator.SetActive (false);
			} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
				LivingIndicator.SetActive (true);
				SleepingIndicator.SetActive (false);
				DiningIndicator.SetActive (false);
			} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
				SleepingIndicator.SetActive (true);
				LivingIndicator.SetActive (false);
				DiningIndicator.SetActive (false);

			}
			f_ManageMapImages(C_GlobalVariables.instance.g_SelectedArea);
		}


	}
	
	// Update is called once per frame
	void Update () {

		if(g_InputField.isFocused && !g_VirtualKeyboard.activeSelf)
		{
			if (g_WayFinderManager.g_IsNavigationActive) {return;}
			f_Reset_Images ();
			g_VirtualKeyboard.SetActive (true);
			KeyboardAnimator.SetBool ("isKeyboard", true);
		}
		else if(g_VirtualKeyboard.activeSelf)
		{
			g_InputField.text = C_VirtualKeyboard.instance.g_TypedString;
		}
	
	}

	public void f_OnSearchBtnClick()
	{
		//C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		KeyboardAnimator.SetBool ("isKeyboard", false);
		Invoke ("f_DisableKeyboard", 2.0f);
		C_ExcelParser.instance.f_SearchWayfinderExcelData (g_InputField.text);
	}

	public void f_DisableKeyboard()
	{
		g_VirtualKeyboard.SetActive (false);
	}

	public void f_OnNavigateBtnClick()
	{
		//C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		if (g_WayFinderManager.g_IsNavigationActive) {return;}
		g_WayFinderManager.f_StartNavigation ();
	}

	public void f_MapButtonHandler(int l_AreaID)
	{
		//C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		if (g_WayFinderManager.g_IsNavigationActive) {return;}
		switch(l_AreaID)
		{
		case 1:
			f_ManageMapImages(E_Area.Living);
			break;

		case 2:
			f_ManageMapImages(E_Area.Decorating);
			break;

		case 3:
			f_ManageMapImages(E_Area.Dining);
			break;

		case 4:
			f_ManageMapImages(E_Area.Dreamroom);
			break;

		case 5:
			f_ManageMapImages(E_Area.Sleeping);
			break;

		case 6:
			f_ManageMapImages(E_Area.Playing);
			break;

		case 7:
			f_ManageMapImages(E_Area.CustomerService);
			break;

		case 8:
			f_ManageMapImages(E_Area.Prayerroom);
			break;

		case 9:
			f_ManageMapImages(E_Area.CashTills);
			break;
		}
	}

	public void f_Reset_Images()
	{
		Living.SetActive (false);
		Decoration.SetActive (false);
		Dining.SetActive (false);
		DreamRoom.SetActive (false);
		Playing.SetActive (false);
		Sleeping.SetActive (false);
	}

	public void f_AreaSelectionHandler(int l_AreaID)
	{
		//C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		g_GetMeThereBtn.SetActive (false);
		if (g_WayFinderManager.g_IsNavigationActive) {return;}

		for(int i=0;i<9;i++)
		{
			g_MapImages [i].f_ResetButton ();
		}

		g_MapImages [l_AreaID - 1].f_OnPointerUp ();


		switch(l_AreaID)
		{
		case 1:
			//g_WayFinderManager.f_ManageAreaSelection (E_Area.Living);
			C_AreaMapManager.Instance.f_ResetAllMaterial ();
			Living.SetActive (true);
			Decoration.SetActive (false);
			Dining.SetActive (false);
			DreamRoom.SetActive (false);
			Playing.SetActive (false);
			Sleeping.SetActive (false);
			break;

		case 2:
			//g_WayFinderManager.f_ManageAreaSelection (E_Area.Decorating);
			C_AreaMapManager.Instance.f_ResetAllMaterial ();
			Living.SetActive (false);
			Decoration.SetActive (true);
			Dining.SetActive (false);
			DreamRoom.SetActive (false);
			Playing.SetActive (false);
			Sleeping.SetActive (false);
			break;

		case 3:
			//g_WayFinderManager.f_ManageAreaSelection(E_Area.Dining);
			C_AreaMapManager.Instance.f_ResetAllMaterial ();
			Living.SetActive (false);
			Decoration.SetActive (false);
			Dining.SetActive (true);
			DreamRoom.SetActive (false);
			Playing.SetActive (false);
			Sleeping.SetActive (false);
			break;

		case 4:
			//g_WayFinderManager.f_ManageAreaSelection (E_Area.Dreamroom);
			C_AreaMapManager.Instance.f_ResetAllMaterial ();
			Living.SetActive (false);
			Decoration.SetActive (false);
			Dining.SetActive (false);
			DreamRoom.SetActive (true);
			Playing.SetActive (false);
			Sleeping.SetActive (false);
			break;

		case 5:
			//g_WayFinderManager.f_ManageAreaSelection(E_Area.Sleeping);
			C_AreaMapManager.Instance.f_ResetAllMaterial ();
			Living.SetActive (false);
			Decoration.SetActive (false);
			Dining.SetActive (false);
			DreamRoom.SetActive (false);
			Playing.SetActive (false);
			Sleeping.SetActive (true);
			break;

		case 6:
			//g_WayFinderManager.f_ManageAreaSelection (E_Area.Playing);
			C_AreaMapManager.Instance.f_ResetAllMaterial ();
			Living.SetActive (false);
			Decoration.SetActive (false);
			Dining.SetActive (false);
			DreamRoom.SetActive (false);
			Playing.SetActive (true);
			Sleeping.SetActive (false);
			break;

		case 7:
			//g_WayFinderManager.f_ManageAreaSelection (E_Area.CustomerService);
			C_AreaMapManager.Instance.f_ResetAllMaterial ();
			C_AreaMapManager.Instance.f_HighlightArea("CustomerServices");
			break;

		case 8:
			//g_WayFinderManager.f_ManageAreaSelection (E_Area.Prayerroom);
			C_AreaMapManager.Instance.f_ResetAllMaterial ();
			C_AreaMapManager.Instance.f_HighlightArea("Prayer");
			break;

		case 9:
			//g_WayFinderManager.f_ManageAreaSelection (E_Area.CashTills);
			C_AreaMapManager.Instance.f_ResetAllMaterial ();
			C_AreaMapManager.Instance.f_HighlightArea("CashTills");
			break;
		}
	}


	void f_ManageMapImages(E_Area l_Area)
	{
		//

		g_WayFinderManager.f_ManageAreaSelection (l_Area);
		switch(l_Area)
		{
		case E_Area.Living:
			g_MapIndicatorAreaText.text = "LIVING AREA";
			break;

		case E_Area.Decorating:
			g_MapIndicatorAreaText.text = "DECORATING AREA";
			break;

		case E_Area.Dining:
			g_MapIndicatorAreaText.text = "DINING AREA";
			break;

		case E_Area.Dreamroom:
			g_MapIndicatorAreaText.text = "DREAMROOM AREA";
			break;

		case E_Area.Sleeping:
			g_MapIndicatorAreaText.text = "SLEEPING AREA";
			break;

		case E_Area.Playing:
			g_MapIndicatorAreaText.text = "PLAYING AREA";
			break;

		case E_Area.CustomerService:
			g_MapIndicatorAreaText.text = "CUSTOMER SERVICE AREA";
			break;

		case E_Area.Prayerroom:
			g_MapIndicatorAreaText.text = "PRAYER ROOM AREA";
			break;

		case E_Area.CashTills:
			g_MapIndicatorAreaText.text = "CASH TILLS AREA";
			break;
		}

		if (C_GlobalVariables.instance.g_SelectedArea != E_Area.Entrance) 
		{
			g_MapIndicator.SetActive (false);

			if (C_GlobalVariables.instance.g_SelectedArea == l_Area) {
				g_MapIndicator.SetActive (true);
			}
		}
	}
}


