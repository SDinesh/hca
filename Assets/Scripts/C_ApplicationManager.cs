﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;



public class C_ApplicationManager : MonoBehaviour {

	private static C_ApplicationManager _instance;
	public static C_ApplicationManager instance
	{
		get
		{
			if(_instance == null)
			{
				GameObject l_GameManagerObj = GameObject.Find("ApplicationManager");
				_instance = l_GameManagerObj.GetComponent<C_ApplicationManager>();
			}
			return _instance;
		}
	}
	//----------------------------------------------------------------------------
	public GameObject g_AreaSelectScreen;
	public GameObject g_Splashscreen;
	public GameObject g_ScreensaverUI;
	//public GameObject g_ScreensaverObj;
	public GameObject g_LanguageSelectPage;
	public GameObject g_MainMenuPage;
	public GameObject g_MainPageUI;
	public GameObject g_StoreMapUI;
	public GameObject g_WayFinderObj;
	public GameObject g_WayFinderUI;
	public GameObject g_CataloguePage;
	public GameObject g_ModularProgramUI;
	public GameObject g_OffersPage;
	public GameObject g_ServicesPage;
	public GameObject g_SocialMediaPage;
	public RawImage g_FacebookBrowserRenderer;
	public RawImage g_TwitterBrowserRenderer;
	public RawImage g_InstagramBrowserRenderer;
	public RawImage g_YoutubeBrowserRenderer;
	public RawImage g_ShukranBrowserRenderer;
	public RawImage g_HC_SiteBrowserRenderer;
	public RawImage g_HC_CatalogueRenderer;
	public C_MainpageUIManager g_MainPageUI_Manager;
	public GameObject ServiceVideosPanel;
	public GameObject ServiceVideosCloseBtn;

	public float g_SplashScreenLifeSpan = 0;
	public bool g_IsSplashScreenActive = true;

	void Awake()
	{
		f_InitApplication ();
	}

	void Start()
	{
		
	}

	/*void Update()
	{
		if(g_IsSplashScreenActive && Time.time >= g_SplashScreenLifeSpan)
		{
			print (">>>>");
			f_ManageApplicationPage (E_ApplicationPage.ScreenSaver);
			g_IsSplashScreenActive = false;
		}

		f_ActivateScreenSaver ();
	}*/
	void Update()
	{
		if(g_IsSplashScreenActive && Time.time >= g_SplashScreenLifeSpan)
		{
			f_ManageApplicationPage (E_ApplicationPage.ScreenSaver);
			g_IsSplashScreenActive = false;
		}

		f_ActivateScreenSaver ();

		if (Input.GetMouseButtonDown (0)) 
		{ 
			C_GlobalVariables.instance.g_LastTouchTime = Time.time;    
		}

		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) 
		{ 
			C_GlobalVariables.instance.g_LastTouchTime = Time.time;    
		}
	} 


	void f_ActivateScreenSaver()
	{
		if (C_GlobalVariables.instance.g_LastTouchTime > 0 && Time.time >= C_GlobalVariables.instance.g_LastTouchTime + 60) 
		{
			if (!g_ScreensaverUI.activeSelf)
			{
				f_ManageApplicationPage (E_ApplicationPage.ScreenSaver);
				f_DisableAllBrowser ();
			}
		}

	}

	public void f_ManageApplicationPage(E_ApplicationPage l_Page)
	{
		f_DisableAllPages ();

		switch(l_Page)
		{
		case E_ApplicationPage.SplashScreen:
			g_Splashscreen.SetActive (true);
			C_GlobalVariables.instance.g_CurrentActivePage = E_ApplicationPage.SplashScreen;
			break;

		case E_ApplicationPage.ScreenSaver:
			g_ScreensaverUI.SetActive (true);
			//g_ScreensaverObj.SetActive (true);
			C_GlobalVariables.instance.g_CurrentActivePage = E_ApplicationPage.ScreenSaver;
			break;

		case E_ApplicationPage.LanguageSelect:
			g_LanguageSelectPage.SetActive (true);
			C_GlobalVariables.instance.g_CurrentActivePage = E_ApplicationPage.LanguageSelect;
			break;

		case E_ApplicationPage.MainMenuPage:
			g_MainMenuPage.SetActive (true);
			C_GlobalVariables.instance.g_CurrentActivePage = E_ApplicationPage.MainMenuPage;
			break;

		case E_ApplicationPage.MainPageUI:
			g_MainPageUI.SetActive (true);
			C_GlobalVariables.instance.g_CurrentActivePage = E_ApplicationPage.MainPageUI;
			break;
		}
	}

	public void f_SetActiveServices(int l_ServiceCode)
	{
		
		g_MainPageUI_Manager.f_ManageToolbarButtons (l_ServiceCode);
		f_DisableAllBrowser ();


		switch(l_ServiceCode)
		{
		case 0:
			C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.None;
			break;

		case 1:
			C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.StoreMap;
			g_WayFinderObj.SetActive (true);
			g_WayFinderUI.SetActive (true);
			break;

		case 2:
			C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.None;
			//C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.Catalogue;
			//g_CataloguePage.SetActive (true);
			//g_HC_CatalogueRenderer.enabled = true;
			break;

		case 3:
			C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.ModularProgram;
			g_ModularProgramUI.SetActive (true);
			break;

		case 4:
			C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.OtherServices;
			g_ServicesPage.SetActive (true);
			break;

		case 5:
			C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.None;
			//C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.Shukran;
			//g_ShukranBrowserRenderer.enabled = true;
			break;

		case 6:
			C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.None;
			//C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.Website;
			//g_HC_SiteBrowserRenderer.enabled = true;
			break;

		case 7:
			C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.SocialMedia;
			g_SocialMediaPage.SetActive (true);
			break;

		case 8:
			C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.Offers;
			g_OffersPage.SetActive (true);
			break;

		case 9:
			C_GlobalVariables.instance.g_CurrentActiveServices = E_Services.ComingSoon;
			break;
		}
	}




	public void f_InitSocialMedia(E_SocialMedia l_SocialMedia)
	{
		f_DisableAllBrowser ();

		switch (l_SocialMedia) 
		{
		case E_SocialMedia.Facebook:
			//g_FacebookBrowserRenderer.enabled = true;
			break;

		case E_SocialMedia.Twitter:
			//g_TwitterBrowserRenderer.enabled = true;
			break;

		case E_SocialMedia.Instagram:
			//g_InstagramBrowserRenderer.enabled = true;
			break;

		case E_SocialMedia.Youtube:
			//g_YoutubeBrowserRenderer.enabled = true;
			break;
		}


	}


	void f_InitApplication()
	{
		//g_IsSplashScreenActive = true;
		//f_ManageApplicationPage (E_ApplicationPage.SplashScreen);
		g_IsSplashScreenActive = false;
//		g_AreaSelectScreen.SetActive (true);
		f_SetActiveServices (0);

		//Testing Purpose
		g_MainMenuPage.SetActive (true);

	}


	public void f_DisableAllPages()
	{
		g_Splashscreen.SetActive (false);
		g_ScreensaverUI.SetActive (false);
		//g_ScreensaverObj.SetActive (false);
		g_LanguageSelectPage.SetActive (false);
		g_MainMenuPage.SetActive (false);
		g_MainPageUI.SetActive (false);
		g_SocialMediaPage.SetActive (false);
		g_AreaSelectScreen.SetActive (false);
		g_StoreMapUI.SetActive (false);
		g_CataloguePage.SetActive (false);
		g_ModularProgramUI.SetActive (false);
		g_OffersPage.SetActive (false);
		g_ServicesPage.SetActive (false);
		g_WayFinderObj.SetActive (false);
		g_WayFinderUI.SetActive (false);
		C_ModularProgram_UI.Instance.f_DisablePages ();
		if (C_StoreMapUIManager.Instance != null) {
			C_StoreMapUIManager.Instance.g_VirtualKeyboard.SetActive (false);
		}
		ServiceVideosPanel.SetActive (false);
		ServiceVideosCloseBtn.SetActive (false);
		C_Mattress_Program.Instance.Mattress.SetActive (false);
		C_Mattress_Program.Instance.f_DisableAll ();
		C_Bedding_Program.Instance.f_DisableAll ();
		C_Bedding_Program.Instance.g_Bedding.SetActive (false);
		C_Pillow_Program.Instance.g_Pillow.SetActive (false);
		C_Pillow_Program.Instance.f_DisableAll ();
		C_Sleeping_UI.Instance.f_DisableAll ();
	}

	void f_DisableAllBrowser()
	{
		g_ShukranBrowserRenderer.enabled = false;
		g_HC_SiteBrowserRenderer.enabled = false;
		g_FacebookBrowserRenderer.enabled = false;
		g_TwitterBrowserRenderer.enabled = false;
		g_InstagramBrowserRenderer.enabled = false;
		g_YoutubeBrowserRenderer.enabled = false;
		g_HC_CatalogueRenderer.enabled = false;
	}
}