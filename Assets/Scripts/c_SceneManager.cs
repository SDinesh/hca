﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class c_SceneManager : MonoBehaviour {


	public void f_LoadScene(string l_String)
	{
		SceneManager.LoadScene (l_String);
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Escape)) {
			SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
		}
	}
}
