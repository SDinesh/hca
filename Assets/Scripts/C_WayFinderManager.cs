﻿using UnityEngine;
using System.Collections;

public class C_WayFinderManager : MonoBehaviour {

	private static C_WayFinderManager instance;
	public static C_WayFinderManager Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_WayFinderManager> ();

			return instance;
		}
	}


	public Material[] g_AreaMaterial = new Material[9];
	public Vector3 g_MapPos_EntranceArea = Vector3.zero;
	public Vector3 g_MapPos_OtherArea = Vector3.zero;
	public Color g_SelectedAreaColor;
	public Color g_UnselectedAreaColor;
	public GameObject g_Navigator;
	public GameObject g_BasicMap;
	public GameObject g_DetailedMap;
	public bool g_IsNavigationActive;
	int g_WayPointIndex = 0;
	public C_AreaMapManager g_AreaManager;
	public C_WaypointManager g_SelectedWaypoint;
	public GameObject g_MainCamera;
	public GameObject g_MapPositionIndicator;
	public C_Highlighter[] g_Highlighters;

	// Use this for initialization
	void OnEnable () 
	{
		
		g_Navigator.SetActive (false);
		g_IsNavigationActive = false;
		g_BasicMap.SetActive (true);
		g_DetailedMap.SetActive (false);
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Entrance)
		{	
			g_BasicMap.transform.position = g_MapPos_EntranceArea;
			f_ResetMaterial ();

		} 
		else 
		{
			g_BasicMap.transform.position = g_MapPos_OtherArea;
			f_ManageAreaSelection (C_GlobalVariables.instance.g_SelectedArea);
		}
	}
	
	// Update is called once per frame
	void Update () {
		f_Navigate ();
	}

	public void f_ManageAreaSelection(E_Area l_Area)
	{
		f_ResetMaterial ();
		//print ("l_Area"+l_Area);
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Entrance)
		{	
			g_AreaManager.f_ActivateArea (l_Area);
			g_BasicMap.SetActive (true);
			g_DetailedMap.SetActive (false);
			return;

		} 

		switch(l_Area)
		{
		case E_Area.Living:
			g_AreaMaterial [0].color = g_SelectedAreaColor;
			break;

		case E_Area.Decorating:
			g_AreaMaterial [1].color = g_SelectedAreaColor;
			break;

		case E_Area.Dining:
			g_AreaMaterial [2].color = g_SelectedAreaColor;
			break;

		case E_Area.Dreamroom:
			g_AreaMaterial [3].color = g_SelectedAreaColor;
			break;

		case E_Area.Sleeping:
			g_AreaMaterial [4].color = g_SelectedAreaColor;
			break;

		case E_Area.Playing:
			g_AreaMaterial [5].color = g_SelectedAreaColor;
			break;

		case E_Area.CustomerService:
			g_AreaMaterial [6].color = g_SelectedAreaColor;
			break;

		case E_Area.Prayerroom:
			g_AreaMaterial [7].color = g_SelectedAreaColor;
			break;

		case E_Area.CashTills:
			g_AreaMaterial [8].color = g_SelectedAreaColor;
			break;
		}
	}


	void f_ResetMaterial()
	{
		for(int i=0;i<9;i++)
		{
			g_AreaMaterial [i].color = g_UnselectedAreaColor;
		}
	}



	public void f_StartNavigation()
	{
		g_IsNavigationActive = true;
		g_WayPointIndex = 1;
		g_MainCamera.SetActive (false);
		g_Navigator.SetActive (true);
		g_Navigator.transform.position = g_SelectedWaypoint.g_ActiveWaypointObjs [0].transform.position;
		for(int i=0;i<g_Highlighters.Length;i++)
		{
			g_Highlighters [i].f_ManageTextObjects(false);
		}
	}

	void f_Navigate()
	{



		if (g_IsNavigationActive && g_WayPointIndex < g_SelectedWaypoint.g_ActiveWaypointObjs.Length) 
		{
			g_MapPositionIndicator.SetActive (false);
			g_Navigator.transform.position = Vector3.MoveTowards (g_Navigator.transform.position,g_SelectedWaypoint.g_ActiveWaypointObjs [g_WayPointIndex].transform.position,6*Time.deltaTime);
				
			//if(g_Navigator.transform.position == g_SelectedWaypoint.g_ActiveWaypointObjs[g_WayPointIndex].transform.position)
			if (Vector3.Distance(g_Navigator.transform.position,g_SelectedWaypoint.g_ActiveWaypointObjs [g_WayPointIndex].transform.position) <= 0.2f) 
			{
				g_WayPointIndex++;
			}

			if (Vector3.Distance(g_Navigator.transform.position,g_SelectedWaypoint.g_ActiveWaypointObjs [g_SelectedWaypoint.g_ActiveWaypointObjs.Length-1].transform.position) <= 0.2f) 
			{
				
				StartCoroutine ("E_NavigatorPause");
			}


		}
	}

	IEnumerator E_NavigatorPause()
	{
		yield return new WaitForSeconds (2.0f);
		g_Navigator.transform.GetChild(0).gameObject.SetActive(false);
		g_MapPositionIndicator.SetActive (true);
		g_MainCamera.SetActive (true);
		yield return new WaitForSeconds(5.0f);
		//g_IsNavigationActive = false;
		g_Navigator.transform.GetChild(0).gameObject.SetActive(true);
		g_Navigator.SetActive (false);
		g_Navigator.GetComponent<TrailRenderer> ().Clear ();
		g_IsNavigationActive = false;
		for(int i=0;i<g_Highlighters.Length;i++)
		{
			g_Highlighters [i].f_ManageTextObjects(false);
		}
	}
}
