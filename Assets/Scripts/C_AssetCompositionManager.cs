﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class C_AssetCompositionManager : MonoBehaviour {

	private static C_AssetCompositionManager instance;
	public static C_AssetCompositionManager Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_AssetCompositionManager> ();

			return instance;
		}
	}

	public GameObject g_CompositionBtnPanel;
	public GameObject g_ColorBtnPanel;
	public GameObject g_TextureBtnPanel;
	public GameObject g_ComposeCameraObj;
	public GameObject g_PreviewCameraPivotObj;
	public Camera g_ComposeCamera;
	public Camera g_PreviewCamera;

	public GameObject LivingLight;
	public GameObject DiningLight;

	public GameObject LivingLight_360;
	public GameObject DiningLight_360;

	public GameObject TableColourHolder;
	public GameObject ChairColourHolder;
	public GameObject TextureBtn;

	public Button MoveBtnActive;
	public Button RotateBtnActive;

	public GameObject g_SelectedAsset;
	public E_CustomizationOption g_SelectedCustomizationOption;

	bool g_ActivatePreviewCamera = false;

	void OnEnable () {
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
			TextureBtn.SetActive (true);
			g_ComposeCameraObj.GetComponent<Camera> ().orthographicSize = 7.5f;
			DiningLight.SetActive (true);
			LivingLight.SetActive (false);
			DiningLight_360.SetActive (true);
			LivingLight_360.SetActive (false);
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			TextureBtn.SetActive (false);
			DiningLight.SetActive (false);
			LivingLight.SetActive (true);
			DiningLight_360.SetActive (false);
			LivingLight_360.SetActive (true);
		} else {
			TextureBtn.SetActive (false);
			DiningLight.SetActive (false);
			LivingLight.SetActive (false);
			LivingLight_360.SetActive (false);
			DiningLight_360.SetActive (true);
		}
		g_CompositionBtnPanel.SetActive(true);
		g_ColorBtnPanel.SetActive(false);
		g_TextureBtnPanel.SetActive(false);
		ChairColourHolder.SetActive (false);
		g_SelectedCustomizationOption = E_CustomizationOption.Move;
		MoveBtnActive.interactable = false;
		C_AssetCompositionUI.Instance.g_Compose.f_OnPointerUp ();
	
	}

	Ray g_Ray;
	RaycastHit g_RaycastHit;
	Vector3 g_TouchWorldPoint;
	Vector3 g_TouchPoint;

	bool g_RotateRightFlag = false;
	bool g_RotateLeftFlag  = false;
	float g_RotateSpeed = 0;
	bool f_IsRotateDirectionSet = false;


	Vector3 g_touch_1;
	Vector3 g_touch_2;
	Vector3 g_PreviewCamInitialPos;


	public bool is_InputEnabled;

	void Start()
	{
		g_PreviewCamInitialPos 	= g_PreviewCameraPivotObj.transform.position;
		g_ActivatePreviewCamera = false;
		f_IsRotateDirectionSet = false;
		g_RotateRightFlag = false;
		g_RotateLeftFlag  = false;
		g_RotateSpeed = 80;
		is_InputEnabled = true;
	}

	void Update () 
	{
		f_Touch();
		f_ManagePreviewCamera ();
	}

	void f_ManageObjRotation()
	{
		if (g_RotateLeftFlag) 
		{
			g_SelectedAsset.transform.Rotate (Vector3.up * g_RotateSpeed * Time.deltaTime);
		}
		else if (g_RotateRightFlag) 
		{
			g_SelectedAsset.transform.Rotate (Vector3.up * -g_RotateSpeed * Time.deltaTime);
		}
	}


	void f_Touch()
	{
		if (!is_InputEnabled) {
			return;
		}
		if (g_SelectedCustomizationOption != E_CustomizationOption.None) {
			if (g_SelectedCustomizationOption == E_CustomizationOption.Move) {
				if (Input.GetMouseButtonDown (0) && g_SelectedAsset == null) {
					C_GlobalVariables.instance.g_LastTouchTime = Time.time;
					g_Ray = g_ComposeCamera.ScreenPointToRay (Input.mousePosition);
					if (Physics.Raycast (g_Ray, out g_RaycastHit)) {
						g_SelectedAsset = g_RaycastHit.transform.gameObject;
						C_Modular_Composition.Instance.g_AssetName = g_SelectedAsset.name;
						if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
							g_SelectedAsset.GetComponent<Rigidbody> ().isKinematic = false;
						}
					}
				} else if (Input.GetMouseButton (0) && g_SelectedAsset != null) {
					C_GlobalVariables.instance.g_LastTouchTime = Time.time;
					g_TouchWorldPoint = Vector3.zero;
					g_TouchPoint = Input.mousePosition;
					g_TouchPoint.z = g_ComposeCameraObj.transform.position.y;
					g_TouchWorldPoint = g_ComposeCamera.ScreenToWorldPoint (g_TouchPoint);
					g_TouchWorldPoint.y = 0;

                    if(!InfinityWardrobeManager.Instance.gameObject.activeInHierarchy)
					    g_SelectedAsset.transform.position = g_TouchWorldPoint; 
				}

				if (Input.GetMouseButtonUp (0)) {
					if (g_SelectedAsset != null) {
						if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
							g_SelectedAsset.GetComponent<Rigidbody> ().isKinematic = true;
						}
						g_SelectedAsset = null;
					}
				}
			} else if (g_SelectedCustomizationOption == E_CustomizationOption.Rotate) {
				f_SingleTouchRotate ();
			}
		} else {
			if (Input.GetMouseButtonDown (0)) {
				if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
					g_Ray = g_ComposeCamera.ScreenPointToRay (Input.mousePosition);
					if (Physics.Raycast (g_Ray, out g_RaycastHit)) {
						C_Modular_Composition.Instance.g_AssetName = g_RaycastHit.transform.gameObject.name;
						C_Modular_Composition.Instance.f_SelectAssetForColour ();
					}
				} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
					g_Ray = g_PreviewCamera.ScreenPointToRay (Input.mousePosition);
					if (Physics.Raycast (g_Ray, out g_RaycastHit)) {

						g_SelectedAsset = g_RaycastHit.transform.gameObject;
						C_Continental_Wardrobe.Instance.g_CurrentlySelectedWardrobe = g_SelectedAsset;
						C_Continental_Wardrobe.Instance.f_Activate ();
					}
				}
			}
		}
	}

//	void f_MangeTouch()
//	{
//		if(g_SelectedCustomizationOption != E_CustomizationOption.None )
//		{
//			if (g_SelectedCustomizationOption == E_CustomizationOption.Move) {
//				if (Input.touchCount > 0) {
//					if (Input.GetTouch (0).phase == TouchPhase.Began) {
//						g_Ray = g_ComposeCamera.ScreenPointToRay (Input.GetTouch (0).position);
//
//						if (Physics.Raycast (g_Ray, out g_RaycastHit)) {
//
//							g_SelectedAsset = g_RaycastHit.transform.gameObject;
//						}
//					} else if (Input.GetTouch (0).phase == TouchPhase.Moved) {
//						if (g_SelectedAsset != null) {
//							g_TouchWorldPoint = Vector3.zero;
//							g_TouchPoint = Input.GetTouch (0).position;
//							g_TouchPoint.z = g_ComposeCameraObj.transform.position.y;
//							g_TouchWorldPoint = g_ComposeCamera.ScreenToWorldPoint (g_TouchPoint);
//							g_TouchWorldPoint.y = 0;
//							g_SelectedAsset.transform.position = g_TouchWorldPoint; 
//						}
//					} else if (Input.GetTouch (0).phase == TouchPhase.Ended) {
//						g_SelectedAsset = null;
//					}
//				}
//			} else if (g_SelectedCustomizationOption == E_CustomizationOption.Rotate) {
//				f_SingleTapRotate ();
//			}
//		}
//	}
//
//	void f_TwoFingerRotate()
//	{
//		if (Input.touchCount > 0) {
//			if (Input.GetTouch (0).phase == TouchPhase.Began) {
//
//				g_touch_1 = Input.GetTouch (0).position;
//				g_Ray = g_ComposeCamera.ScreenPointToRay (Input.GetTouch (0).position);
//
//				if (Physics.Raycast (g_Ray, out g_RaycastHit)) {
//					g_SelectedAsset = g_RaycastHit.transform.gameObject;
//					f_IsRotateDirectionSet = false;
//				}
//			}
//		}
//		if (Input.touchCount > 1 && g_SelectedAsset != null) {
//
//			if (Input.GetTouch(1).phase == TouchPhase.Began) 
//			{
//
//				g_touch_2 = Input.GetTouch (1).position;
//			}
//			else if(Input.GetTouch(1).phase == TouchPhase.Stationary)
//			{
//				g_touch_2 = Input.GetTouch (1).position;
//				f_IsRotateDirectionSet = false;
//
//			}
//
//			else if(Input.GetTouch(1).phase == TouchPhase.Moved)
//			{
//
//				if(!f_IsRotateDirectionSet)
//				{
//					if (Input.GetTouch (1).position.x < g_touch_2.x &&  Input.GetTouch (1).position.y < g_touch_1.y) 
//					{
//						g_RotateLeftFlag = true;
//						g_RotateRightFlag = false;
//					}
//					else if (Input.GetTouch (1).position.x > g_touch_2.x &&  Input.GetTouch (1).position.y < g_touch_1.y) 
//					{
//						g_RotateLeftFlag = false;
//						g_RotateRightFlag = true;
//					}
//					else if (Input.GetTouch (1).position.x < g_touch_2.x &&  Input.GetTouch (1).position.y >= g_touch_1.y) 
//					{
//						g_RotateLeftFlag = false;
//						g_RotateRightFlag = true;
//					}
//					else if (Input.GetTouch (1).position.x > g_touch_2.x &&  Input.GetTouch (1).position.y >= g_touch_1.y) 
//					{
//						g_RotateLeftFlag = true;
//						g_RotateRightFlag = false;
//					}
//					f_IsRotateDirectionSet = true;
//				}
//
//				f_ManageObjRotation ();
//
//
//			}
//
//			else if(Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(1).phase == TouchPhase.Ended)
//			{
//				g_SelectedAsset = null;
//				f_IsRotateDirectionSet = false;
//			}
//
//		}
//	}

	void f_SingleTouchRotate()
	{
		if (Input.GetMouseButtonDown(0)) {
			C_GlobalVariables.instance.g_LastTouchTime = Time.time;
			g_Ray = g_ComposeCamera.ScreenPointToRay (Input.mousePosition);

			if (Physics.Raycast (g_Ray, out g_RaycastHit)) {
				g_SelectedAsset = g_RaycastHit.transform.gameObject;

				if(g_SelectedAsset != null)
				{
					if(string.Compare(g_SelectedAsset.name,"roundtabletop(Clone)") != 0)
					g_SelectedAsset.transform.eulerAngles += new Vector3 (0, 45, 0);
				}
			}
		}
		if (Input.GetMouseButtonUp (0)) {
			g_SelectedAsset = null;
		}
	}

//	void f_SingleTapRotate()
//	{
//		if (Input.touchCount > 0) 
//		{
//			if (Input.GetTouch (0).phase == TouchPhase.Began) 
//			{
//				g_Ray = g_ComposeCamera.ScreenPointToRay (Input.GetTouch (0).position);
//
//				if (Physics.Raycast (g_Ray, out g_RaycastHit)) {
//					g_SelectedAsset = g_RaycastHit.transform.gameObject;
//
//					if(g_SelectedAsset != null)
//					{
//						g_SelectedAsset.transform.eulerAngles += new Vector3 (0, 45, 0);
//					}
//
//				}
//			}
//		}
//	}



	public void f_ManageComposeColorTexturePanel(int l_ID)
	{
		//C_AssetCompositionUI.Instance.f_ResetAllBtns ();
		if(l_ID == 1)
		{
			g_CompositionBtnPanel.SetActive(true);
			g_ColorBtnPanel.SetActive(false);
			g_TextureBtnPanel.SetActive(false);
			ChairColourHolder.SetActive (false);
			g_TextureBtnPanel.SetActive (false);
			g_SelectedCustomizationOption = E_CustomizationOption.Move;
			MoveBtnActive.interactable = false;
			RotateBtnActive.interactable = true;
		}
		else if(l_ID == 2)
		{
			g_CompositionBtnPanel.SetActive(false);
			if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
				ChairColourHolder.SetActive (true);
				g_ColorBtnPanel.SetActive (false);
				C_Modular_Composition.Instance.f_SelectAssetForColour ();
			} else if(C_GlobalVariables.instance.g_SelectedArea == E_Area.Living){
				g_ColorBtnPanel.SetActive(true);
				ChairColourHolder.SetActive (false);
			}

			g_TextureBtnPanel.SetActive(false);
			g_SelectedCustomizationOption = E_CustomizationOption.None;
		}
		else if(l_ID == 3)
		{
			g_CompositionBtnPanel.SetActive(false);
			g_ColorBtnPanel.SetActive(false);
			ChairColourHolder.SetActive (false);
			if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
				g_TextureBtnPanel.SetActive (true);
			}
			g_SelectedCustomizationOption = E_CustomizationOption.None;
		}
	}

	public void f_OnCustomizationOptionsBtnClick(int l_ID)
	{
		if (l_ID == 1) 
		{
			g_SelectedCustomizationOption = E_CustomizationOption.Move;
			MoveBtnActive.interactable = false;
			RotateBtnActive.interactable = true;
		} 
		else if(l_ID == 2)
		{
			g_SelectedCustomizationOption = E_CustomizationOption.Rotate;
			RotateBtnActive.interactable = false;
			MoveBtnActive.interactable = true;
		}
		else if(l_ID == 3)
		{
			g_SelectedCustomizationOption = E_CustomizationOption.None;
			g_SelectedAsset = null;
		}
	}

	public void f_OnZoombuttonClick(int l_Val)
	{
		if(l_Val > 0 && g_ComposeCameraObj.GetComponent<Camera>().orthographicSize >= 3.0f){
			g_ComposeCameraObj.GetComponent<Camera> ().orthographicSize -= 0.5f;
		} else if(l_Val < 0 && g_ComposeCameraObj.GetComponent<Camera>().orthographicSize <= 7.0f){
			g_ComposeCameraObj.GetComponent<Camera> ().orthographicSize += 0.5f;
		}
	}
		
	public void f_OnPointerExitRotateBtn()
	{
		g_ActivatePreviewCamera = false;
		g_ComposeCameraObj.SetActive (true);
		g_PreviewCameraPivotObj.SetActive (false);
	}

	public void f_OnPointerDownRotateBtn()
	{
		g_ActivatePreviewCamera = true;
		g_ComposeCameraObj.SetActive (false);
		g_PreviewCameraPivotObj.SetActive (true);
		g_PreviewCameraPivotObj.transform.position = g_PreviewCamInitialPos;
		g_PreviewCameraPivotObj.transform.rotation = Quaternion.identity;
	}

	public void f_OnPointerUpRotateBtn()
	{
		g_ActivatePreviewCamera = false;
		g_ComposeCameraObj.SetActive (true);
		g_PreviewCameraPivotObj.SetActive (false);
		g_SelectedCustomizationOption = E_CustomizationOption.Move;
		MoveBtnActive.interactable = false;
		RotateBtnActive.interactable = true;
	}

	void f_ManagePreviewCamera()
	{
		if(g_ActivatePreviewCamera)
		{
			g_PreviewCameraPivotObj.transform.Rotate (Vector3.up * 20 * Time.deltaTime);
		}
	}
}

public enum E_CustomizationOption
{
	Move,
	Rotate,
	None
}
