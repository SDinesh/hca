﻿using UnityEngine;
using System.Collections;

public class C_Mattress_Program : MonoBehaviour {
	
	private static C_Mattress_Program instance;
	public static C_Mattress_Program Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_Mattress_Program> ();

			return instance;
		}
	}


	public GameObject[] g_MatteressPanels;
	public GameObject Mattress;

	public void f_MattressBtn(){
		C_ApplicationManager.instance.f_DisableAllPages ();
		Mattress.SetActive (true);
		g_MatteressPanels [0].SetActive (true);
		C_ApplicationManager.instance.g_MainPageUI.SetActive (true);
	}

	public void f_NextBtn(int l_Val)
	{
		f_DisableAll ();
		g_MatteressPanels [l_Val].SetActive (true);

	}

	public void f_BackToMain(){
		
	}

	public void f_DisableAll(){
		for (int l_i = 0; l_i < g_MatteressPanels.Length; l_i++) {
			g_MatteressPanels [l_i].SetActive (false);
		}
	}
}
