﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System.Linq;

public class SnapManager : MonoBehaviour
{

    private static SnapManager _instance;
    public static SnapManager Instance { get { return _instance; } private set { _instance = value; } }


    private List<Snapable> snapableList;
    private Snapable selectedObject;


    [SerializeField]
    public float unlockThresholdDistance=4;

    private Ray dragRay;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }
    // Use this for initialization
    void Start()
    {
        snapableList = new List<Snapable>(FindObjectsOfType<Snapable>());
    }

    public void doSnap(Snapable snapingObj)
    {
       
    }

}
