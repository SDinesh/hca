﻿using UnityEngine;
using System.Collections;

public class SnapAnchor : MonoBehaviour {

    Snapable parentSnapable;

    void OnEnable()
    {
        parentSnapable = GetComponentInParent<Snapable>();
        if (parentSnapable == null)
            Debug.LogError("Anchor is attached to multiple or no snapbale object");
    }

    void OnTriggerEnter()
    {
        parentSnapable.SetActiveAnchor(this);
    }

}
