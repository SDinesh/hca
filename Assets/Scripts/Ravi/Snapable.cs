﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class Snapable : MonoBehaviour, IEndDragHandler, IDragHandler,IBeginDragHandler
{

    public List<SnapAnchor> anchors;
    SphereCollider proximitySphere;

    private SnapAnchor activeAchor;

    public void SetActiveAnchor(SnapAnchor anchor)
    {
        activeAchor = anchor;
    }

    bool isHandled;
    public void OnDrag(PointerEventData eventData)
    {
        Vector3 mouseWorldPos  = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Vector3.Distance(this.transform.position, Camera.main.transform.position)));
        if (isHandled)
        {
            this.transform.position = new Vector3(mouseWorldPos.x, this.transform.position.y, mouseWorldPos.z);
        }
        if(!isHandled && Vector3.Distance(mouseWorldPos,this.transform.position)>SnapManager.Instance.unlockThresholdDistance)
        {
            isHandled = true;
            this.transform.position = new Vector3(mouseWorldPos.x, this.transform.position.y, mouseWorldPos.z);
        }
    }
    public void OnEndDrag(PointerEventData eventData)
    {

        isHandled = false;
   
    }
    void OnTriggerEnter(Collider other)
    {
        if (isHandled)
        {
            var sa = other.GetComponent<SnapAnchor>();
            if (sa != null)
            {
                this.transform.position = sa.transform.position;
                this.transform.position +=  sa.transform.position- activeAchor.transform.position;
                isHandled = false;
            }
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        isHandled = true;
    }
}
