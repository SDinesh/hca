﻿using UnityEngine;
using System.Collections;

public class C_Highlighter : MonoBehaviour {

	// Use this for initialization
	public GameObject[] g_TextObjects;

	void OnEnable () {
		f_ManageTextObjects (false);

	}

	void OnTriggerEnter () {
		f_ManageTextObjects (true);
	
	}
	
	// Update is called once per frame
	void OnTriggerExit () {
		f_ManageTextObjects (false);
	
	}

	public void f_ManageTextObjects (bool l_Stat)
	{
		for(int i=0;i<g_TextObjects.Length;i++)
		{
			g_TextObjects [i].SetActive (l_Stat);
		}
	}
}
