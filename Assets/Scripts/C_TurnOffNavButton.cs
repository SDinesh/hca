﻿using UnityEngine;
using System.Collections;

public class C_TurnOffNavButton : MonoBehaviour {

	public GameObject GetMeThereButton;  // Drag in the Get Me There button into this in the inspector

	void  OnEnable()
	{
		GetMeThereButton.SetActive (false);
	}

	void OnDisable()
	{
		GetMeThereButton.SetActive (true);
	}
}
