﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class C_Sleeping_UI : MonoBehaviour {

	// Use this for initialization
	/*void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}*/

	private static C_Sleeping_UI instance;
	public static C_Sleeping_UI Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_Sleeping_UI> ();

			return instance;
		}
	}

	private int SIA_temp;
	private bool color_bar_aristocrat = false;
	private bool color_bar_indulgence = false;
	private bool color_bar_supreme = false;
	private bool color_bar_temp = false;
	private int hide_count;
	private bool toggle_bar_aristocrat = false;
	private bool toggle_bar_indulgence = false;
	private bool toggle_bar_supreme = false;
	public Button aristrocrat_main;
	public Button indulgence_main;
	public Button supreme_main;


	[Header("Towel GameObject References")]

	public GameObject g_SleepingPanel;
	public GameObject g_SleepingPanel_Towels_main;
	public GameObject g_SleepingPanel_Towels;
	public GameObject g_SleepingPanel_Towels_Aristocrat_Detail;
	public GameObject g_SleepingPanel_Towels_Indulgence_Detail;
	public GameObject g_SleepingPanel_Towels_Supreme_Detail;
	public GameObject g_SleepingPanel_Color_Bar_Aristocrat;
	public GameObject g_SleepingPanel_Color_Bar_Indulgence;
	public GameObject g_SleepingPanel_Color_Bar_Supreme;

	public void Sleeping_program_towels_button_click(int show_count)
	{
		hide_count = show_count;
		g_SleepingPanel.SetActive (false);
		switch(show_count)
		{
		case 1:
				g_SleepingPanel_Towels_main.SetActive (true);
				g_SleepingPanel_Towels.SetActive (true);
				Debug.Log ("1");
				break;

			case 2:
				g_SleepingPanel_Towels_Aristocrat_Detail.SetActive(true);
				g_SleepingPanel_Towels_Indulgence_Detail.SetActive(false);
				g_SleepingPanel_Towels_Supreme_Detail.SetActive(false);
				Debug.Log ("2");
				break;

			case 3:
				g_SleepingPanel_Towels_Aristocrat_Detail.SetActive(false);
				g_SleepingPanel_Towels_Indulgence_Detail.SetActive(true);
				g_SleepingPanel_Towels_Supreme_Detail.SetActive(false);
				Debug.Log ("3");
				break;

			case 4:
				g_SleepingPanel_Towels_Aristocrat_Detail.SetActive(false);
				g_SleepingPanel_Towels_Indulgence_Detail.SetActive(false);
				g_SleepingPanel_Towels_Supreme_Detail.SetActive(true);
				Debug.Log ("4");
				break;

			case 5:
				g_SleepingPanel_Color_Bar_Aristocrat.SetActive (true);
				Debug.Log ("5");
				break;

			case 6:
				g_SleepingPanel_Color_Bar_Indulgence.SetActive(true);
				Debug.Log ("6");
				break;

			case 7:
				g_SleepingPanel_Color_Bar_Supreme.SetActive(true);
				Debug.Log ("7");
				break;
			}
	}

	public void aristocrat_color_change(int a_color_id)
	{
		switch (a_color_id) 
		{
			case 1:
				aristrocrat_main.image.sprite = Resources.Load<Sprite> ("Aristocrat Peacock Teal");
				break;

			case 2:
				aristrocrat_main.image.sprite = Resources.Load<Sprite> ("Aristocrat Green");
				break;

			case 3:
				aristrocrat_main.image.sprite = Resources.Load<Sprite> ("Aristocrat Butter");
				break;

			case 4:
				aristrocrat_main.image.sprite = Resources.Load<Sprite> ("Aristocrat Dk.Tangerine");
				break;

			case 5:
				aristrocrat_main.image.sprite = Resources.Load<Sprite> ("Aristocrat Charcoal");
				break;

			case 6:
				aristrocrat_main.image.sprite = Resources.Load<Sprite> ("Aristocrat Camel");
				break;

			case 7:
				aristrocrat_main.image.sprite = Resources.Load<Sprite> ("Aristocrat White");
				break;
			}
		}

	public void indulgence_color_change(int a_color_id)
	{
		switch (a_color_id) 
		{
		case 1:
			indulgence_main.image.sprite = Resources.Load<Sprite> ("Indulgence Yellow");
			break;

		case 2:
			indulgence_main.image.sprite = Resources.Load<Sprite> ("Indulgence Emerald Green");
			break;

		case 3:
			indulgence_main.image.sprite = Resources.Load<Sprite> ("Indulgence Beige");
			break;

		case 4:
			indulgence_main.image.sprite = Resources.Load<Sprite> ("Indulgence Rose");
			break;

		case 5:
			indulgence_main.image.sprite = Resources.Load<Sprite> ("Indulgence Cream");
			break;

		case 6:
			indulgence_main.image.sprite = Resources.Load<Sprite> ("Indulgence Grey");
			break;

		case 7:
			indulgence_main.image.sprite = Resources.Load<Sprite> ("Indulgence White");
			break;
		}
	}

	public void supreme_color_change(int a_color_id)
	{
		switch (a_color_id) 
		{
		case 1:
			supreme_main.image.sprite = Resources.Load<Sprite> ("Supreme Iris");
			break;

		case 2:
			supreme_main.image.sprite = Resources.Load<Sprite> ("Supreme Eucalyptus");
			break;

		case 3:
			supreme_main.image.sprite = Resources.Load<Sprite> ("Supreme Charcoal");
			break;

		case 4:
			supreme_main.image.sprite = Resources.Load<Sprite> ("Supreme Aquifer");
			break;

		case 5:
			supreme_main.image.sprite = Resources.Load<Sprite> ("Supreme Elder Berry");
			break;

		case 6:
			supreme_main.image.sprite = Resources.Load<Sprite> ("Supreme Stone");
			break;

		case 7:
			supreme_main.image.sprite = Resources.Load<Sprite> ("Supreme Creme");
			break;

		case 8:
			supreme_main.image.sprite = Resources.Load<Sprite> ("Supreme White");
			break;
		}
	}
		
	public void show_Bedding_main_page()
	{
			//hide_count = 1;
			g_SleepingPanel.SetActive(true);
			g_SleepingPanel_Towels_main.SetActive (false);
			g_SleepingPanel_Towels.SetActive(false);
			g_SleepingPanel_Towels_Aristocrat_Detail.SetActive(false);
			g_SleepingPanel_Towels_Indulgence_Detail.SetActive(false);
			g_SleepingPanel_Towels_Supreme_Detail.SetActive(false);
			g_SleepingPanel_Color_Bar_Aristocrat.SetActive(false);
			g_SleepingPanel_Color_Bar_Indulgence.SetActive(false);
			g_SleepingPanel_Color_Bar_Supreme.SetActive(false);
	}

	public void show_Towels_main_page()
	{
		g_SleepingPanel.SetActive(false);
		g_SleepingPanel_Towels.SetActive(true);
		g_SleepingPanel_Towels_Aristocrat_Detail.SetActive(false);
		g_SleepingPanel_Towels_Indulgence_Detail.SetActive(false);
		g_SleepingPanel_Towels_Supreme_Detail.SetActive(false);
		g_SleepingPanel_Color_Bar_Aristocrat.SetActive(false);
		g_SleepingPanel_Color_Bar_Indulgence.SetActive(false);
		g_SleepingPanel_Color_Bar_Supreme.SetActive(false);
	}
	public void f_DisableAll()
	{
		g_SleepingPanel.SetActive(false);
		g_SleepingPanel_Towels_main.SetActive (false);
		g_SleepingPanel_Towels.SetActive(false);
		g_SleepingPanel_Towels_Aristocrat_Detail.SetActive(false);
		g_SleepingPanel_Towels_Indulgence_Detail.SetActive(false);
		g_SleepingPanel_Towels_Supreme_Detail.SetActive(false);
		g_SleepingPanel_Color_Bar_Aristocrat.SetActive(false);
		g_SleepingPanel_Color_Bar_Indulgence.SetActive(false);
		g_SleepingPanel_Color_Bar_Supreme.SetActive(false);
	}
	public void toggle_color_bar_aristocrat()
	{
		if (toggle_bar_aristocrat == false) 
		{
			g_SleepingPanel_Color_Bar_Aristocrat.SetActive(true);
			toggle_bar_aristocrat = true;
			C_TowelsColorButtonUI.Instance.g_Aristocrat.gameObject.SetActive (true);
		}
		else if(toggle_bar_aristocrat == true) 
		{
			g_SleepingPanel_Color_Bar_Aristocrat.SetActive(false);
			toggle_bar_aristocrat = false;
			C_TowelsColorButtonUI.Instance.g_Aristocrat.gameObject.SetActive (false);
		}
	}

	public void toggle_color_bar_indulgence()
	{
		if (toggle_bar_indulgence == false) 
		{
			g_SleepingPanel_Color_Bar_Indulgence.SetActive(true);
			toggle_bar_indulgence = true;
			C_TowelsColorButtonUI.Instance.g_Indulgence.gameObject.SetActive (true);
		}
		else if(toggle_bar_indulgence == true) 
		{
			g_SleepingPanel_Color_Bar_Indulgence.SetActive(false);
			toggle_bar_indulgence = false;
			C_TowelsColorButtonUI.Instance.g_Indulgence.gameObject.SetActive (false);
		}
	}

	public void toggle_color_bar_supreme()
	{
		if (toggle_bar_supreme == false) 
		{
			g_SleepingPanel_Color_Bar_Supreme.SetActive(true);
			toggle_bar_supreme = true;
			C_TowelsColorButtonUI.Instance.g_Supreme.gameObject.SetActive (true);
		}
		else if(toggle_bar_supreme == true) 
		{
			g_SleepingPanel_Color_Bar_Supreme.SetActive(false);
			toggle_bar_supreme = false;
			C_TowelsColorButtonUI.Instance.g_Supreme.gameObject.SetActive (false);
		}
	}
}


