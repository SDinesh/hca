﻿using UnityEngine;
using System.Collections;

public class C_DiningCombination : MonoBehaviour {

	private static C_DiningCombination instance;
	public static C_DiningCombination Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_DiningCombination> ();

			return instance;
		}
	}

	public GameObject ColourBtn;
	//public GameObject TableTopColourBtn;
	public GameObject Btn360;
	public bool isCombination;


	public void f_ActivePage(int l_Val){
		if (l_Val == 0) {
			ColourBtn.SetActive (false);
			Btn360.SetActive (false);
			isCombination = true;
		} else {
			ColourBtn.SetActive (true);
			Btn360.SetActive (true);
			isCombination = false;
		}
	}

	public void f_SetCombinaton(int l_Val){
		if (l_Val == 0) {
			C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [0];
			C_Colours_Hardcoded.Instance.ChairMats [4].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [2];
			C_Colours_Hardcoded.Instance.ChairMats [5].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [2];
		}else if (l_Val == 1) {
			C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [2];
			C_Colours_Hardcoded.Instance.ChairMats [6].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [7];
			C_Colours_Hardcoded.Instance.ChairMats [7].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [7];
			C_Colours_Hardcoded.Instance.ChairMats [8].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [7];
		}else if (l_Val == 2) {
			C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [4];
			C_Colours_Hardcoded.Instance.ChairMats [0].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [8];
		}else if (l_Val == 3) {
			C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [3];
			C_Colours_Hardcoded.Instance.ChairMats [2].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [3];
		}else if (l_Val == 4) {
			C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [3];
			C_Colours_Hardcoded.Instance.ChairMats [1].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [0];
		}else if (l_Val == 5) {
			C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [3];
			C_Colours_Hardcoded.Instance.ChairMats [4].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [2];
			C_Colours_Hardcoded.Instance.ChairMats [5].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [2];
		}else if (l_Val == 6) {
			C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [2];
			C_Colours_Hardcoded.Instance.ChairMats [6].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [7];
			C_Colours_Hardcoded.Instance.ChairMats [7].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [7];
			C_Colours_Hardcoded.Instance.ChairMats [8].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [7];
		}else if (l_Val == 7) {
			C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [1];
			C_Colours_Hardcoded.Instance.ChairMats [0].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [8];
		}else if (l_Val == 8) {
			C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [0];
			C_Colours_Hardcoded.Instance.ChairMats [3].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [6];
		}
	}
}
