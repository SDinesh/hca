﻿using UnityEngine;
using System.Collections;

public class C_MediaPlayerManager : MonoBehaviour {
	public RenderHeads.Media.AVProVideo.MediaPlayer g_MediaPlayer;

	// Use this for initialization

	int g_FileNum = 0;
	public int g_TotalVideoCount;
	public string g_FolderPath = "";

	void OnEnable () {
		g_FileNum = 1;
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) 
		{
			g_TotalVideoCount = 36;
			g_FolderPath = "C:\\avpro\\Dining\\";
		}
		else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) 
		{
			g_TotalVideoCount = 38;
			g_FolderPath = "C:\\avpro\\Living\\";
		}
		else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) 
		{
			g_TotalVideoCount = 32;
			g_FolderPath = "C:\\avpro\\Sleeping\\";
		}
		else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Entrance) 
		{
			g_TotalVideoCount = 38;
			g_FolderPath = "C:\\avpro\\Entrance\\";
		}
		g_MediaPlayer.m_VideoPath = g_FolderPath+g_FileNum+".mp4";
		g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
		print (g_MediaPlayer.m_VideoPath);
	
	}
	
	// Update is called once per frame
	void Update () {

		if (g_MediaPlayer.Control.IsFinished ())
		{
			g_FileNum++;
			if (g_FileNum > g_TotalVideoCount)
			{
				g_FileNum = 1;
			}
			g_MediaPlayer.m_VideoPath = g_FolderPath+g_FileNum+".mp4";

			if (!System.IO.File.Exists(g_MediaPlayer.m_VideoPath))
			{
				g_FileNum = 1;
				g_MediaPlayer.m_VideoPath = g_FolderPath+g_FileNum+".mp4";
			}
			g_MediaPlayer.OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer.FileLocation.AbsolutePathOrURL,g_MediaPlayer.m_VideoPath,true);
		}
	
	}
}
