﻿using UnityEngine;
using System.Collections;

public class C_GarbageCollector : MonoBehaviour {

	float g_TimeKeeper;

	void Update(){
		if (Time.frameCount % 30 == 0) {
			System.GC.Collect ();
		}
		g_TimeKeeper += Time.deltaTime;
		if (g_TimeKeeper >= (60.0f * 60.0f)) {
			Resources.UnloadUnusedAssets ();
		}
	}
}
