﻿using UnityEngine;
using System.Collections;

public class C_Continental_Wardrobe : MonoBehaviour {

	private static C_Continental_Wardrobe instance;
	public static C_Continental_Wardrobe Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_Continental_Wardrobe> ();

			return instance;
		}
	}

	public GameObject g_TwoDoor;
	public GameObject g_SingleDoor;
	public GameObject g_CurrentlySelectedWardrobe;
	public GameObject g_ContinentalDoorsBtnHolder;

	public void f_Activate()
	{
		if ((string.Compare (g_CurrentlySelectedWardrobe.name, "2doorwardrobewithprops01(Clone)") == 0) || (string.Compare (g_CurrentlySelectedWardrobe.name, "2doorwardrobewithprops02(Clone)") == 0) || (string.Compare (g_CurrentlySelectedWardrobe.name, "2doorwardrobewithprops03(Clone)") == 0)) {
			g_TwoDoor.SetActive (true);
			g_SingleDoor.SetActive (false);
		} else {
			g_SingleDoor.SetActive (true);
			g_TwoDoor.SetActive (false);
		}
	}

	public void f_DoorsSelected(int l_Val){
		f_TurnoffDoors ();
		g_CurrentlySelectedWardrobe.transform.GetChild (l_Val).gameObject.SetActive (true);
	}

	public void f_TurnoffDoors(){
		g_CurrentlySelectedWardrobe.transform.GetChild (1).gameObject.SetActive (false);
		g_CurrentlySelectedWardrobe.transform.GetChild (2).gameObject.SetActive (false);
		g_CurrentlySelectedWardrobe.transform.GetChild (3).gameObject.SetActive (false);
		g_CurrentlySelectedWardrobe.transform.GetChild (4).gameObject.SetActive (false);
		if (g_TwoDoor.activeSelf) {
			g_CurrentlySelectedWardrobe.transform.GetChild (5).gameObject.SetActive (false);
			g_CurrentlySelectedWardrobe.transform.GetChild (6).gameObject.SetActive (false);
		}
	}
}
