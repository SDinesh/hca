﻿using UnityEngine;
using System.Collections;

public class C_GlobalVariables : MonoBehaviour
{
	private static C_GlobalVariables _instance;
	public static C_GlobalVariables instance
	{
		get
		{
			if(_instance == null)
			{
				GameObject l_GameManagerObj = GameObject.Find("ApplicationManager");
				_instance = l_GameManagerObj.GetComponent<C_GlobalVariables>();
			}
			return _instance;
		}
	}
	//----------------------------------------------------------------------------

	public E_ApplicationPage g_CurrentActivePage;
	public E_Services g_CurrentActiveServices;
	public E_Area g_SelectedArea;
	public E_View g_View;
	public float g_LastTouchTime;


	void Awake()
	{

	}

}

public enum E_Language
{
	ENGLISH,
	ARABIC
}

public enum E_ApplicationPage
{
	SelectArea,
	SplashScreen,
	ScreenSaver,
	LanguageSelect,
	MainMenuPage,
	MainPageUI,
	StoreMap,
	Catalogue,
	ModularProgram,
	OffersPage,
	OtherServices
}

public enum E_Services
{
	StoreMap,
	Catalogue,
	ModularProgram,
	Offers,
	OtherServices,
	Shukran,
	Website,
	SocialMedia,
	ComingSoon,
	None
}

public enum E_SocialMedia
{
	Facebook,
	Twitter,
	Instagram,
	Youtube
}

public enum E_Area
{
	Entrance,
	Living,
	Decorating,
	Dining,
	Dreamroom,
	Sleeping,
	Playing,
	CustomerService,
	Prayerroom,
	CashTills,
	None
}

public enum E_View
{
	Preview,
	Compose,
	None
}
