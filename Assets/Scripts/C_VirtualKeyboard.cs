﻿using UnityEngine;
using System.Collections;

public class C_VirtualKeyboard : MonoBehaviour {

	private static C_VirtualKeyboard _instance;
	public static C_VirtualKeyboard instance
	{
		get
		{
			if(_instance == null)
			{
				GameObject l_GameManagerObj = GameObject.Find("VirtualKeyboard");
				_instance = l_GameManagerObj.GetComponent<C_VirtualKeyboard>();
			}
			return _instance;
		}
	}
	//----------------------------------------------------------------------------
	public string g_TypedString;
	string g_Substring = "";
	Animator KeyBoardAnimator;


	void Start(){
		KeyBoardAnimator = GetComponent<Animator> ();
	}

	void OnEnable ()
	{
		g_TypedString = "";
	}
	
	public void f_OnCharaterBtnClick(string l_Str)
	{
		g_TypedString += l_Str;
	}

	public void f_OnSpacebarBtnClick()
	{
		g_TypedString += " ";
	}

	public void f_OnBackspaceBtnClick()
	{
		if(g_TypedString.Length > 0)
		{
			g_Substring = g_TypedString.Substring (0, g_TypedString.Length - 1);
			g_TypedString = g_Substring;
		}
	}

	public void f_CloseClick()
	{
		KeyBoardAnimator.SetBool ("isKeyboard", false);
		Invoke ("f_DisableKeyboard", 2.0f);
	}

	public void f_OnEnterBtnClick()
	{
	}

	void f_DisableKeyboard(){
		C_StoreMapUIManager.Instance.f_DisableKeyboard ();
	}
}
