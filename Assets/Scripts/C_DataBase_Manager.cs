﻿using UnityEngine;
using System.Collections;
using System.Data;
using System;
using Mono.Data.Sqlite;
using UnityEngine.UI;

public class C_DataBase_Manager : MonoBehaviour {

	private static C_DataBase_Manager instance;
	public static C_DataBase_Manager Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_DataBase_Manager> ();

			return instance;
		}
	}

	string g_DBConnectionString;

	[Header("Scene Referenences")]
	public Text g_ProductTitle;
	public Text g_Dimensions;
	public Text g_Price;
	public Text g_Description;

	public string g_DB_ProductTitle;
	public string g_DB_Price;
	public string g_DB_ImageURL;
	public float g_PriceCalc;

	void Start () {
		g_DBConnectionString = "URI=file:" + Application.streamingAssetsPath + "/NewHCDB.sqlite";

	}

	public void f_GetData(int l_ID)
	{
		if (C_GlobalVariables.instance.g_View == E_View.Preview) {
			for (int l_i = 0; l_i < C_AssetPreviewManager.Instance.g_SelectedAssetArray.Length; l_i++) {
				C_AssetPreviewManager.Instance.g_SelectedAssetArray [l_i].SetActive (false);
			}
		}
		using (IDbConnection l_dbConnection = new SqliteConnection (g_DBConnectionString)) {

			//TestingPurpose
			return;


			if(l_dbConnection.State == ConnectionState.Open)
			l_dbConnection.Open ();

			using (IDbCommand l_dbCommand = l_dbConnection.CreateCommand ()) {
				string l_SQLQuery = String.Format("SELECT * FROM Home_Center_DB WHERE Item_ID = \"{0}\" ",l_ID);
				l_dbCommand.CommandText = l_SQLQuery;

				using (IDataReader l_Reader = l_dbCommand.ExecuteReader ()) {
					while (l_Reader.Read ()) {
						C_AssetBundle_Downloader.Instance.g_AssetName = l_Reader.GetString (11).ToString();
						if (C_GlobalVariables.instance.g_View == E_View.Preview) {
							g_ProductTitle.text = l_Reader.GetString (3);
							g_Price.text = "AED " + l_Reader.GetFloat (5).ToString ();
							g_Description.text = l_Reader.GetString (14);
							C_AssetBundle_Downloader.Instance.f_StartDownloadingAssetBundle ();
						} else {
							g_DB_ProductTitle = l_Reader.GetString (3);
							g_DB_Price = "AED " + l_Reader.GetFloat (5).ToString ();
							g_PriceCalc = l_Reader.GetFloat (5);
						}
					}

					l_dbConnection.Close ();
					l_Reader.Close ();
				}
			}
		}
	}
}
