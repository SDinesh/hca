﻿using UnityEngine;
using System.Collections;

public class C_ServicesVideoPlayController : MonoBehaviour {

	private static C_ServicesVideoPlayController instance;
	public static C_ServicesVideoPlayController Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_ServicesVideoPlayController> ();

			return instance;
		}
	}

	public GameObject OmniChannelVideo;
	public GameObject ServicesPage;
	public GameObject ServiceVideoCloseBtn;
	public GameObject ServicesVideoPlayer;
	public GameObject MainPanelUI;

	public void f_PlayServicesVideo(int l_VideoObj)
	{
		ServicesPage.SetActive (false);
		MainPanelUI.SetActive (false);
		ServiceVideoCloseBtn.SetActive (true);
		ServicesVideoPlayer.SetActive (true);
		C_ServiceVideoPlayer.Instance.f_PlayVideo (l_VideoObj);
	}

	public void f_CloseBtn()
	{
		ServicesPage.SetActive (true);
		MainPanelUI.SetActive (true);
		ServiceVideoCloseBtn.SetActive (false);
		ServicesVideoPlayer.SetActive (false);
		OmniChannelVideo.SetActive (false);
	}

	public void f_OmniChannel(){
		OmniChannelVideo.SetActive (true);
		Invoke ("f_CloseVideoPanel", 10f);
	}

	public void f_CloseVideoPanel(){
		f_CloseBtn ();
	}

	void Update () {

		if (ServicesVideoPlayer.activeInHierarchy) 
		{
			if (C_ServiceVideoPlayer.Instance.g_MediaPlayer.Control.IsFinished ())
			{
				f_CloseBtn ();
			}
		}
	}
}
