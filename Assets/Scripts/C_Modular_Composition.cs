﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class C_Modular_Composition : MonoBehaviour {

	private static C_Modular_Composition instance;
	public static C_Modular_Composition Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_Modular_Composition> ();

			return instance;
		}
	}

	public GameObject[] g_StellarBedProgPrefabList;
	public GameObject[] g_WardRobeProgPrefabList;
	public GameObject[] g_ProductListBtn;
	public GameObject[] g_PrefabListTable;
	public GameObject[] g_PrefabListDiningChairs;
	public GameObject[] g_PrefabListSofas;
	public GameObject[] g_AssetsInScene;
	public GameObject g_SelectProduct_Dining;
	public GameObject g_SelectProduct_Sleeping;
	public GameObject g_SelectProductPanel_Dining_Tables;
	public GameObject g_SelectProductPanel_Dining_Chairs;
	public GameObject g_SelectProduct_Living;
	public GameObject CloseAddBtn;
	public GameObject AddProductsBtn;
	public GameObject g_AssetHolderParent;
	public GameObject g_Headboard_King;
	public GameObject g_Base_King;
	public GameObject g_Headboard_Queen;
	public GameObject g_Base_Queen;
	public GameObject g_Headboard_Single;
	public GameObject g_Base_Single;
	public GameObject g_Drawer;
	public GameObject g_SelectSize;
	public GameObject g_Continental_Products;
	public Text g_TotalPrice;
	public float[] g_Price;
	public float TotalPrice;

	public int g_StellarSize;

	public string g_AssetName;

	Vector3[] g_InitialChairPositions;
	Vector3 g_InitialChairRotation;
	Vector3 g_InitialWardrobeRotation;
	Vector3 g_WardrobePosition;
	public int g_ProductCounter;
	int g_ChairCount;

	void Start(){
		g_Price = new float[20];
		g_InitialChairPositions = new Vector3[13];
		g_InitialChairPositions [0] = Vector3.zero;
		g_InitialChairPositions [1] = new Vector3 (-3.0f, 0.0f, 2.0f);
		g_InitialChairPositions [2] = new Vector3 (-1.0f, 0.0f, 2.0f);
		g_InitialChairPositions [3] = new Vector3 (1.0f, 0.0f, 2.0f);
		g_InitialChairPositions [4] = new Vector3 (3.0f, 0.0f, 2.0f);
		g_InitialChairPositions [5] = new Vector3 (5.0f, 0.0f, 2.0f);
		g_InitialChairPositions [6] = new Vector3 (7.0f, 0.0f, 2.0f);
		g_InitialChairPositions [7] = new Vector3 (7.5f, 0.0f, 4.0f);
		g_InitialChairPositions [8] = new Vector3 (5.5f, 0.0f, 4.0f);
		g_InitialChairPositions [9] = new Vector3 (3.5f, 0.0f, 4.0f);
		g_InitialChairPositions [10] = new Vector3 (1.5f, 0.0f, 4.0f);
		g_InitialChairPositions [11] = new Vector3 (-0.5f, 0.0f, 4.0f);
		g_InitialChairPositions [12] = new Vector3 (-2.5f, 0.0f, 4.0f);

		g_InitialChairRotation = new Vector3 (0.0f, 180.0f, 0.0f);

		g_WardrobePosition = new Vector3 (3.5f,0.0f,0.0f);

		g_InitialWardrobeRotation = new Vector3 (-10.0f, 180.0f, 0.0f);

		f_ClearComposition ();
	}

	public void f_ClearComposition(){
		f_ResetAllPencilSelect ();
		f_DisableAllColourPanels ();
		g_TotalPrice.text = "";
		TotalPrice = 0.0f;
		AddProductsBtn.SetActive (true);
		for (int l_i = 0; l_i < g_ProductListBtn.Length; l_i++) {
			g_ProductListBtn [l_i].SetActive (false);
		}
		g_ProductCounter = 0;

		for (int l_i = 0; l_i < g_AssetsInScene.Length; l_i++) {
			if (g_AssetsInScene [l_i] != null) {
				Destroy (g_AssetsInScene [l_i]);
			}
		}
	}

	public void f_SelectSizeBtn(){
		g_SelectProduct_Sleeping.SetActive (true);
		g_SelectSize.SetActive (true);
		f_ClearComposition ();
	}

	public void f_SelectSize(int l_Val){
		g_StellarSize = l_Val;
		f_CloseProductsSelection ();
	}

	public void f_AddedProductBtn()
	{
		AddProductsBtn.SetActive (false);
		CloseAddBtn.SetActive (true);
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
			g_SelectProduct_Dining.SetActive (true);
			if (g_AssetsInScene [0] == null) {
				g_SelectProductPanel_Dining_Tables.SetActive (true);
			} else {
				g_SelectProductPanel_Dining_Chairs.SetActive (true);
			}
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			g_SelectProduct_Living.SetActive (true);
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			g_SelectProduct_Sleeping.SetActive (true);
			if (C_StellarBed_Manager.Instance.g_SleepingType == 0) {
				if (g_AssetsInScene [0] == null) {
					if (g_StellarSize == 0) {
						g_Headboard_King.SetActive (true);
					} else if (g_StellarSize == 1) {
						g_Headboard_Queen.SetActive (true);
					} else {
						g_Headboard_Single.SetActive (true);
					}
				} else if (g_AssetsInScene [1] == null) {
					if (g_StellarSize == 0) {
						g_Base_King.SetActive (true);
					} else if (g_StellarSize == 1) {
						g_Base_Queen.SetActive (true);
					} else {
						g_Base_Single.SetActive (true);
					}
				} else {
					g_Drawer.SetActive (true);
				}
			} else if (C_StellarBed_Manager.Instance.g_SleepingType == 1) {
				g_Continental_Products.SetActive (true);
			} else if (C_StellarBed_Manager.Instance.g_SleepingType == 2) {
			}
		}
	}

	public void f_CloseProductsSelection()
	{
		AddProductsBtn.SetActive (true);
		CloseAddBtn.SetActive (false);
		g_SelectProductPanel_Dining_Tables.SetActive (false);
		g_SelectProductPanel_Dining_Chairs.SetActive (false);
		g_SelectProduct_Dining.SetActive (false);
		g_SelectProduct_Living.SetActive (false);
		g_Headboard_King.SetActive (false);
		g_Base_King.SetActive (false);
		g_Headboard_Queen.SetActive (false);
		g_Base_Queen.SetActive (false);
		g_Headboard_Single.SetActive (false);
		g_Base_Single.SetActive (false);
		g_Drawer.SetActive (false);
		g_SelectSize.SetActive (false);
		g_SelectProduct_Sleeping.SetActive (false);
		g_Continental_Products.SetActive (false);
	}

	void f_AddedProductUIControl()
	{
		g_ProductListBtn [g_ProductCounter].transform.GetChild (4).transform.GetChild(1).GetComponent<Text> ().text = C_DataBase_Manager.Instance.g_DB_ProductTitle;
		g_ProductListBtn [g_ProductCounter].transform.GetChild (4).transform.GetChild (2).GetComponent<Text> ().text = C_DataBase_Manager.Instance.g_DB_Price;
		g_Price [g_ProductCounter] = C_DataBase_Manager.Instance.g_PriceCalc;
		f_PriceTotal ();
		f_CloseProductsSelection ();
		g_ProductCounter++;
		if (g_ProductCounter >= 4 && C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			AddProductsBtn.SetActive (false);
		}

	}


	void f_AddedProductUIControl_New(int l_Val)
	{
		g_ProductListBtn [l_Val].transform.GetChild (4).transform.GetChild(1).GetComponent<Text> ().text = C_DataBase_Manager.Instance.g_DB_ProductTitle;
		g_ProductListBtn [l_Val].transform.GetChild (4).transform.GetChild (2).GetComponent<Text> ().text = C_DataBase_Manager.Instance.g_DB_Price;
		g_Price [l_Val] = C_DataBase_Manager.Instance.g_PriceCalc;
		f_PriceTotal ();
		f_CloseProductsSelection ();
		g_ProductCounter++;
		if (g_ProductCounter > g_ChairCount && C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
			AddProductsBtn.SetActive (false);
		}
		if (g_ProductCounter >= 13 && C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			AddProductsBtn.SetActive (false);
		}if (g_ProductCounter >= g_WardRobeProgPrefabList.Length && C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			AddProductsBtn.SetActive (false);
		}
	}

	public void f_AddProduct_New(){
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
			if (g_AssetsInScene [0] == null) {
				for (int l_j = 0; l_j < g_PrefabListTable.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListTable [l_j].name) == 0) {
						g_ProductListBtn [0].SetActive (true);

						if (string.Compare (g_PrefabListTable [l_j].name, "roundtabletop") == 0) {
							g_AssetsInScene [0] = Instantiate (g_PrefabListTable [l_j], new Vector3 (1.25f, 0.0f, 0.0f), Quaternion.Euler (-90.0f, 0.0f, 0.0f)) as GameObject;
							g_AssetsInScene [0].transform.parent = g_AssetHolderParent.transform;
							g_ChairCount = 4;
						} else {
							g_AssetsInScene [0] = Instantiate (g_PrefabListTable [l_j], new Vector3 (1.25f, 0.0f, 0.0f), Quaternion.identity) as GameObject;
							g_AssetsInScene [0].transform.parent = g_AssetHolderParent.transform;
							g_ChairCount = 6;
						}
						f_AddedProductUIControl_New (0);
					}
				}
			} else if (g_AssetsInScene [1] == null && g_ChairCount >= 4) {
				for (int l_j = 0; l_j < g_PrefabListDiningChairs.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListDiningChairs [l_j].name) == 0) {
						g_ProductListBtn [1].SetActive (true);
						g_AssetsInScene [1] = Instantiate (g_PrefabListDiningChairs [l_j], new Vector3 (-3.0f, 0.0f, 5.0f), Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [1].transform.parent = g_AssetHolderParent.transform;
						g_AssetName = g_AssetsInScene [1].name;
						f_AddedProductUIControl_New (1);
					}
				}
			} else if (g_AssetsInScene [2] == null && g_ChairCount >= 4) {
				for (int l_j = 0; l_j < g_PrefabListDiningChairs.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListDiningChairs [l_j].name) == 0) {
						g_ProductListBtn [2].SetActive (true);
						g_AssetsInScene [2] = Instantiate (g_PrefabListDiningChairs [l_j], new Vector3 (-1.0f, 0.0f, 5.0f), Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [2].transform.parent = g_AssetHolderParent.transform;
						g_AssetName = g_AssetsInScene [2].name;
						f_AddedProductUIControl_New (2);
					}
				}
			} else if (g_AssetsInScene [3] == null && g_ChairCount >= 4) {
				for (int l_j = 0; l_j < g_PrefabListDiningChairs.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListDiningChairs [l_j].name) == 0) {
						g_ProductListBtn [3].SetActive (true);
						g_AssetsInScene [3] = Instantiate (g_PrefabListDiningChairs [l_j], new Vector3 (1.0f, 0.0f, 5.0f), Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [3].transform.parent = g_AssetHolderParent.transform;
						g_AssetName = g_AssetsInScene [3].name;
						f_AddedProductUIControl_New (3);
					}
				}
			} else if (g_AssetsInScene [4] == null && g_ChairCount >= 4) {
				for (int l_j = 0; l_j < g_PrefabListDiningChairs.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListDiningChairs [l_j].name) == 0) {
						g_ProductListBtn [4].SetActive (true);
						g_AssetsInScene [4] = Instantiate (g_PrefabListDiningChairs [l_j], new Vector3 (3.0f, 0.0f, 5.0f), Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [4].transform.parent = g_AssetHolderParent.transform;
						g_AssetName = g_AssetsInScene [4].name;
						f_AddedProductUIControl_New (4);
					}
				}
			} else if (g_AssetsInScene [5] == null && g_ChairCount >= 6) {
				for (int l_j = 0; l_j < g_PrefabListDiningChairs.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListDiningChairs [l_j].name) == 0) {
						g_ProductListBtn [5].SetActive (true);
						g_AssetsInScene [5] = Instantiate (g_PrefabListDiningChairs [l_j], new Vector3 (5.0f, 0.0f, 5.0f), Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [5].transform.parent = g_AssetHolderParent.transform;
						g_AssetName = g_AssetsInScene [5].name;
						f_AddedProductUIControl_New (5);
					}
				}
			} else if (g_AssetsInScene [6] == null && g_ChairCount >= 6) {
				for (int l_j = 0; l_j < g_PrefabListDiningChairs.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListDiningChairs [l_j].name) == 0) {
						g_ProductListBtn [6].SetActive (true);
						g_AssetsInScene [6] = Instantiate (g_PrefabListDiningChairs [l_j], new Vector3 (7.0f, 0.0f, 5.0f), Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [6].transform.parent = g_AssetHolderParent.transform;
						g_AssetName = g_AssetsInScene [6].name;
						f_AddedProductUIControl_New (6);
					}
				}
			} else {
				f_CloseProductsSelection ();
			}
			f_SelectAssetForColour ();
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			if (g_AssetsInScene [0] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [0].SetActive (true);
						g_AssetsInScene [0] = Instantiate (g_PrefabListSofas [l_j], new Vector3 (1.25f, 0.0f, 0.0f), Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [0].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (0);
					}
				}
			} else if (g_AssetsInScene [1] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [1].SetActive (true);
						g_AssetsInScene [1] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [1], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [1].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (1);
					}
				}
			} else if (g_AssetsInScene [2] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [2].SetActive (true);
						g_AssetsInScene [2] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [2], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [2].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (2);
					}
				}
			} else if (g_AssetsInScene [3] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [3].SetActive (true);
						g_AssetsInScene [3] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [3], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [3].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (3);
					}
				}
			} else if (g_AssetsInScene [4] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [4].SetActive (true);
						g_AssetsInScene [4] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [4], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [4].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (4);
					}
				}
			} else if (g_AssetsInScene [5] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [5].SetActive (true);
						g_AssetsInScene [5] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [5], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [5].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (5);
					}
				}
			} else if (g_AssetsInScene [6] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [6].SetActive (true);
						g_AssetsInScene [6] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [6], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [6].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (6);
					}
				}
			} else if (g_AssetsInScene [7] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [7].SetActive (true);
						g_AssetsInScene [7] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [7], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [7].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (7);
					}
				}
			} else if (g_AssetsInScene [8] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [8].SetActive (true);
						g_AssetsInScene [8] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [8], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [8].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (8);
					}
				}
			} else if (g_AssetsInScene [9] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [9].SetActive (true);
						g_AssetsInScene [9] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [9], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [9].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (9);
					}
				}
			} else if (g_AssetsInScene [10] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [10].SetActive (true);
						g_AssetsInScene [10] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [10], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [10].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (10);
					}
				}
			} else if (g_AssetsInScene [11] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [11].SetActive (true);
						g_AssetsInScene [11] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [11], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [11].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (11);
					}
				}
			} else if (g_AssetsInScene [12] == null) {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						g_ProductListBtn [12].SetActive (true);
						g_AssetsInScene [12] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [12], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [12].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl_New (12);
					}
				}
			} else {
				f_CloseProductsSelection ();
			}
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			if (C_StellarBed_Manager.Instance.g_SleepingType == 1) {
				if (g_AssetsInScene [0] == null) {
					for (int l_j = 0; l_j < g_WardRobeProgPrefabList.Length; l_j++) {
						if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_WardRobeProgPrefabList [l_j].name) == 0) {
							g_ProductListBtn [0].SetActive (true);
							g_AssetsInScene [0] = Instantiate (g_WardRobeProgPrefabList [l_j], g_WardrobePosition, Quaternion.Euler (g_InitialWardrobeRotation)) as GameObject;
							g_AssetsInScene [0].transform.parent = g_AssetHolderParent.transform;
							C_Continental_Wardrobe.Instance.g_CurrentlySelectedWardrobe = g_AssetsInScene [0];
							C_Continental_Wardrobe.Instance.f_Activate ();
							f_AddedProductUIControl_New (0);
						}
					}
				} else if (g_AssetsInScene [1] == null) {
					for (int l_j = 0; l_j < g_WardRobeProgPrefabList.Length; l_j++) {
						if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_WardRobeProgPrefabList [l_j].name) == 0) {
							g_ProductListBtn [1].SetActive (true);
							g_AssetsInScene [1] = Instantiate (g_WardRobeProgPrefabList [l_j], g_WardrobePosition, Quaternion.Euler (g_InitialWardrobeRotation)) as GameObject;
							g_AssetsInScene [1].transform.parent = g_AssetHolderParent.transform;
							C_Continental_Wardrobe.Instance.g_CurrentlySelectedWardrobe = g_AssetsInScene [1];
							C_Continental_Wardrobe.Instance.f_Activate ();
							f_AddedProductUIControl_New (1);
						}
					}
				} else if (g_AssetsInScene [2] == null) {
					for (int l_j = 0; l_j < g_WardRobeProgPrefabList.Length; l_j++) {
						if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_WardRobeProgPrefabList [l_j].name) == 0) {
							g_ProductListBtn [2].SetActive (true);
							g_AssetsInScene [2] = Instantiate (g_WardRobeProgPrefabList [l_j], g_WardrobePosition, Quaternion.Euler (g_InitialWardrobeRotation)) as GameObject;
							g_AssetsInScene [2].transform.parent = g_AssetHolderParent.transform;
							C_Continental_Wardrobe.Instance.g_CurrentlySelectedWardrobe = g_AssetsInScene [2];
							C_Continental_Wardrobe.Instance.f_Activate ();
							f_AddedProductUIControl_New (2);
						}
					}
				} else if (g_AssetsInScene [3] == null) {
					for (int l_j = 0; l_j < g_WardRobeProgPrefabList.Length; l_j++) {
						if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_WardRobeProgPrefabList [l_j].name) == 0) {
							g_ProductListBtn [3].SetActive (true);
							g_AssetsInScene [3] = Instantiate (g_WardRobeProgPrefabList [l_j], g_WardrobePosition, Quaternion.Euler (g_InitialWardrobeRotation)) as GameObject;
							g_AssetsInScene [3].transform.parent = g_AssetHolderParent.transform;
							C_Continental_Wardrobe.Instance.g_CurrentlySelectedWardrobe = g_AssetsInScene [3];
							C_Continental_Wardrobe.Instance.f_Activate ();
							f_AddedProductUIControl_New (3);
						}
					}
				} else if (g_AssetsInScene [4] == null) {
					for (int l_j = 0; l_j < g_WardRobeProgPrefabList.Length; l_j++) {
						if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_WardRobeProgPrefabList [l_j].name) == 0) {
							g_ProductListBtn [4].SetActive (true);
							g_AssetsInScene [4] = Instantiate (g_WardRobeProgPrefabList [l_j], g_WardrobePosition, Quaternion.Euler (g_InitialWardrobeRotation)) as GameObject;
							g_AssetsInScene [4].transform.parent = g_AssetHolderParent.transform;
							C_Continental_Wardrobe.Instance.g_CurrentlySelectedWardrobe = g_AssetsInScene [4];
							C_Continental_Wardrobe.Instance.f_Activate ();
							f_AddedProductUIControl_New (4);
						}
					}
				}
			} else if (C_StellarBed_Manager.Instance.g_SleepingType == 2) {
			}
		}
	}

	public void f_AddProduct()
	{
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
			if (g_AssetsInScene [0] == null) {
				for (int l_j = 0; l_j < g_AssetsInScene.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListTable [l_j].name) == 0) {
						g_ProductListBtn [0].SetActive (true);
						g_AssetsInScene [0] = Instantiate (g_PrefabListTable [l_j], new Vector3 (1.25f, 0.0f, 0.0f), Quaternion.identity) as GameObject;
						g_AssetsInScene [0].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl ();
						if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "roundtabletop") == 0) {
							g_ChairCount = 4;
						} else {
							g_ChairCount = 6;
						}
					}
				}
			} else {
				for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
						if (g_ProductCounter <= g_PrefabListSofas.Length) {
							g_ProductListBtn [g_ProductCounter].SetActive (true);
							g_AssetsInScene [g_ProductCounter] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [g_ProductCounter], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
							g_AssetsInScene [g_ProductCounter].transform.parent = g_AssetHolderParent.transform;
							f_AddedProductUIControl ();
						} else {
							AddProductsBtn.SetActive (false);
						}
					}
				}
			}
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			for (int l_j = 0; l_j < g_PrefabListSofas.Length; l_j++) {
				if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_PrefabListSofas [l_j].name) == 0) {
					if (g_ProductCounter < g_PrefabListSofas.Length) {
						g_ProductListBtn [g_ProductCounter].SetActive (true);
						g_AssetsInScene [g_ProductCounter] = Instantiate (g_PrefabListSofas [l_j], g_InitialChairPositions [g_ProductCounter], Quaternion.Euler (g_InitialChairRotation)) as GameObject;
						g_AssetsInScene [g_ProductCounter].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl ();
					}
				}
			}
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			for (int l_j = 0; l_j < g_WardRobeProgPrefabList.Length; l_j++) {
				if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_WardRobeProgPrefabList [l_j].name) == 0) {
					if (g_ProductCounter < g_WardRobeProgPrefabList.Length) {
						g_ProductListBtn [g_ProductCounter].SetActive (true);
						g_AssetsInScene [g_ProductCounter] = Instantiate (g_WardRobeProgPrefabList [l_j], g_InitialChairPositions [g_ProductCounter], Quaternion.Euler (-90.0f, 0.0f, 180.0f)) as GameObject;
						g_AssetsInScene [g_ProductCounter].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl ();
					} else {
						AddProductsBtn.SetActive (false);
					}
				}
			}
		}
	}

	public void f_DestroyAsset(int l_Asset){
		f_ResetAllPencilSelect ();
		f_DisableAllColourPanels ();
		Destroy (g_AssetsInScene [l_Asset]);
		g_ProductListBtn [l_Asset].SetActive (false);
		if (g_ProductCounter > 0) {
			TotalPrice -= g_Price [l_Asset];
			if (TotalPrice < 0.0f) {
				TotalPrice = 0.0f;
			}
			g_TotalPrice.text = TotalPrice.ToString ();
			g_ProductCounter--;
			if (!AddProductsBtn.activeSelf) {
				AddProductsBtn.SetActive (true);
			}
		}
	}

	public void f_TurnOffAsset(int l_Asset){
		g_AssetsInScene [l_Asset].SetActive (false);
		g_ProductListBtn [l_Asset].transform.GetChild (1).gameObject.SetActive(false);
		g_ProductListBtn [l_Asset].transform.GetChild (2).gameObject.SetActive(true);
	}

	public void f_TurnOnAsset(int l_Asset){
		g_AssetsInScene [l_Asset].SetActive (true);
		g_ProductListBtn [l_Asset].transform.GetChild (1).gameObject.SetActive(true);
		g_ProductListBtn [l_Asset].transform.GetChild (2).gameObject.SetActive(false);
	}

	public void f_StellarHeadBoard()
	{
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			if (g_AssetsInScene [0] == null) {
				for (int l_j = 0; l_j < g_StellarBedProgPrefabList.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_StellarBedProgPrefabList [l_j].name) == 0) {
						g_ProductListBtn [0].SetActive (true);
						g_AssetsInScene [0] = Instantiate (g_StellarBedProgPrefabList [l_j], new Vector3 (0.63f, 1.0f, 0.0f), Quaternion.Euler (0.0f, 160.0f, 0.0f)) as GameObject;
						g_AssetsInScene [0].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl ();
					}
				}
			}
		}
	}

	public void f_Base()
	{
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			if (g_AssetsInScene [1] == null && g_AssetsInScene [0] != null) {
				for (int l_j = 0; l_j < g_StellarBedProgPrefabList.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_StellarBedProgPrefabList [l_j].name) == 0) {
						g_ProductListBtn [1].SetActive (true);
						g_AssetsInScene [1] = Instantiate (g_StellarBedProgPrefabList [l_j], new Vector3 (1.0f, 1.15f, -1.0f), Quaternion.Euler (0.0f, 160.0f, 0.0f)) as GameObject;
						g_AssetsInScene [1].transform.parent = g_AssetHolderParent.transform;
						f_AddedProductUIControl ();
					}
				}
			}
		}
	}

	public void f_Drawer()
	{
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			if (g_AssetsInScene [1] != null && g_AssetsInScene [0] != null) {
				for (int l_j = 0; l_j < g_StellarBedProgPrefabList.Length; l_j++) {
					if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, g_StellarBedProgPrefabList [l_j].name) == 0) {
						if (g_AssetsInScene [2] == null) {
							g_ProductListBtn [2].SetActive (true);
							g_AssetsInScene [2] = Instantiate (g_StellarBedProgPrefabList [l_j], new Vector3 (1.9f, 1.0f, 0.75f), Quaternion.Euler (0.0f, 160.0f, 0.0f)) as GameObject;
							g_AssetsInScene [2].transform.parent = g_AssetHolderParent.transform;
							f_AddedProductUIControl ();
						} else if (g_AssetsInScene [3] == null) {
							g_ProductListBtn [3].SetActive (true);
							g_AssetsInScene [3] = Instantiate (g_StellarBedProgPrefabList [l_j], new Vector3 (-0.7f, 1.0f, -0.1f), Quaternion.Euler (0.0f, 160.0f, 0.0f)) as GameObject;
							g_AssetsInScene [3].transform.parent = g_AssetHolderParent.transform;
							f_AddedProductUIControl ();
						}
					}
				}
			}
		}
	}							
	public void f_PriceTotal(){
		TotalPrice += g_Price [g_ProductCounter];
		g_TotalPrice.text = "AED " + TotalPrice.ToString ();
	}



	public GameObject ChairColourHandlerBlend;
	public GameObject ChairColourHandlerBerkley;
	public GameObject ChairColourHandlerGaily;
	public GameObject ChairColourHandlerClaire;
	public GameObject ChairColourHandlerHexa;
	public GameObject ChairColourHandlerParlin;
	public GameObject TableColourHandler;


	public void f_PencilSelect(int l_Val){
		f_ResetAllPencilSelect ();
//		f_DisableAllColourPanels ();
		g_ProductListBtn [l_Val].transform.GetChild (0).GetComponent<Button>().interactable = false;
//		if (string.Compare (g_AssetsInScene [l_Val].name, "berkeleychair(Clone)") == 0) {
//			g_AssetName = g_AssetsInScene [l_Val].name;
//			ChairColourHandlerBerkley.SetActive (true);
//		} else if (string.Compare (g_AssetsInScene [l_Val].name, "blendchair(Clone)") == 0) {
//			ChairColourHandlerBlend.SetActive (true);
//			g_AssetName = g_AssetsInScene [l_Val].name;
//		} else if (string.Compare (g_AssetsInScene [l_Val].name, "clairechair(Clone)") == 0) {
//			ChairColourHandlerClaire.SetActive (true);
//			g_AssetName = g_AssetsInScene [l_Val].name;
//		} else if (string.Compare (g_AssetsInScene [l_Val].name, "gailychair(Clone)") == 0) {
//			ChairColourHandlerGaily.SetActive (true);
//			g_AssetName = g_AssetsInScene [l_Val].name;
//		} else if (string.Compare (g_AssetsInScene [l_Val].name, "hexachair(Clone)") == 0) {
//			ChairColourHandlerHexa.SetActive (true);
//			g_AssetName = g_AssetsInScene [l_Val].name;
//		} else if (string.Compare (g_AssetsInScene [l_Val].name, "parlinchair(Clone)") == 0) {
//			ChairColourHandlerParlin.SetActive (true);
//			g_AssetName = g_AssetsInScene [l_Val].name;
//		}else if (string.Compare (g_AssetsInScene [l_Val].name, "squaretabletop(Clone)") == 0) {
//			TableColourHandler.SetActive (true);
//			g_AssetName = g_AssetsInScene [l_Val].name;
//		}else if (string.Compare (g_AssetsInScene [l_Val].name, "roundtabletop(Clone)") == 0) {
//			TableColourHandler.SetActive (true);
//			g_AssetName = g_AssetsInScene [l_Val].name;
//		}
	}

	void f_ResetAllPencilSelect(){
		for (int l_i = 0; l_i < g_ProductListBtn.Length; l_i++) {
			g_ProductListBtn [l_i].transform.GetChild (0).GetComponent<Button>().interactable = true;
		}
	}

	void f_DisableAllColourPanels(){
		ChairColourHandlerBerkley.SetActive (false);
		ChairColourHandlerBlend.SetActive (false);
		ChairColourHandlerClaire.SetActive (false);
		ChairColourHandlerGaily.SetActive (false);
		ChairColourHandlerHexa.SetActive (false);
		ChairColourHandlerParlin.SetActive (false);
		//TableColourHandler.SetActive (false);
	}

	public void f_ChangeColourChair_Compose(int l_Val){
		if (string.Compare (g_AssetName, "clairechair(Clone)") == 0) {
			if (l_Val == 5) {
				C_Colours_Hardcoded.Instance.ChairMats [8].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
				C_Colours_Hardcoded.Instance.ChairMats [7].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
				C_Colours_Hardcoded.Instance.ChairMats [6].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} else if (l_Val == 7) {
				C_Colours_Hardcoded.Instance.ChairMats [8].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
				C_Colours_Hardcoded.Instance.ChairMats [7].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
				C_Colours_Hardcoded.Instance.ChairMats [6].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} 
		} else if (string.Compare (g_AssetName, "gailychair(Clone)") == 0) {
			if (l_Val == 3) {
				C_Colours_Hardcoded.Instance.ChairMats [2].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} else if (l_Val == 4) {
				C_Colours_Hardcoded.Instance.ChairMats [2].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			}
		} else if (string.Compare (g_AssetName, "hexachair(Clone)") == 0) {
			if (l_Val == 2) {
				C_Colours_Hardcoded.Instance.ChairMats [3].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} else if (l_Val == 1) {
				C_Colours_Hardcoded.Instance.ChairMats [3].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} else if (l_Val == 3) {
				C_Colours_Hardcoded.Instance.ChairMats [3].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} else if (l_Val == 6) {
				C_Colours_Hardcoded.Instance.ChairMats [3].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			}
		} else if (string.Compare (g_AssetName, "blendchair(Clone)") == 0) {
			if (l_Val == 0) {
				C_Colours_Hardcoded.Instance.ChairMats [1].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} else if (l_Val == 1) {
				C_Colours_Hardcoded.Instance.ChairMats [1].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} else if (l_Val == 2) {
				C_Colours_Hardcoded.Instance.ChairMats [1].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} 
		} else if (string.Compare (g_AssetName, "berkeleychair(Clone)") == 0) {
			if (l_Val == 8) {
				C_Colours_Hardcoded.Instance.ChairMats [0].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} else if (l_Val == 1) {
				C_Colours_Hardcoded.Instance.ChairMats [0].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} 
		} else if (string.Compare (g_AssetName, "parlinchair(Clone)") == 0) {
			if (l_Val == 5) {
				C_Colours_Hardcoded.Instance.ChairMats [4].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
				C_Colours_Hardcoded.Instance.ChairMats [5].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} else if (l_Val == 1) {
				C_Colours_Hardcoded.Instance.ChairMats [4].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
				C_Colours_Hardcoded.Instance.ChairMats [5].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			} else if (l_Val == 2) {
				C_Colours_Hardcoded.Instance.ChairMats [4].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
				C_Colours_Hardcoded.Instance.ChairMats [5].mainTexture = C_Colours_Hardcoded.Instance.ChairsTextures [l_Val];
			}
		} else if (string.Compare (g_AssetName, "squaretabletop(Clone)") == 0) {
			if (l_Val == 0) {
				C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 1) {
				C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 2) {
				C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 3) {
				C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 4) {
				C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			}
		} else if (string.Compare (g_AssetName, "roundtabletop(Clone)") == 0) {
			if (l_Val == 0) {
				C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 1) {
				C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 2) {
				C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 3) {
				C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 4) {
				C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			}
		}
	}

	public void f_TableColours(int l_Val){
		if (string.Compare (g_AssetsInScene [0].name, "roundtabletop(Clone)") == 0) {
			if (l_Val == 0) {
				C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 1) {
				C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 2) {
				C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 3) {
				C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 4) {
				C_Colours_Hardcoded.Instance.TableTopMats [1].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			}
		} else {
			if (l_Val == 0) {
				C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 1) {
				C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 2) {
				C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 3) {
				C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			} else if (l_Val == 4) {
				C_Colours_Hardcoded.Instance.TableTopMats [0].mainTexture = C_Colours_Hardcoded.Instance.TableTopTextures [l_Val];
			}
		}
	}

	public void f_SelectAssetForColour(){
		f_DisableAllColourPanels ();
		f_ResetAllPencilSelect ();
		if (string.Compare (g_AssetName, "berkeleychair(Clone)") == 0) {
			//g_AssetName = g_AssetsInScene [l_Val].name;
			ChairColourHandlerBerkley.SetActive (true);
		} else if (string.Compare (g_AssetName, "blendchair(Clone)") == 0) {
			ChairColourHandlerBlend.SetActive (true);
			//g_AssetName = g_AssetsInScene [l_Val].name;
		} else if (string.Compare (g_AssetName, "clairechair(Clone)") == 0) {
			ChairColourHandlerClaire.SetActive (true);
			//g_AssetName = g_AssetsInScene [l_Val].name;
		} else if (string.Compare (g_AssetName, "gailychair(Clone)") == 0) {
			ChairColourHandlerGaily.SetActive (true);
			//g_AssetName = g_AssetsInScene [l_Val].name;
		} else if (string.Compare (g_AssetName, "hexachair(Clone)") == 0) {
			ChairColourHandlerHexa.SetActive (true);
			//g_AssetName = g_AssetsInScene [l_Val].name;
		} else if (string.Compare (g_AssetName, "parlinchair(Clone)") == 0) {
			ChairColourHandlerParlin.SetActive (true);
			//g_AssetName = g_AssetsInScene [l_Val].name;
		}
	}

}
