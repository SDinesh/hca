﻿using UnityEngine;
using System.Collections;

public class C_AssetPreviewManager : MonoBehaviour {

	private static C_AssetPreviewManager instance;
	public static C_AssetPreviewManager Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_AssetPreviewManager> ();

			return instance;
		}
	}

	public GameObject[] g_SelectedAssetArray =  new GameObject[8];
	public Material g_SelectedAssetMaterial = null;
	public GameObject g_SelectedAsset;
	public GameObject[] g_WardrobeDoors;

	public Texture[] g_AssetTexture;
	public GameObject g_RotateBtnPanel;
	public GameObject g_ColorBtnPanel;
	public GameObject g_TextureBtnPanel;

	bool g_RotateRightFlag = false;
	bool g_RotateLeftFlag  = false;
	string g_RotateBtnFocusedOn = "";
	float f_RotateSpeed = 0;


	public GameObject RelatedDining;
	public GameObject RelatedLiving;
	public GameObject RelatedSleeping;
	public GameObject RelatedDiningCombinations;
	public GameObject RelatedContinentalCombinations;
	public GameObject RelatedInfinitiCombinations;

	public GameObject ShowDoorsBtn;
	public GameObject HideDoorsBtn;

	public GameObject g_LivingLight;
	public GameObject g_DiningLight;

	void Start()
	{
		f_RotateSpeed = 45;
	}

	// Use this for initialization
	void OnEnable () 
	{
		f_DisableAllRelated ();
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
			if (C_DiningCombination.Instance.isCombination) {
				RelatedDiningCombinations.SetActive (true);
			} else {
				RelatedDining.SetActive (true);
			}
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
			RelatedLiving.SetActive (true);
		} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
			if (C_StellarBed_Manager.Instance.g_SleepingType == 0) {
				RelatedSleeping.SetActive (true);
			} else if (C_StellarBed_Manager.Instance.g_SleepingType == 1) {
				RelatedContinentalCombinations.SetActive (true);
			} else if (C_StellarBed_Manager.Instance.g_SleepingType == 2) {
				RelatedInfinitiCombinations.SetActive (true);
			}
		}
		g_RotateRightFlag = false;
		g_RotateLeftFlag  = false;
		if (C_StellarBed_Manager.Instance.isRotatable) {
			g_RotateBtnPanel.SetActive (true);
		} else {
			g_RotateBtnPanel.SetActive (false);
		}
		g_ColorBtnPanel.SetActive(false);
		g_TextureBtnPanel.SetActive(false);
		C_Colours_Hardcoded.Instance.TblColourHandler.SetActive (false);
		C_Colours_Hardcoded.Instance.ChairColourHandler.SetActive (false);

		C_GlobalVariables.instance.g_View = E_View.Preview;
	}

	public void f_DoorsControl(bool l_ShowDoor){
		if (l_ShowDoor) {
			ShowDoorsBtn.SetActive (false);
			HideDoorsBtn.SetActive (true);
		} else {
			ShowDoorsBtn.SetActive (true);
			HideDoorsBtn.SetActive (false);
		}

			for (int l_i = 0; l_i < g_WardrobeDoors.Length; l_i++) {
			g_WardrobeDoors [l_i].SetActive (l_ShowDoor);
			}
	}

	void f_DisableAllRelated(){
		RelatedSleeping.SetActive (false);
		RelatedLiving.SetActive (false);
		RelatedDining.SetActive (false);
		RelatedDiningCombinations.SetActive (false);
		RelatedInfinitiCombinations.SetActive (false);
		RelatedContinentalCombinations.SetActive (false);
	}

	void OnDisable()
	{
		for (int l_i = 0; l_i < g_SelectedAssetArray.Length; l_i++) {
			g_SelectedAssetArray [l_i].SetActive (false);
		}
		C_GlobalVariables.instance.g_View = E_View.None;
	}

	// Update is called once per frame
	void Update () 
	{
		f_RotateAsset ();
	}


	void f_RotateAsset()
	{
		if (g_RotateLeftFlag) 
		{
			g_SelectedAsset.transform.Rotate (Vector3.up * f_RotateSpeed * Time.deltaTime);
		}
		else if (g_RotateRightFlag) 
		{
			g_SelectedAsset.transform.Rotate (Vector3.up * -f_RotateSpeed * Time.deltaTime);
		}
	}


	//------------------------------------------------UI-------------------------------------------------------



	public void f_Rotate(int l_Dir)
	{
		if (l_Dir == 0) {
			g_RotateRightFlag = true;
		} else {
			g_RotateLeftFlag = true;
		}
	}

	public void f_RotateStop(){
		g_RotateRightFlag = false;
		g_RotateLeftFlag = false;
	}

	public void f_OnPointerExitRotateBtn()
	{
		g_RotateBtnFocusedOn = "";
		g_RotateRightFlag = false;
		g_RotateLeftFlag  = false;
	}

	public void f_OnPointerDownRotateBtn(string l_Dir)
	{
		//if (g_RotateBtnFocusedOn != "") {return;}

		if (l_Dir == "left") 
		{
			g_RotateBtnFocusedOn = "L";
			g_RotateLeftFlag  = true;
		}
		else if (l_Dir == "right") 
		{
			g_RotateBtnFocusedOn = "R";
			g_RotateRightFlag = true;
		}
	}

	public void f_OnPointerUpRotateBtn()
	{
		g_RotateBtnFocusedOn = "";
		g_RotateRightFlag = false;
		g_RotateLeftFlag  = false;
	}


	public void f_ManageAssetTexture(int l_ColorID)
	{
		
		switch(l_ColorID)
		{
		case 1:
			
			g_SelectedAssetMaterial.SetTexture("_MainTex",g_AssetTexture [0]);
			break;

		case 2:
			g_SelectedAssetMaterial.mainTexture = g_AssetTexture [1];
			break;

		case 3:
			g_SelectedAssetMaterial.mainTexture = g_AssetTexture [2];
			break;

		case 4:
			g_SelectedAssetMaterial.mainTexture = g_AssetTexture [3];
			break;

		case 5:
			g_SelectedAssetMaterial.mainTexture = g_AssetTexture [4];
			break;
		}
	}


	public void f_ManageRotateAndColorPanel(int l_ID)
	{
		//C_AssetPreviewUI.Instance.f_ResetAllBtns ();
		if (l_ID == 1) {
			g_RotateBtnPanel.SetActive (true);
			g_ColorBtnPanel.SetActive (false);
			g_TextureBtnPanel.SetActive (false);
			C_Colours_Hardcoded.Instance.f_DisableAll ();
		} else if (l_ID == 2) {
			g_RotateBtnPanel.SetActive (false);
			g_TextureBtnPanel.SetActive (false);
			C_Colours_Hardcoded.Instance.f_DisableAll ();
			if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
				if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "squaretabletop") == 0 || string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "roundtabletop") == 0) {
					C_Colours_Hardcoded.Instance.f_DisableAll ();
					C_Colours_Hardcoded.Instance.TblColourHandler.SetActive (true);
				} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "clairechair") == 0) {
					C_Colours_Hardcoded.Instance.f_DisableAll ();
					C_Colours_Hardcoded.Instance.ChairColourHandlerClaire.SetActive (true);
				} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "gailychair") == 0) {
					C_Colours_Hardcoded.Instance.f_DisableAll ();
					C_Colours_Hardcoded.Instance.ChairColourHandlerGaily.SetActive (true);
				} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "hexachair") == 0) {
					C_Colours_Hardcoded.Instance.f_DisableAll ();
					C_Colours_Hardcoded.Instance.ChairColourHandlerHexa.SetActive (true);
				} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "blendchair") == 0) {
					C_Colours_Hardcoded.Instance.f_DisableAll ();
					C_Colours_Hardcoded.Instance.ChairColourHandlerBlend.SetActive (true);
				} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "berkeleychair") == 0) {
					C_Colours_Hardcoded.Instance.f_DisableAll ();
					C_Colours_Hardcoded.Instance.ChairColourHandlerBerkley.SetActive (true);
				} else if (string.Compare (C_AssetBundle_Downloader.Instance.g_AssetName, "parlinchair") == 0) {
					C_Colours_Hardcoded.Instance.f_DisableAll ();
					C_Colours_Hardcoded.Instance.ChairColourHandlerParlin.SetActive (true);
				}
			} else {
				g_ColorBtnPanel.SetActive (true);
				C_Colours_Hardcoded.Instance.f_DisableAll ();
			}
		}
		else if(l_ID == 3)
		{
			g_RotateBtnPanel.SetActive(false);
			g_ColorBtnPanel.SetActive(false);
			C_Colours_Hardcoded.Instance.TblColourHandler.SetActive (false);
			C_Colours_Hardcoded.Instance.ChairColourHandler.SetActive (false);
			g_TextureBtnPanel.SetActive(true);
		}
	}

	public void f_DisableAllBtnsAndPanels()
	{
		C_Colours_Hardcoded.Instance.f_DisableAll ();
		C_AssetPreviewUI.Instance.f_ResetAllBtns ();
		g_RotateBtnPanel.SetActive (false);
	}

	public void f_SelectNextRelatedPreview(){
		f_DisableAllBtnsAndPanels ();
		g_RotateBtnPanel.SetActive (true);
		C_AssetPreviewUI.Instance.g_360View.f_OnPointerUp ();
	}
}


