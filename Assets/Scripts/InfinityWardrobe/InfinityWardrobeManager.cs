﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;


public class InfinityWardrobeManager : MonoBehaviour {

    public enum FrameHeights
    {
        k2_1M,
        k2_4M
    };

    public enum ItemTypes
    {
        kFrame,
        kDoor,
        kAccessory
    };

    public enum DoorTypes
    {
        swingDoor,
        slidingDoor,
        None
    };

    public enum DoorSides
    {
        left,
        right
    };

    public enum ProductTypes
    {
        kFrame45CM = 1,
        kFrame90CM = 2,
        kFrame180CM = 3,
        kFrame270CM = 4,
        kFrameLShape = 5,

        kSwingDoorWhite = 101,
        kSwingDoorCream = 102,
        kSwingDoorGrey = 103,
        kSwingDoorGreyOakMelamine = 104,
        kSwingDoorWalnutMelamine = 105,
        kSwingDoorMirror = 106,

        kSlidingDoorWhite = 201,
        kSlidingDoorCream = 202,
        kSlidingDoorGrey = 203,
        kSlidingDoorGreyOakMelamine = 204,
        kSlidingDoorWalnutMelamine = 205,
        kSlidingDoorMirror = 206,

        kSidePanel = 301,
        kCenterPanel = 302,

        kAccessory90CM3DChest = 401,
        kAccessory90CMShelf = 402,
        kAccessory90CMPantHanger = 403,
        kAccessory90CMPulloutShelf = 404,
        kAccessory90CMTieBeltRack = 405,
        kAccessoryLShapedShelf = 406,
        kAccessoryLShapedHangingRod = 407,
        kAccessory90CM3DGlassChest = 408,
        kAccessory45CMShelf = 409,
        kAccessory45CM3DChest = 410,
        kAccessory45CM3DGlassChest = 411,
		kAccessory45CMPulloutShelf = 412,
		kAccessory45CMHangingRod = 413,
		kAccessory90CMHangingRod = 414,
		kAccessory45CMMetalBasket = 415,
		kAccessory90CMMetalBasket = 416,
		kAccessory90CMPullDownHanger = 417,
		kAccessory90CMGlassShelves = 418,
		kAccessory100CMDivider = 419,

        None
    };

    public static InfinityWardrobeManager Instance;

    public PrefabReferences prefabReferences;
    public PanelUI panelUI;
    public Camera camera;

    // Contains the details about each and every product in json dict format
    public IDictionary IWData; 

    [HideInInspector] public List<Frame> frameList = new List<Frame>();

    [HideInInspector] public FrameHeights selectedFrameHeight = FrameHeights.k2_1M;
    [HideInInspector] public ProductTypes currentFrameType;

    private GameObject productsParentObj;
    [HideInInspector] public GameObject generatedAccessoryObj;

    [Range(0,2)]
    public int maxSides = 2;
    public int maxLengthInRow; // 450
    [HideInInspector] public int totalSides; // 0, 1, 2
    [HideInInspector] public int[] lengthOnSides = new int[3]; // Only 3 sides are allowed to construct
    private float minLengthOfFrame = 45;

    [HideInInspector] public Vector3 startingPosition = new Vector3(-3.57f,0,-2.54f);
    [HideInInspector] public bool isCompositionFinished = false;

    public LayerMask raycastLayerMask;

    private float accessoryYDistance = 0;
    private float side1YDistancePer45 = 1.1f;
    private float yPositionFor45 = 0.75f;

    public delegate void FinishCompositionAction();
    public static event FinishCompositionAction OnFinish;

    public delegate void MouseUpAction();
    public static event MouseUpAction OnMouseUp;

	public bool isLShape;

    void Awake()
    {
        Instance = this;
    }

    void OnEnable()
    {
        camera.gameObject.SetActive(true);
    }

    public void Reset()
    {
        totalSides = 0;
        for(int i = 0; i < 3; i++)
        {
            lengthOnSides[i] = 0;
        }

        frameList.Clear();
        currentFrameType = ProductTypes.None;
        selectedFrameHeight = FrameHeights.k2_1M;
        camera.orthographic = true;
    }

    /// <summary>
    /// Create the parent gameobject to hold the Frames if not available.
    /// If already created returns the object reference
    /// </summary>
    /// <returns>The parent for object.</returns>
    GameObject GetParentForObject()
    {
        if(!productsParentObj)
        {
            productsParentObj = new GameObject("IWAssetHolder");
        }
        return productsParentObj; 
    }

    #region Frame related functionalities

    public void AddFrame(ProductTypes type)
    {
        currentFrameType = type;

        GetParentForObject().transform.rotation = Quaternion.Euler(0,0,0);
        Frame frameObj = prefabReferences.GetFrame(selectedFrameHeight,type);
        GameObject obj = Instantiate(frameObj.gameObject) as GameObject;
        obj.transform.parent = GetParentForObject().transform;

        Frame objInstance = obj.GetComponent<Frame>();
        frameList.Add(objInstance);
        objInstance.SetFrameDetails(type);

        ShowSide(totalSides);

        CalculateLengthAndSide(type,totalSides,true);

        panelUI.ManipulateFinishCompositionBtn(false);
    }

    public void RemoveFrame(Frame frame)
    {
        CalculateLengthAndSide(frame.type,frame.side,false);
        frameList.Remove(frame);
    }

    /// <summary>
    /// It'll be called when an frame got deleted. So it'll call other frames to reset the position to snap in.
    /// </summary>
    public void UpdateFramePosition()
    {
        foreach(Frame frame in frameList)
        {
            frame.SetPosition();
        }
    }

    /// <summary>
    /// Removes the Frames from the given index
    /// </summary>
    /// <param name="fromIndex">From index.</param>
    public void RemoveFramesFromIndex(int fromIndex)
    {
        for(int i = frameList.Count - 1; i >= fromIndex; i--)
        {
            Frame frame = frameList[i];
            RemoveFrame(frame);
            Destroy(frame.gameObject);
        }

        if(fromIndex == 0)
        {
            panelUI.ManipulateFinishCompositionBtn(false);
            panelUI.ManipulateShowHideBtn();
            isCompositionFinished = false;
        }
        else if(fromIndex > 0)
        {
            panelUI.ManipulateFinishCompositionBtn(true);
        }

    }

    #endregion

    public void AddDoor(ProductTypes colorID)
    {
        ShowSide(totalSides);
        frameList[frameList.Count - 1].ActivateDoor(colorID);
    }

    public void AddAccessory(ProductTypes productID)
    {
        GameObject objRef = prefabReferences.GetAccessory(productID);
        generatedAccessoryObj = Instantiate (objRef) as GameObject;

		if (generatedAccessoryObj.GetComponent<Accessory> ().type == ProductTypes.kAccessoryLShapedShelf || generatedAccessoryObj.GetComponent<Accessory> ().type == ProductTypes.kAccessoryLShapedHangingRod) {
			isLShape = true;
		} else {
			isLShape = false;
		}
    }

    #region Other functionalities

    void Update()
    {
        // If the user selects any object from play area, appropriate product in left panel scroll will be highlighted through this
        if (Input.GetMouseButtonDown (0) && panelUI.isInGamePlayArea) {

			if(EventSystem.current.IsPointerOverGameObject())
			{
				Debug.Log("Clicked on the UI");
				return;
			}

            Ray ray =  Camera.main.ScreenPointToRay (Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast (ray, out hit)) 
            {
                GameObject obj = hit.transform.gameObject;
                Frame frame = obj.GetComponent<Frame>();
                if(frame)
                {
                    frame.AskScrollToMove();
                }
            }
        }

        // Moving the accessory in mouse position on drag
        // Raycast targets the gameobject "InfinityWardrobeBackground".
        if(generatedAccessoryObj)
        {
            Ray ray =  Camera.main.ScreenPointToRay (Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast (ray, out hit, raycastLayerMask))
            {
                Vector3 position = Vector3.zero;
                switch(panelUI.currentVisibleSide)
                {
				case 0:
						if (isLShape) {
							position = new Vector3 (hit.point.x, 0 + 1, hit.point.z);
						}
						else {
							position = new Vector3 (hit.point.x, 0, hit.point.z);
						}
                        break;
				case 1:
						if (isLShape) {
							position = new Vector3 (hit.point.x, accessoryYDistance + 0.5f, hit.point.z);
						}
						else {
							position = new Vector3 (hit.point.x, accessoryYDistance, hit.point.z);
						}
                        break;
                    case 2:
                        break;
                }
                
                generatedAccessoryObj.transform.position = position;
            }
        }

        // Fires a mouseup event
        if(Input.GetMouseButtonUp(0))
        {
            if(OnMouseUp != null)
                OnMouseUp();
        }
    }

    /// <summary>
    /// This method will show the given side of the wardrobe
    /// And deactivated the products opposite to it. Coz it will hide the current side
    /// </summary> 
    /// <param name="side">Side.</param>
    public void ShowSide(int side)
    {
        int sideToHide = 4; // Since there is no 4th side
        switch(side)
        {
            case 0:
                GetParentForObject().transform.position = new Vector3(0f,0,0);
                GetParentForObject().transform.rotation = Quaternion.Euler(0,0,0);
                sideToHide = 2;
                break;
            case 1:
                GetParentForObject().transform.position = new Vector3(-4.45f,0,0);
                GetParentForObject().transform.rotation = Quaternion.Euler(0,0,-90);
                sideToHide = 4;
                break;
            case 2:
                GetParentForObject().transform.position = new Vector3(4.8f,0,0);
                GetParentForObject().transform.rotation = Quaternion.Euler(0,0,-180);
                sideToHide = 0;
                break;
            case 3: // This case will show the L Shape view

                int totalLength = lengthOnSides[0] + lengthOnSides[1];

                float startingValue = 3.4f;
                float valuePer45 = 0.25f;
                float remainder = totalLength / minLengthOfFrame;
                float xPosition = startingValue - (remainder * valuePer45);

                GetParentForObject().transform.position = new Vector3(xPosition,-15.7f,0.4f);
                GetParentForObject().transform.rotation = Quaternion.Euler(-15.115f,0.313f,-44.391f);

                float initialScale = 1;
                float valuePer300 = 0.12f;
                float lengthToReduceScale = 300;
                remainder = totalLength / lengthToReduceScale;

                float scaleFactor = initialScale - (remainder * valuePer300);

                if(totalLength >= lengthToReduceScale)
                {
                    GetParentForObject().transform.localScale = new Vector3(scaleFactor,scaleFactor,scaleFactor);
                }
                break;
        }

        for(int i = 0; i < frameList.Count; i++)
        {
            Frame frame = frameList[i];
            if(frame.side == sideToHide)
            {
                frame.gameObject.SetActive(false);  
            }
            else
            {
                frame.gameObject.SetActive(true);
            }
        }

        panelUI.UpdateVisibleSide(side);
    }

    #endregion

    #region Play area bottom panel functions

    /// <summary>
    /// Toggles the doors for all added frames.
    /// </summary>
    /// <param name="isActive">If set to <c>true</c> is active.</param>
    public void ToggleDoors(bool isActive)
    {
        foreach(Frame frame in frameList)   
        {
            frame.ToggleDoors(isActive);
        }
    }

    /// <summary>
    /// Completes the composition and shows in 3D angle. 
    /// </summary>
    public void CompleteComposition()
    {
        isCompositionFinished = true;    

        Frame frame = GetFrameAtIndex(frameList.Count - 1);      
        frame.CheckAndActivatePanel();

        camera.orthographic = false;

        if(OnFinish != null)
            OnFinish();

        switch(totalSides)
        {
            case 0:
            case 1:
                ShowSide(3);
                break;
            case 2:
                ShowSide(1);
                break;
        }
    }

    public void ResetCamera()
    {
        if(!camera.orthographic)
            camera.orthographic = true;
    }

    /// <summary>
    /// Removes all the composed items
    /// </summary>
    public void ClearWardrobe()
    {
        for(int i = frameList.Count - 1; i >= 0; i--)
        {
            Frame frame = frameList[i];
            RemoveFrame(frame);
            Destroy(frame.gameObject);
        }

        isCompositionFinished = false;

        Destroy(productsParentObj);
        productsParentObj = null;
    }

    #endregion

    #region Helpers

    /// <summary>
    /// Calculates the length and side.
    /// </summary>
    /// <param name="type">Type.</param>
    /// <param name="side">Side.</param>
    /// <param name="shouldAdd">If set to <c>true</c> should add.</param>
    private void CalculateLengthAndSide(ProductTypes type, int side, bool shouldAdd)
    {
        if(shouldAdd)
            lengthOnSides[side] += GetLengthForFrameType(type);
        else
            lengthOnSides[side] -= GetLengthForFrameType(type);

        if(type == ProductTypes.kFrameLShape && shouldAdd)
        {
            totalSides++;

            if(totalSides == 1)
            {
                int remainder = lengthOnSides[0] / 45;
                remainder = remainder - 1;

                accessoryYDistance = yPositionFor45 - (remainder * side1YDistancePer45);
                print("accessoryYDistance "+accessoryYDistance);                                
            }
        }
        else if(type == ProductTypes.kFrameLShape && !shouldAdd)
        {
            totalSides--; 
        }
    }

    /// <summary>
    /// It asks the current frame to enquire about doors
    /// </summary>
    /// <returns><c>true</c> if this instance is door needed; otherwise, <c>false</c>.</returns>
    public bool IsDoorNeeded()
    {
        if(frameList.Count > 0)
        {
            Frame currentFrame = frameList[frameList.Count - 1];
            return currentFrame.IsDoorNeeded();    
        }
        return false;
    }

    public int GetFrameCount()
    {
        return frameList.Count;
    }

    /// <summary>
    /// Checks the length of the side and returns bool based on the length validation
    /// </summary>
    /// <returns><c>true</c> if this instance can add frame in row the specified type; otherwise, <c>false</c>.</returns>
    /// <param name="type">Type.</param>
    public bool CanAddFrameInRow(ProductTypes type)
    {
        if(type == ProductTypes.kFrameLShape)
        {
            if(lengthOnSides[totalSides] > 0 && totalSides < maxSides)
                return true;
            else
                return false;
        }
        else
        {
            if(lengthOnSides[totalSides] + GetLengthForFrameType(type) <= maxLengthInRow)
            {
                return true;
            }    
        }
        return false;
    }

    /// <summary>
    /// Just returns the frame @ Index
    /// </summary>
    /// <returns>The frame at index.</returns>
    /// <param name="index">Index.</param>
    public Frame GetFrameAtIndex(int index)
    {
        return frameList[index];
    }

    /// <summary>
    /// Returns the length of frame
    /// </summary>
    /// <returns>The length for frame type.</returns>
    /// <param name="type">Type.</param>
    private int GetLengthForFrameType(ProductTypes type)
    {
        int length = 0;
        switch(type)
        {
            case ProductTypes.kFrame45CM:
                length = 45;
                break;
            case ProductTypes.kFrame90CM:
                length = 90;
                break;
            case ProductTypes.kFrame180CM:
                length = 180;
                break;
            case ProductTypes.kFrame270CM:
                length = 270;
                break;
            default:
                length = 0;
                break;
        }
        return length;
    }

    /// <summary>
    /// This method returns true, if there is a condition like when you delete the frame which is in the formation of
    /// Null -> Frame -> LShape or LShape -> Frame -> LShape
    /// since we can't connect two LShapes together and the wardrobe starting can't be a LShape
    /// </summary>
    /// <returns><c>true</c>, if remove all frames from was shoulded, <c>false</c> otherwise.</returns>
    /// <param name="frame">Frame.</param>
    public bool ShouldRemoveAllFramesFrom(Frame frame)
    {
        int index = frameList.IndexOf(frame);

        Frame previousFrame = null;
        Frame nextFrame = null;

        if(index > 0)
            previousFrame = GetFrameAtIndex(index - 1);
        if(frameList.Count - 1 > index)
            nextFrame = GetFrameAtIndex(index + 1);

        if(previousFrame && nextFrame)
        {
            if(previousFrame.type == ProductTypes.kFrameLShape && nextFrame.type == ProductTypes.kFrameLShape)
                return true;
        }
        else if(!previousFrame && nextFrame)
        {
            if(nextFrame.type == ProductTypes.kFrameLShape)
                return true;
        }
        return false;
    }
    #endregion
}
   