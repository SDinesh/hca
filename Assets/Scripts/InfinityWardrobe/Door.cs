﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Door instance. Handles door related functionalities
/// </summary>
public class Door : MonoBehaviour {

    public GameObject doorMatObj;
    public InfinityWardrobeManager.DoorTypes type;
    public InfinityWardrobeManager.DoorSides side;

    public InfinityWardrobeManager.ProductTypes color;

    private GameObject uiScrollRef = null;
    private int doorFrameSide;
    private int doorPlaceInFrame;
    private bool isDoorOfLShape = false;

    public void SetDoorDetails(InfinityWardrobeManager.ProductTypes type, int frameSide, int doorCount, bool isLShape = false)
    {
        doorFrameSide = frameSide;
        doorPlaceInFrame = doorCount;
        isDoorOfLShape = isLShape;

        UpdateMat(type);
    }

    public void UpdateMat(InfinityWardrobeManager.ProductTypes toColor)
    {
        doorMatObj.GetComponent<MeshRenderer>().material = InfinityWardrobeManager.Instance.prefabReferences.GetMaterialForDoor(toColor); 
        color = toColor;

        AddProductToScroll();

        GotoSide();
    }

    public void GotoSide()
    {
        if(isDoorOfLShape && doorPlaceInFrame == 1)
            InfinityWardrobeManager.Instance.ShowSide(doorFrameSide + 1);
        else
            InfinityWardrobeManager.Instance.ShowSide(doorFrameSide);
    }

    void AddProductToScroll()
    {
        if(!uiScrollRef)
        {
            uiScrollRef = InfinityWardrobeManager.Instance.panelUI.AddProductInScroll(color,gameObject, 0);    
        }
    }

    public void ChangeToNextType()
    {
        InfinityWardrobeManager.ProductTypes nextColor = InfinityWardrobeManager.Instance.prefabReferences.GetNextDoorType(color);
        UpdateMat(nextColor);
    }

    void OnDestroy()
    {
        Destroy(uiScrollRef);
    }
}
