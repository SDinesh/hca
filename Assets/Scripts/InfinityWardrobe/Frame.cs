﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Frame instance. Handles frame related functionalities
/// </summary>
public class Frame : MonoBehaviour {

    public InfinityWardrobeManager.FrameHeights height;
    public InfinityWardrobeManager.ProductTypes type;

    public int possibleNumberOfDoors;
    public int currentDoorCount = 0;
    [HideInInspector] public int side = 0;

    public List<Door> frameDoorsList;
    private List<GameObject> activatedDoorsList = new List<GameObject>();

    public List<GameObject> panels;
    public List<AccessoryRef> accessorySlotsList;

	public List<AccessoryRef> dividerSlotsList1;
	public List<AccessoryRef> dividerSlotsList2;
	public List<AccessoryRef> dividerSlotsList3;

    private GameObject uiScrollRef;

    void OnEnable()
    {
        InfinityWardrobeManager.OnMouseUp += DisableReferencePoints;
    }

    void OnDisable()
    {
        InfinityWardrobeManager.OnMouseUp -= DisableReferencePoints;
    }

    public void SetFrameDetails(InfinityWardrobeManager.ProductTypes _type)
    {
        type = _type;
        side = InfinityWardrobeManager.Instance.totalSides;
        SetPosition();
        uiScrollRef = InfinityWardrobeManager.Instance.panelUI.AddProductInScroll(type,gameObject, 0);
        CheckAndActivatePanel();
    }

    #region Position setting
    /// <summary>
    /// Sets the frame position
    /// Snapping is happening here 
    /// Auto positioning happens here
    /// </summary>
    public void SetPosition()
    {
        Vector3 previousPoint = GetPreviousPoint();

        if(side > 0)
        {
            Frame previousFrame = GetPreviousFrame();
            transform.rotation = previousFrame.gameObject.transform.rotation;

            if(side == 1 && type == InfinityWardrobeManager.ProductTypes.kFrameLShape)
            {
                transform.rotation = Quaternion.Euler(90,90,90);
            }
        }

        transform.position = previousPoint;

        Vector3 v1 = Vector3.zero;
        if(side == 0)
        {
            v1 = new Vector3(transform.position.x + (transform.position.x - panels[0].transform.position.x),previousPoint.y,previousPoint.z);
        }
        else if(side == 1)
        {
            Frame previousFrame = GetPreviousFrame();
            if(previousFrame.type == InfinityWardrobeManager.ProductTypes.kFrameLShape)
            {
                v1 = new Vector3(previousFrame.panels[2].transform.position.x,transform.position.y + (transform.position.y - panels[0].transform.position.y),previousPoint.z);
            }
            else
                v1 = new Vector3(previousPoint.x,transform.position.y + (transform.position.y - panels[0].transform.position.y),previousPoint.z);
        }
        else if(side == 2)
        {
            Frame previousFrame = GetPreviousFrame();
            if(previousFrame.type == InfinityWardrobeManager.ProductTypes.kFrameLShape)
            {
                v1 = new Vector3(transform.position.x + (transform.position.x - panels[0].transform.position.x),previousFrame.panels[2].transform.position.y,previousPoint.z);
            }
            else
            {
                v1 = new Vector3(transform.position.x + (transform.position.x - panels[0].transform.position.x),previousPoint.y,previousPoint.z);
            }
        }
        transform.position = v1;
    }

    /// <summary>
    /// Gets the previous point.
    /// Uses the previous frames Right side panel as reference
    /// </summary>
    /// <returns>The previous point.</returns>
    Vector3 GetPreviousPoint()
    {
        Vector3 position = InfinityWardrobeManager.Instance.startingPosition;

        Frame previousFrame = GetPreviousFrame();
        if(previousFrame)
        {
            if(side == 0)
                position = new Vector3(previousFrame.panels[2].transform.position.x,previousFrame.gameObject.transform.position.y,previousFrame.gameObject.transform.position.z);
            else if(side == 1)
                position = new Vector3(previousFrame.gameObject.transform.position.x,previousFrame.panels[2].transform.position.y,previousFrame.gameObject.transform.position.z);
            else if(side == 2)
                position = new Vector3(previousFrame.panels[2].transform.position.x,previousFrame.gameObject.transform.position.y,previousFrame.gameObject.transform.position.z);
        }

        return position;
    }

    /// <summary>
    /// Gets the previous frame.
    /// </summary>
    /// <returns>The previous frame.</returns>
    Frame GetPreviousFrame()
    {
        int currentIndex = InfinityWardrobeManager.Instance.frameList.IndexOf(this);
        if(currentIndex > 0)
            return InfinityWardrobeManager.Instance.GetFrameAtIndex(currentIndex - 1);      

        return null;
    }

    #endregion

    #region Activation of Doors & Panels
    public void CheckAndActivatePanel()
    {
        // Activates the right side panel
        if(InfinityWardrobeManager.Instance.isCompositionFinished)
        {
            panels[2].gameObject.SetActive(true);

            if(type == InfinityWardrobeManager.ProductTypes.kFrameLShape)
                panels[2].gameObject.GetComponent<WardrobePanel>().SetProductDetails(side + 1);
            else
                panels[2].gameObject.GetComponent<WardrobePanel>().SetProductDetails(side);
        }
        else
        {
            // If this is the very first frame then activate the left side panel
            if(InfinityWardrobeManager.Instance.GetFrameCount() == 1)
            {
                // Activate left side panel
                panels[0].gameObject.SetActive(true);
                panels[0].gameObject.GetComponent<WardrobePanel>().SetProductDetails(side);
            }
            else if(InfinityWardrobeManager.Instance.GetFrameCount() > 0)
            {
                // Activate Center panel
                panels[1].gameObject.SetActive(true);
                panels[1].gameObject.GetComponent<WardrobePanel>().SetProductDetails(side);
            }    
        }
    }

    // Checks and returns bool based on activated door count vs possible door count
    public bool IsDoorNeeded()
    {
        if(possibleNumberOfDoors == currentDoorCount)
        {
            return false;
        }
        return true;
    }

    public void ActivateDoor(InfinityWardrobeManager.ProductTypes color)
    {
        bool isLShape = false;
        if(type == InfinityWardrobeManager.ProductTypes.kFrameLShape)
            isLShape = true;
        
        frameDoorsList[currentDoorCount].gameObject.SetActive(true);
        frameDoorsList[currentDoorCount].SetDoorDetails(color,side,currentDoorCount,isLShape);
        activatedDoorsList.Add(frameDoorsList[currentDoorCount].gameObject);
        currentDoorCount++;

        if(type == InfinityWardrobeManager.ProductTypes.kFrameLShape)
        {
            if(currentDoorCount == 1)
            {
                InfinityWardrobeManager.Instance.ShowSide(InfinityWardrobeManager.Instance.totalSides - 1);
            }
        }

        InfinityWardrobeManager.Instance.panelUI.ManipulateFinishCompositionBtn(!IsDoorNeeded());
        InfinityWardrobeManager.Instance.panelUI.ManipulateShowHideBtn();
    }

    /// <summary>
    /// Toggles the doors for this frame
    /// </summary>
    /// <param name="shouldHide">If set to <c>true</c> should hide.</param>
    public void ToggleDoors(bool shouldHide)
    {
        foreach(GameObject door in activatedDoorsList)
        {
            door.gameObject.SetActive(shouldHide);
        }
    }

    #endregion

    #region Accessory Related

    /// <summary>
    /// Shows the possible slots to set the accessory by highlighting those..
    /// </summary>
    /// <param name="other">Other.</param>
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("RealAccessory"))
        {
            Accessory realObjRef = other.gameObject.GetComponent<Accessory>();
            for(int i = 0; i < accessorySlotsList.Count; i++)
            {
                AccessoryRef accRef = accessorySlotsList[i];

                if(!accRef.IsOccupied && accRef.BlockCount == 0)
                {
                    if(IsSlotReady(accRef,realObjRef))
                    {
                        accRef.ShowAvailability(true);                        
                    }    
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("RealAccessory"))
        {
            DisableReferencePoints();
        }
    }

    /// <summary>
    /// This method finds the possible slots for the current dragging accessory
    /// </summary>
    /// <returns><c>true</c> if this instance is slot ready the specified accRef realAccessoryRef; otherwise, <c>false</c>.</returns>
    /// <param name="accRef">Acc reference.</param>
    /// <param name="realAccessoryRef">Real accessory reference.</param>
    bool IsSlotReady(AccessoryRef accRef,Accessory realAccessoryRef)
    {
        bool canAttach = false;
        switch(accRef.type)
        {
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMShelf:
                if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory90CMPulloutShelf ||
                    realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory90CMShelf ||
                    realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory90CMTieBeltRack || 
					realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory90CMMetalBasket)
                {
                    canAttach = true;
                }
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CM3DChest:
                if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory90CM3DChest || 
                    realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory90CM3DGlassChest)
                    canAttach = true;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMPantHanger:
                if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory90CMPantHanger)
                    canAttach = true;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory45CM3DChest:
                if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory45CM3DChest || 
                    realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory45CM3DGlassChest)
                    canAttach = true;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory45CMShelf:
				if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory45CMShelf || 
					realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory45CMPulloutShelf ||
					realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory45CMMetalBasket)
                    canAttach = true;
                break;
			case InfinityWardrobeManager.ProductTypes.kAccessoryLShapedShelf:
				if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessoryLShapedShelf)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessoryLShapedHangingRod:
				if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessoryLShapedHangingRod)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory45CMHangingRod:
				if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory45CMHangingRod)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMHangingRod:
				if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory90CMHangingRod)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMPullDownHanger:
				if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory90CMPullDownHanger)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMGlassShelves:
				if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory90CMGlassShelves)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory100CMDivider:
				if(realAccessoryRef.type == InfinityWardrobeManager.ProductTypes.kAccessory100CMDivider)
					canAttach = true;
				break;
        }

        return canAttach;
    }

    /// <summary>
    /// Disables the highlighted slots
    /// </summary>
    void DisableReferencePoints()
    {
        for(int i = 0; i < accessorySlotsList.Count; i++)
        {
            AccessoryRef accRef = accessorySlotsList[i];
            accRef.ShowAvailability(false);
        }
    }

    #endregion

    #region Others
    public void AskScrollToMove()
    {
        InfinityWardrobeManager.Instance.panelUI.MoveScrollToIndex(uiScrollRef);
    }

    public void GotoSide()
    {
        InfinityWardrobeManager.Instance.ShowSide(side);
    }
    #endregion

    /// <summary>
    /// Raises the destroy event.
    /// It asks to destroy the related UI reference from the scroll as well. 
    /// </summary>
    void OnDestroy()
    {
        Destroy(uiScrollRef);
    }


}
