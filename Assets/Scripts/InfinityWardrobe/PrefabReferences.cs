﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// Holds the prefab references needed for Infinity wardrobe
/// </summary>
public class PrefabReferences : MonoBehaviour {

    #region References
    [Header("2.1 Frames")]
    public Frame f_21_45cm;
    public Frame f_21_90cm;
    public Frame f_21_180cm;
    public Frame f_21_270cm;
    public Frame f_21_LShape;

    [Header("2.4 Frames")]
    public Frame f_24_45cm;
    public Frame f_24_90cm;
    public Frame f_24_180cm;
    public Frame f_24_270cm;
    public Frame f_24_LShape;

    [Header("Door Materials")]
    public Material white;
    public Material cream;
    public Material grey;
    public Material greyOakMelamine;
    public Material walnutMelamine;
    public Material Mirror;

    [Header("Accessories Prefab")]
    public GameObject k90CM3DChest;
    public GameObject k90CMShelf;
    public GameObject k90CMPantHanger;
    public GameObject k90CMPulloutShelf;
    public GameObject k90CMTieBeltRack;
    public GameObject kLShapedShelf;
    public GameObject kLShapedHangingRod;
    public GameObject k90CM3DGlassChest;
    public GameObject k45CMShelf;
    public GameObject k45CM3DChest;
    public GameObject k45CM3DGlassChest;
	public GameObject k45CMPulloutShelf;
	public GameObject k45CMHangingRod;
	public GameObject k90CMHangingRod;
	public GameObject k45CMMetalBasket;
	public GameObject k90CMMetalBasket;
	public GameObject k90CMPullDownHanger;
	public GameObject k90CMGlassShelves;
	public GameObject k100CMCenterDivider;

    [Header("Side scroll Images")]
    public Sprite k45CMFrame;
    public Sprite k90CMFrame;
    public Sprite k180CMFrame;
    public Sprite k270CMFrame;
    public Sprite kLShapeFrame;
    public Sprite kSwingDoorWhite;
    public Sprite kSwingDoorCream;
    public Sprite kSwingDoorGrey;
    public Sprite kSwingDoorGreyOakMelamine;
    public Sprite kSwingDoorWalnutMelamine;
    public Sprite kSwingDoorMirror;
    public Sprite kSlidingDoorWhite;
    public Sprite kSlidingDoorCream;
    public Sprite kSlidingDoorGrey;
    public Sprite kSlidingDoorGreyOakMelamine;
    public Sprite kSlidingDoorWalnutMelamine;
    public Sprite kSlidingDoorMirror;
    public Sprite kSidePanel;
    public Sprite kCenterPanel;
    public Sprite kAccessory90CM3DChest;
    public Sprite kAccessory90CMShelf;
    public Sprite kAccessory90CMPantHanger;
    public Sprite kAccessory90CMPulloutShelf;
    public Sprite kAccessory90CMTieBeltRack;
    public Sprite kAccessoryLShapedShelf;
    public Sprite kAccessoryLShapedHangingRod;
    public Sprite kAccessory90CM3DGlassChest;
    public Sprite kAccessory45CMShelf;
    public Sprite kAccessory45CM3DChest;
    public Sprite kAccessory45CM3DGlassChest;
	public Sprite kAccessory45CMPulloutShelf;
	public Sprite kAccessory45CMHangingRod;
	public Sprite kAccessory90CMHangingRod;
	public Sprite kAccessory45CMMetalBasket;
	public Sprite kAccessory90CMMetalBasket;
	public Sprite kAccessory90CMPullDownHanger;
	public Sprite kAccessory90CMGlassShelves;
	public Sprite kAccessory100CMCenterDivider;

    #endregion
    /// <summary>
    /// Returns the prefab reference of frame according to height
    /// </summary>
    /// <returns>The frame.</returns>
    /// <param name="height">Height.</param>
    /// <param name="type">Type.</param>
    public Frame GetFrame(InfinityWardrobeManager.FrameHeights height, InfinityWardrobeManager.ProductTypes type)
    {
        Frame obj = null;
        switch(height)
        {
            case InfinityWardrobeManager.FrameHeights.k2_1M:
                switch(type)
                {
                    case InfinityWardrobeManager.ProductTypes.kFrame45CM:
                        obj = f_21_45cm;
                        break;
                    case InfinityWardrobeManager.ProductTypes.kFrame90CM:
                        obj = f_21_90cm;
                        break;
                    case InfinityWardrobeManager.ProductTypes.kFrame180CM:
                        obj = f_21_180cm;
                        break;
                    case InfinityWardrobeManager.ProductTypes.kFrame270CM:
                        obj = f_21_270cm;
                        break;
                    case InfinityWardrobeManager.ProductTypes.kFrameLShape:
                        obj = f_21_LShape;
                        break;
                }
                break;
            case InfinityWardrobeManager.FrameHeights.k2_4M:
                switch(type)
                {
                    case InfinityWardrobeManager.ProductTypes.kFrame45CM:
                        obj = f_24_45cm;
                        break;
                    case InfinityWardrobeManager.ProductTypes.kFrame90CM:
                        obj = f_24_90cm;
                        break;
                    case InfinityWardrobeManager.ProductTypes.kFrame180CM:
                        obj = f_24_180cm;
                        break;
                    case InfinityWardrobeManager.ProductTypes.kFrame270CM:
                        obj = f_24_270cm;
                        break;
                    case InfinityWardrobeManager.ProductTypes.kFrameLShape:
                        obj = f_24_LShape;
                        break;
                }
                break;
        }
        return obj;
    }

    /// <summary>
    /// Returns the material for the given door color 
    /// </summary>
    /// <returns>The material for door.</returns>
    /// <param name="color">Color.</param>
    public Material GetMaterialForDoor(InfinityWardrobeManager.ProductTypes color)
    {
        Material mat = null;
        switch(color)
        {
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorWhite:
            case InfinityWardrobeManager.ProductTypes.kSwingDoorWhite:
                mat = white;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorCream:
            case InfinityWardrobeManager.ProductTypes.kSwingDoorCream:
                mat = cream;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorGrey:
            case InfinityWardrobeManager.ProductTypes.kSwingDoorGrey:
                mat = grey;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorGreyOakMelamine:
            case InfinityWardrobeManager.ProductTypes.kSwingDoorGreyOakMelamine:
                mat = greyOakMelamine;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorWalnutMelamine:
            case InfinityWardrobeManager.ProductTypes.kSwingDoorWalnutMelamine:
                mat = walnutMelamine;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorMirror:
            case InfinityWardrobeManager.ProductTypes.kSwingDoorMirror:
                mat = Mirror;
                break;
        }
        return mat;
    }

    /// <summary>
    /// Returns the next type door according to current type.
    /// Used for change door texture functionality
    /// </summary>
    /// <returns>The next door type.</returns>
    /// <param name="currentColor">Current color.</param>
    public InfinityWardrobeManager.ProductTypes GetNextDoorType(InfinityWardrobeManager.ProductTypes currentColor)
    {
        InfinityWardrobeManager.ProductTypes nextType = InfinityWardrobeManager.ProductTypes.None;

        switch(currentColor)
        {
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorWhite:
                nextType = InfinityWardrobeManager.ProductTypes.kSlidingDoorCream;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorCream:
                nextType = InfinityWardrobeManager.ProductTypes.kSlidingDoorGrey;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorGrey:
                nextType = InfinityWardrobeManager.ProductTypes.kSlidingDoorGreyOakMelamine;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorGreyOakMelamine:
                nextType = InfinityWardrobeManager.ProductTypes.kSlidingDoorWalnutMelamine;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorWalnutMelamine:
                nextType = InfinityWardrobeManager.ProductTypes.kSlidingDoorMirror;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorMirror:
                nextType = InfinityWardrobeManager.ProductTypes.kSlidingDoorWhite;
                break;


            case InfinityWardrobeManager.ProductTypes.kSwingDoorWhite:
                nextType = InfinityWardrobeManager.ProductTypes.kSwingDoorCream;
                break;
            case InfinityWardrobeManager.ProductTypes.kSwingDoorCream:
                nextType = InfinityWardrobeManager.ProductTypes.kSwingDoorGrey;
                break;
            case InfinityWardrobeManager.ProductTypes.kSwingDoorGrey:
                nextType = InfinityWardrobeManager.ProductTypes.kSwingDoorGreyOakMelamine;
                break;
            case InfinityWardrobeManager.ProductTypes.kSwingDoorGreyOakMelamine:
                nextType = InfinityWardrobeManager.ProductTypes.kSwingDoorWalnutMelamine;
                break;
            case InfinityWardrobeManager.ProductTypes.kSwingDoorWalnutMelamine:
                nextType = InfinityWardrobeManager.ProductTypes.kSwingDoorMirror;
                break;
            case InfinityWardrobeManager.ProductTypes.kSwingDoorMirror:
                nextType = InfinityWardrobeManager.ProductTypes.kSwingDoorWhite;
                break;
        }
        return nextType;
    }

    /// <summary>
    /// Returns the referred Sprite to place it in the left scroll product prefabs
    /// </summary>
    /// <returns>The sprite for item.</returns>
    /// <param name="type">Type.</param>
    public Sprite GetSpriteForItem(InfinityWardrobeManager.ProductTypes type)
    {
        Sprite spr = null;
        switch(type)
        {
            case InfinityWardrobeManager.ProductTypes.kFrame45CM:
                spr = k45CMFrame;
                break;
            case InfinityWardrobeManager.ProductTypes.kFrame90CM:
                spr = k90CMFrame;
                break;
            case InfinityWardrobeManager.ProductTypes.kFrame180CM:
                spr = k180CMFrame;
                break;
            case InfinityWardrobeManager.ProductTypes.kFrame270CM:
                spr = k270CMFrame;
                break;
            case InfinityWardrobeManager.ProductTypes.kFrameLShape:
                spr = kLShapeFrame;
                break;
            
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorWhite:
                spr = kSlidingDoorWhite;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorCream:
                spr = kSlidingDoorCream;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorGrey:
                spr = kSlidingDoorGrey;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorGreyOakMelamine:
                spr = kSlidingDoorGreyOakMelamine;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorWalnutMelamine:
                spr = kSlidingDoorWalnutMelamine;
                break;
            case InfinityWardrobeManager.ProductTypes.kSlidingDoorMirror:
                spr = kSlidingDoorMirror;
                break;

            case InfinityWardrobeManager.ProductTypes.kSwingDoorWhite:
                spr = kSwingDoorWhite;
                break;
            case InfinityWardrobeManager.ProductTypes.kSwingDoorCream:
                spr = kSwingDoorCream;
                break;
            case InfinityWardrobeManager.ProductTypes.kSwingDoorGrey:
                spr = kSwingDoorGrey;
                break;
            case InfinityWardrobeManager.ProductTypes.kSwingDoorGreyOakMelamine:
                spr = kSwingDoorGreyOakMelamine;
                break;
            case InfinityWardrobeManager.ProductTypes.kSwingDoorWalnutMelamine:
                spr = kSwingDoorWalnutMelamine;
                break;
            case InfinityWardrobeManager.ProductTypes.kSwingDoorMirror:
                spr = kSwingDoorMirror;
                break;

            case InfinityWardrobeManager.ProductTypes.kSidePanel:
                spr = kSidePanel;
                break;
            case InfinityWardrobeManager.ProductTypes.kCenterPanel:
                spr = kCenterPanel;
                break;

            case InfinityWardrobeManager.ProductTypes.kAccessory90CM3DChest:
                spr = kAccessory90CM3DChest;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMShelf:
                spr = kAccessory90CMShelf;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMPantHanger:
                spr = kAccessory90CMPantHanger;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMPulloutShelf:
                spr = kAccessory90CMPulloutShelf;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMTieBeltRack:
                spr = kAccessory90CMTieBeltRack;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessoryLShapedShelf:
                spr = kAccessoryLShapedShelf;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessoryLShapedHangingRod:
                spr = kAccessoryLShapedHangingRod;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CM3DGlassChest:
                spr = kAccessory90CM3DGlassChest;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory45CMShelf:
                spr = kAccessory45CMShelf;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory45CM3DChest:
                spr = kAccessory45CM3DChest;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory45CM3DGlassChest:
                spr = kAccessory45CM3DGlassChest;
                break;
			case InfinityWardrobeManager.ProductTypes.kAccessory45CMPulloutShelf:
				spr = kAccessory45CMPulloutShelf;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory45CMHangingRod:
				spr = kAccessory45CMHangingRod;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMHangingRod:
				spr = kAccessory90CMHangingRod;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory45CMMetalBasket:
				spr = kAccessory45CMMetalBasket;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMMetalBasket:
				spr = kAccessory90CMMetalBasket;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMPullDownHanger:
				spr = kAccessory90CMPullDownHanger;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMGlassShelves:
				spr = kAccessory90CMGlassShelves;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory100CMDivider:
				spr = kAccessory100CMCenterDivider;
				break;
        }
        return spr;
    }

    /// <summary>
    /// Returns accessory prefab
    /// </summary>
    /// <returns>The accessory.</returns>
    /// <param name="type">Type.</param>
    public GameObject GetAccessory(InfinityWardrobeManager.ProductTypes type)
    {
        GameObject obj = null;
        switch(type)
        {
            case InfinityWardrobeManager.ProductTypes.kAccessory90CM3DChest:
                obj = k90CM3DChest;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMPantHanger:
                obj = k90CMPantHanger;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMPulloutShelf:
                obj = k90CMPulloutShelf;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMTieBeltRack:
                obj = k90CMTieBeltRack;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMShelf:
                obj = k90CMShelf;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessoryLShapedHangingRod:
                obj = kLShapedHangingRod;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessoryLShapedShelf:
                obj = kLShapedShelf;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CM3DGlassChest:
                obj = k90CM3DGlassChest;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory45CMShelf:
                obj = k45CMShelf;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory45CM3DChest:
                obj = k45CM3DChest;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory45CM3DGlassChest:
                obj = k45CM3DGlassChest;
                break;
			case InfinityWardrobeManager.ProductTypes.kAccessory45CMPulloutShelf:
				obj = k45CMPulloutShelf;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory45CMHangingRod:
				obj = k45CMHangingRod;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMHangingRod:
				obj = k90CMHangingRod;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory45CMMetalBasket:
				obj = k45CMMetalBasket;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMMetalBasket:
				obj = k90CMMetalBasket;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMPullDownHanger:
				obj = k90CMPullDownHanger;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMGlassShelves:
				obj = k90CMGlassShelves;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory100CMDivider:
				obj = k100CMCenterDivider;
				break;
        }
        return obj;
    }

}
