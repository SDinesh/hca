﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

/// <summary>
/// This script is used to find the touch detection on UI Button.
/// UI Button directly provides callback only on OnClick. ie.After mouse button releases
/// This script finds the button click on UI button before releases
/// </summary>
public class AccessoryController : MonoBehaviour {

    private float touchDuration;
    public float cloneDuration = 0.3f;
    private bool isObjectGenerated = false;

    private Vector3 lastPosition = Vector3.zero;
    private Vector3 deltaPos = Vector3.zero;

    public ScrollRect accessoryScrollRect;

	void Update () 
	{
		GenerateObject ();
	}

	private void GenerateObject()
	{
        if(lastPosition.Equals(Vector3.zero))
        {
            lastPosition = TouchOrMousePosition();
        }

		if (Input.GetMouseButton (0)) 
		{
			touchDuration += Time.deltaTime;
			Texture2D tempTexture;

			GameObject rayObject = RaycastingUI (); 

			if (rayObject != null) 
			{
                if (rayObject.CompareTag ("AccessoryUIBtn")) 
				{
					deltaPos = TouchOrMousePosition () - lastPosition;
					if (deltaPos.magnitude < 2) {

						if (touchDuration > cloneDuration && !isObjectGenerated)
						{
							isObjectGenerated = true;
                            accessoryScrollRect.enabled = false;

							InfinityWardrobeManager.ProductTypes objectType = rayObject.GetComponent<AccessoryBtn> ().type;
                            InfinityWardrobeManager.Instance.AddAccessory(objectType);
                        }
					}
				}
			}
		}

		if (Input.GetMouseButtonUp (0))
        {
			isObjectGenerated = false;
            accessoryScrollRect.enabled = true;
            touchDuration = 0;
		}

        lastPosition = TouchOrMousePosition();
	}

	private GameObject RaycastingUI ()
	{
		if (IsTouchDevice ()) {
			if (Input.touchCount == 0) {
				return null;
			}
		}

		if (!IsPointerOverGameObject ()) {
			return null;
		}

		PointerEventData pe = new PointerEventData (EventSystem.current);
		pe.position = TouchOrMousePosition ();

		List<RaycastResult> hits = new List<RaycastResult> ();
		EventSystem.current.RaycastAll (pe, hits);

		for (int i = 0; i < hits.Count; i++) {
			GameObject g = hits [i].gameObject;
			return g;
		}

		return null;
	}


	/// <summary>
	/// Determines whether the platform is mobile or not.
	/// </summary>
	/// <returns><c>true</c> if this instance is touch device; otherwise, <c>false</c>.</returns>
	private bool IsTouchDevice ()
	{
		if (Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.IPhonePlayer) {
			return true;
		}
		return false;
	}

	/// <summary>
	/// Determines whether the pointer is over UI game object or not.
	/// </summary>
	/// <returns><c>true</c> if this instance is pointer over game object; otherwise, <c>false</c>.</returns>
	private bool IsPointerOverGameObject ()
	{
		List<RaycastResult> rayCastResults = new List<RaycastResult> ();

		var eventDataCurrentPosition = new PointerEventData (EventSystem.current);

		eventDataCurrentPosition.position = Input.mousePosition;

		rayCastResults.Clear ();

		EventSystem.current.RaycastAll (eventDataCurrentPosition, rayCastResults);

		return rayCastResults.Count > 0;
	}

	/// <summary>
	/// Determines the touch or mouse click position
	/// </summary>
	/// <returns>The position.</returns>
	private Vector3 TouchOrMousePosition ()
	{
		if (IsTouchDevice ()) {
			if (Input.touchCount > 0) {
				return Input.GetTouch (0).position;
			}
			return Vector3.zero;
		} else {
			return Input.mousePosition;
		}
	}

}
