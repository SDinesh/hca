﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

/// <summary>
/// Just reads the JSON file, which contains the details about every product related to Infinity wardrobe.
/// </summary>
public class DBReader : MonoBehaviour {

    public string dbFileName;

    void Awake()
    {
        string data = GetDetailsFromFile(dbFileName);
        InfinityWardrobeManager.Instance.IWData = Json.Deserialize(data) as IDictionary;
    }

    string GetDetailsFromFile(string fileName)
    {
        TextAsset textAsset = Resources.Load(fileName) as TextAsset;
        string fileData = textAsset.text;
        return fileData;
    }
}
