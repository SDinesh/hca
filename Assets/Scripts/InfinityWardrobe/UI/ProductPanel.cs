﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ProductPanel : MonoBehaviour {

    public Button framesBtn;
    public Button doorsBtn;
    public Button accessoriesBtn;

    void OnEnable()
    {
        if(!InfinityWardrobeManager.Instance.panelUI.pageStack.Contains(gameObject))
        {
            InfinityWardrobeManager.Instance.panelUI.DeactivatePreviousPage();
            InfinityWardrobeManager.Instance.panelUI.AddPage(gameObject);

            Reset();
            Validate();
        }
    }

    [EnumActionAttribute(typeof(InfinityWardrobeManager.ItemTypes))]
    public void Navigate(int index)
    {
        InfinityWardrobeManager.ItemTypes type = (InfinityWardrobeManager.ItemTypes)index;
        switch(type)
        {
            case InfinityWardrobeManager.ItemTypes.kFrame:
                InfinityWardrobeManager.Instance.panelUI.framesPage.SetActive(true);
                break;
            case InfinityWardrobeManager.ItemTypes.kDoor:
                InfinityWardrobeManager.Instance.panelUI.doorsPage.SetActive(true);
                break;
            case InfinityWardrobeManager.ItemTypes.kAccessory:
                InfinityWardrobeManager.Instance.panelUI.ActivateAccessories();

                InfinityWardrobeManager.Instance.panelUI.DeactivatePreviousPage();
                InfinityWardrobeManager.Instance.panelUI.CloseAllPages();
                break;
        } 
    }

    void Reset()
    {
        framesBtn.interactable = true;
        doorsBtn.interactable = true;
        accessoriesBtn.interactable = true;
    }

    /// <summary>
    /// Validate the scenario to enable, disable the products
    /// </summary>
    void Validate()
    {
        if(!InfinityWardrobeManager.Instance.IsDoorNeeded())
        {
            doorsBtn.interactable = false;
            framesBtn.interactable = true;
        }
        else
        {
            doorsBtn.interactable = true;
            framesBtn.interactable = false;
        }

        if(InfinityWardrobeManager.Instance.GetFrameCount() <= 0)
        {
            accessoriesBtn.interactable = false;
        }

        if(InfinityWardrobeManager.Instance.isCompositionFinished)
        {
            framesBtn.interactable = false;
        }

    }
}
