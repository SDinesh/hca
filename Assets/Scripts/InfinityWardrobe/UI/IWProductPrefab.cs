﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

/// <summary>
/// An instance of left side scrolls product prefab.
/// Used to manipulate the object related to it such as Frame, Door, Accessories
/// </summary>
public class IWProductPrefab : MonoBehaviour {

    public Image productIcon;
    public Text productName;
    public Text productPrice;
    public Button deleteBtn;
    public Button changeTextureBtn;

    public Image imageToHighlight;
    public Color highlightColor;
    public Color regularColor;

    // Holds the product reference related to this object
    private GameObject productRef;
    private InfinityWardrobeManager.ProductTypes productType;
	private int dividerSlotNumb;
	private Frame accessoryFrameParent;
    private string price;
    /// <summary>
    /// Sets the product detail to this prefab
    /// </summary>
    /// <param name="type">Type.</param>
    /// <param name="referingToObj">Refering to object.</param>

    void OnEnable()
    {
        InfinityWardrobeManager.OnFinish += FinishComposition;
    }

    void OnDisable()
    {
        InfinityWardrobeManager.OnFinish -= FinishComposition;
    }

	public void SetProductDetails(InfinityWardrobeManager.ProductTypes type, GameObject referingToObj, int dividerSlot, Frame frameParent = null)
    {
        productType = type;
        productRef = referingToObj;
		dividerSlotNumb = dividerSlot;
		accessoryFrameParent = frameParent;

        IDictionary itemDict = GetProductDetail(type);
        if(itemDict != null)
        {
            productIcon.sprite = InfinityWardrobeManager.Instance.prefabReferences.GetSpriteForItem(productType);
            productName.text = itemDict["ProductName"] as string;
            price = itemDict["Price"] as string;
            productPrice.text = price;
            InfinityWardrobeManager.Instance.panelUI.UpdateTotalPrice(price,true);

            deleteBtn.gameObject.SetActive(Convert.ToBoolean(itemDict["CanDelete"]));
            changeTextureBtn.gameObject.SetActive(Convert.ToBoolean(itemDict["CanChangeTexture"]));
        }
    }

    /// <summary>
    /// Returns the detail about the product in dictionary format to be displayed on UI Scroll
    /// </summary>
    /// <returns>The product detail.</returns>
    /// <param name="type">Type.</param>
    IDictionary GetProductDetail(InfinityWardrobeManager.ProductTypes type)
    {
        string key = ((int)type).ToString();
        if(InfinityWardrobeManager.Instance.IWData.Contains(key))
            return InfinityWardrobeManager.Instance.IWData[((int)type).ToString()] as IDictionary;

        return null;
    }

    public void OnProductSelect()
    {
        if(!InfinityWardrobeManager.Instance.isCompositionFinished)
        {
            InfinityWardrobeManager.Instance.panelUI.DisableSelectionInProductScroll();
            imageToHighlight.fillCenter = true;
            imageToHighlight.color = highlightColor;

            GotoProductSide();    
        }
    }

    public void OnProductDeselect()
    {
        imageToHighlight.fillCenter = false;
        imageToHighlight.color = regularColor;
    }

    void GotoProductSide()
    {
        if(productRef.GetComponent<Frame>())
        {
            Frame frame = productRef.GetComponent<Frame>();
            frame.GotoSide();
        }
        else if(productRef.GetComponent<WardrobePanel>())
        {
            WardrobePanel panel = productRef.GetComponent<WardrobePanel>();
            panel.GotoSide();
        }
        else if(productRef.GetComponent<Door>())
        {
            Door door = productRef.GetComponent<Door>();
            door.GotoSide();
        }
    }

    /// <summary>
    /// Delete button action event
    /// Call the manager to delete the frame for frames
    /// </summary>
    public void DeleteBtnAction()
    {
        Frame frame = productRef.GetComponent<Frame>();
        if(frame)
        {
            int frameIndex = InfinityWardrobeManager.Instance.frameList.IndexOf(frame);
            if(frame.type == InfinityWardrobeManager.ProductTypes.kFrameLShape)
            {
                InfinityWardrobeManager.Instance.RemoveFramesFromIndex(frameIndex);
            }
            else
            {
                if(InfinityWardrobeManager.Instance.ShouldRemoveAllFramesFrom(frame))
                    InfinityWardrobeManager.Instance.RemoveFramesFromIndex(frameIndex);
                else
                {
                    InfinityWardrobeManager.Instance.RemoveFrame(frame);
                    InfinityWardrobeManager.Instance.UpdateFramePosition();    

                    // If the removed frame is the last frame then enable the finish button. Coz the previous frame must be finished before.
                    if((frameIndex) == InfinityWardrobeManager.Instance.GetFrameCount())
                    {
                        print("Enabling the finish button");
                        InfinityWardrobeManager.Instance.panelUI.ManipulateFinishCompositionBtn(true);
                    }

                    InfinityWardrobeManager.Instance.panelUI.ManipulateShowHideBtn();
                }
            }
        }
        else 
        {
            Accessory accessory = productRef.GetComponent<Accessory>();
            accessory.ReleaseBlockedAccessorySlots();
        }

        Destroy(productRef);

		if (productType == InfinityWardrobeManager.ProductTypes.kAccessory100CMDivider) {
			
			print("Disable All Related to Divider----->"+ dividerSlotNumb + "frameparent -- >"+ accessoryFrameParent.name);
			RemoveDividerRelatedAccessories (accessoryFrameParent, dividerSlotNumb);
		}

		InfinityWardrobeManager.Instance.panelUI.MoveScrollToIndex (0);
    }


	void RemoveDividerRelatedAccessories(Frame frame, int count)
	{
		switch (count) {
		case 1:
			foreach (AccessoryRef obj in frame.dividerSlotsList1) {
				foreach (Transform child in obj.transform) {
					obj.IsOccupied = false;
					obj.BlockSlots(false);
					obj.BlockCount = 0;
					Destroy (child.gameObject);
				}
				obj.gameObject.SetActive (false);
			}
			break;
		case 2:
			foreach (AccessoryRef obj in frame.dividerSlotsList2) {
				foreach (Transform child in obj.transform) {
					obj.IsOccupied = false;
					obj.BlockSlots(false);
					obj.BlockCount = 0;
					Destroy (child.gameObject);
				}
				obj.gameObject.SetActive (false);
			}
			break;
		case 3:
			foreach (AccessoryRef obj in frame.dividerSlotsList3) {
				foreach (Transform child in obj.transform) {
					obj.IsOccupied = false;
					obj.BlockSlots(false);
					obj.BlockCount = 0;
					Destroy (child.gameObject);
				}
				obj.gameObject.SetActive (false);
			}
			break;
		}
	}


    /// <summary>
    /// Change texture button action event
    /// </summary>
    public void ChangeTextureBtnAction()
    {
        Door doorInstance = productRef.GetComponent<Door>();
        InfinityWardrobeManager.ProductTypes nextColor = InfinityWardrobeManager.Instance.prefabReferences.GetNextDoorType(doorInstance.color);
        doorInstance.UpdateMat(nextColor);

        InfinityWardrobeManager.Instance.panelUI.UpdateTotalPrice(price,false);
        SetProductDetails(nextColor,productRef,0);
    }

    void FinishComposition()
    {
        changeTextureBtn.interactable = false;
        deleteBtn.interactable = false;
    }

    void OnDestroy()
    {
        InfinityWardrobeManager.Instance.panelUI.UpdateTotalPrice(price,false);
        InfinityWardrobeManager.Instance.panelUI.RemoveObjectFromList(gameObject);
    }
}
