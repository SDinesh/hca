﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DoorsPanel : MonoBehaviour {

    public GameObject doorTypesPanel;
    public GameObject doorsPanel;

    public Button swingDoorBtn;
    public Button slidingDoorBtn;

    public GameObject swingDoorPanel;
    public GameObject slidingDoorPanel;

    void OnEnable()
    {
        if(!InfinityWardrobeManager.Instance.panelUI.pageStack.Contains(gameObject))
        {
            InfinityWardrobeManager.Instance.panelUI.DeactivatePreviousPage();
            InfinityWardrobeManager.Instance.panelUI.AddPage(gameObject);

            Reset();
            Validate();
        }
    }

    void Reset()
    {
        swingDoorBtn.interactable = true;
        slidingDoorBtn.interactable = true;

        swingDoorPanel.SetActive(false);
        slidingDoorPanel.SetActive(false);

        doorTypesPanel.SetActive(true);
    }
	
    /// <summary>
    /// Validates which type of door can be added for the current frame type
    /// </summary>
    void Validate()
    {
        if(InfinityWardrobeManager.Instance.currentFrameType == InfinityWardrobeManager.ProductTypes.kFrame45CM || InfinityWardrobeManager.Instance.currentFrameType == InfinityWardrobeManager.ProductTypes.kFrame90CM || 
            InfinityWardrobeManager.Instance.currentFrameType == InfinityWardrobeManager.ProductTypes.kFrameLShape)
        {
            slidingDoorBtn.interactable = false;
        }
        else if(InfinityWardrobeManager.Instance.currentFrameType == InfinityWardrobeManager.ProductTypes.kFrame180CM || InfinityWardrobeManager.Instance.currentFrameType == InfinityWardrobeManager.ProductTypes.kFrame270CM)
        {
            swingDoorBtn.interactable = false;
        }
    }

    [EnumActionAttribute(typeof(InfinityWardrobeManager.DoorTypes))]
    public void Navigate(int doorType)
    {
        InfinityWardrobeManager.DoorTypes type = (InfinityWardrobeManager.DoorTypes)doorType;
        print("Type "+type);
        switch(type)
        {
            case InfinityWardrobeManager.DoorTypes.swingDoor:
                swingDoorPanel.SetActive(true);
                break;
            case InfinityWardrobeManager.DoorTypes.slidingDoor:
                slidingDoorPanel.SetActive(true);
                break;
            default:
                break;
        } 
    }
}
