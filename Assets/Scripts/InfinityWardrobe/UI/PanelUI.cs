﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

/// <summary>
/// This class Handles most of the UI related functionality in Infinity wardrobe module.
/// </summary>
public class PanelUI : MonoBehaviour
{
    // Previous page reference, before coming to infinity wardrobe 
    public GameObject previousPage;

    [Header("Product pages")]
    public GameObject frameSizePage;
    public GameObject productCategoryPage;
    public GameObject framesPage;
    public GameObject doorsPage;

    [Header("Left Panel Items")]
    public Button addProductBtn;
    public Button closeBtn;
    public GameObject scrollBarObj;
    public GameObject iwItemPrefabRef;
    public Text totalPrice;
    public Text totalDimension;

    [Header("Common panel")]
    public Button hideDoorBtn;
    public Button showDoorBtn;
    public Button previousSideBtn;
    public Button nextSideBtn;
    public Button finishBtn;
    public Button clearCompositionBtn;
    public GameObject commonPanelBottom;

    [Header("Accessories")]
    public GameObject accessoriesScroll;

    [HideInInspector] public bool isInGamePlayArea = false;
    [HideInInspector] public List<GameObject> pageStack = new List<GameObject>();
    [HideInInspector] public List<GameObject> scrollProductsList = new List<GameObject>();
    [HideInInspector] public int currentVisibleSide = 0;

    RectTransform scrollBarRect;
    Vector2 scrollToMovePosition;
    bool shouldMoveScroll = false;

    float totalPriceValue;

    void OnEnable()
    {
        Reset();

        AddPage(previousPage);
        previousPage.SetActive(false);
        ResetPages();
        ResetCommonPanel();

        scrollBarRect = scrollBarObj.GetComponent<RectTransform>();
    }

    void Reset()
    {
        totalPriceValue = 0;
        isInGamePlayArea = false;

        addProductBtn.gameObject.SetActive(true);
        closeBtn.gameObject.SetActive(false);
        accessoriesScroll.SetActive(false);
        commonPanelBottom.SetActive(false);

        showDoorBtn.gameObject.SetActive(false);
        hideDoorBtn.gameObject.SetActive(true);
        hideDoorBtn.interactable = false;

        shouldMoveScroll = false;
        scrollToMovePosition = new Vector2(0,0);

        ManipulateClearCompositionBtn(false);
        ManipulateFinishCompositionBtn(false);
        ManipulateAddProductBtn(false);
    }
 
    void ResetPages()
    {
        frameSizePage.SetActive(true);
        productCategoryPage.SetActive(false);
        framesPage.SetActive(false);
        doorsPage.SetActive(false);
    }

    void ResetCommonPanel()
    {
        previousSideBtn.interactable = false;
        nextSideBtn.interactable = false;
    }

    #region Handling Pages
    /// <summary>
    /// Adds the page into list
    /// </summary>
    /// <param name="go">Go.</param>
    /// <param name="shouldAdd">If set to <c>true</c> should add.</param>
    public void AddPage(GameObject go = null,bool shouldAdd = true)
    {
        if(shouldAdd)
        {
            if(!pageStack.Contains(go))
                pageStack.Add(go);
        }
        else
            pageStack.RemoveAt(pageStack.Count - 1);
    }

    public void DeactivatePreviousPage()
    {
        GameObject previousPage = GetLastPage();
        if(previousPage) 
        {
            previousPage.SetActive(false);
            AddPage(previousPage);    
        }
    }

    public void CloseAllPages()
    {
        for(int i = 0; i < pageStack.Count - 1; i++)
        {
            AddPage(null,false);
        }

        ManipulateAddProductBtn(true);
        isInGamePlayArea = true;
    }

    /// <summary>
    /// Handles the back button functionality through out Infinity wardrobe
    /// </summary>
    public void GoBack()
    {
        if(!isInGamePlayArea)
        {
            GameObject page = GetLastPage();
            if(page) {

                page.SetActive(false);
                RemovePage(page);
            }

            if(page && !page.name.Equals("ProductListPanel"))
            {
                page = GetLastPage();
                if(page)
                    page.SetActive(true);            

                if(pageStack.Count == 1)
                {
                    C_StellarBed_Manager.Instance.PreviewMakeYourOwnCompose.SetActive(true);
                    pageStack.RemoveAt(0);
                    InfinityWardrobeManager.Instance.gameObject.SetActive(false);
                }
            }
            else if(page)
            {
                isInGamePlayArea = true;
                ManipulateAddProductBtn(true);
            }

            if(page == null)
                ManipulateAddProductBtn(true);
        }
        else
        {
            // Going back from gameplay area to height selection
            GameObject page = GetLastPage();
            if(page)
                page.SetActive(true);

            Reset();
            InfinityWardrobeManager.Instance.ClearWardrobe();
            InfinityWardrobeManager.Instance.Reset();
        }
    }

    GameObject GetLastPage()
    {
        GameObject obj = null;
        if(pageStack.Count > 0)
        {
            obj = pageStack[pageStack.Count - 1];
        }
        return obj;
    }

    void RemovePage(GameObject obj)
    {
        pageStack.Remove(obj);
    }

    #endregion

    #region Left Panel Functionalities
    /// <summary>
    /// Manipulates the add product button 
    /// Should be called based on current page we are in. So that user shouldn't able to press this button
    /// </summary>
    /// <param name="isActive">If set to <c>true</c> is active.</param>
    public void ManipulateAddProductBtn(bool isActive)
    {
        addProductBtn.interactable = isActive;
        if(!InfinityWardrobeManager.Instance.isCompositionFinished)
            commonPanelBottom.SetActive(isActive);
    }

    /// <summary>
    /// Will be called when clicking on Add product in left panel
    /// </summary>
    public void AddProductBtnClick()
    {
        productCategoryPage.SetActive(true);
        ManipulateAddProductBtn(false);
        isInGamePlayArea = false;
    }

    /// <summary>
    /// Adds the product in left side scroll
    /// </summary>
    /// <returns>The product in scroll.</returns>
    /// <param name="type">Type.</param>
    /// <param name="productRef">Product reference.</param>
	public GameObject AddProductInScroll(InfinityWardrobeManager.ProductTypes type, GameObject productRef, int dividerSlot, Frame frameParent = null)
    {
        GameObject obj = Instantiate(iwItemPrefabRef) as GameObject;
        obj.transform.SetParent(scrollBarObj.transform); 
        obj.GetComponent<RectTransform>().localScale = Vector3.one;
		obj.GetComponent<IWProductPrefab>().SetProductDetails(type,productRef, dividerSlot, frameParent);

        obj.SetActive(true);

        scrollProductsList.Add(obj);

        if(scrollProductsList.Count > 3)
        {
            MoveScrollToIndex(scrollProductsList.Count - 3);
        }

        ManipulateClearCompositionBtn(true);

        return obj;
    }

    /// <summary>
    /// Removes the given gameobject from the maintained scroll products list
    /// </summary>
    /// <param name="obj">Object.</param>
    public void RemoveObjectFromList(GameObject obj)
    {
        scrollProductsList.Remove(obj);

        if(scrollProductsList.Count <= 0) {

            ManipulateClearCompositionBtn(false);
            ManipulateFinishCompositionBtn(false);
        }
    }

    /// <summary>
    /// Activates accessories scroll
    /// </summary>
    public void ActivateAccessories()
    {
        addProductBtn.gameObject.SetActive(false);
        accessoriesScroll.SetActive(true);
        closeBtn.gameObject.SetActive(true);
            
        // If the doors are visible, disable them 
        if(IsDoorAvailable())
        {
            InfinityWardrobeManager.Instance.ToggleDoors(false);
            showDoorBtn.gameObject.SetActive(true);
            hideDoorBtn.gameObject.SetActive(false);
            showDoorBtn.interactable = false;
        }
    }

    /// <summary>
    /// Closing the accessory scroll
    /// </summary>
    public void CloseBtnAction()
    {
        closeBtn.gameObject.SetActive(false);
        accessoriesScroll.SetActive(false);
        if(!InfinityWardrobeManager.Instance.isCompositionFinished)
            addProductBtn.gameObject.SetActive(true);

        if(IsDoorAvailable())
            showDoorBtn.interactable = true;
    }

    void Update()
    {
        // Moves the scroll to the desired position
        if(shouldMoveScroll)
        {
            float distance = Vector2.Distance(scrollBarRect.anchoredPosition,scrollToMovePosition);
            if(distance > 2)
                scrollBarRect.anchoredPosition = Vector2.MoveTowards(scrollBarRect.anchoredPosition,scrollToMovePosition,100f);
            else
                shouldMoveScroll = false;
        }
    }

    public void MoveScrollToIndex(GameObject obj)
    {
        DisableSelectionInProductScroll();

        obj.GetComponent<IWProductPrefab>().OnProductSelect();
        MoveScrollToIndex(scrollProductsList.IndexOf(obj));
    }

	public void MoveScrollToIndex(int index)
    {
        scrollToMovePosition = new Vector2(0,(index * 150) + 5);
        shouldMoveScroll = true;
    }

    public void DisableSelectionInProductScroll()
    {
        for(int i = 0; i < scrollProductsList.Count; i++)
        {
            GameObject obj = scrollProductsList[i];
            obj.GetComponent<IWProductPrefab>().OnProductDeselect();
        }
    }

    #endregion
   
    #region Bottom Panel functionalities
    /// <summary>
    /// Toggles the doors visibility
    /// </summary>
    public void ToggleDoorsVisibility()
    {
        if(!showDoorBtn.gameObject.activeInHierarchy)
        {
            InfinityWardrobeManager.Instance.ToggleDoors(false);
            showDoorBtn.gameObject.SetActive(true);
            hideDoorBtn.gameObject.SetActive(false);
        }
        else
        {
            InfinityWardrobeManager.Instance.ToggleDoors(true);
            showDoorBtn.gameObject.SetActive(false);
            hideDoorBtn.gameObject.SetActive(true);
        }    
    }

    public void ManipulateShowHideBtn()
    {
        if(InfinityWardrobeManager.Instance.frameList.Count > 0)
        {
            Frame firstFrame = InfinityWardrobeManager.Instance.frameList[0];
            if(firstFrame.currentDoorCount > 0)
            {
                hideDoorBtn.interactable = true;
            }

            if(!hideDoorBtn.gameObject.activeInHierarchy)
            {
                hideDoorBtn.gameObject.SetActive(true);
                showDoorBtn.gameObject.SetActive(false);
                InfinityWardrobeManager.Instance.ToggleDoors(true);
            }
        }
        else
        {
            showDoorBtn.gameObject.SetActive(false);
            hideDoorBtn.gameObject.SetActive(true);
            hideDoorBtn.interactable = false;
        }
    }

    /// <summary>
    /// This method Manipulates the next and previous button based on current side of wardrobe
    /// </summary>
    /// <param name="side">Side.</param>
    public void UpdateVisibleSide(int side)
    {
        currentVisibleSide = side;

        if(currentVisibleSide == 0)
        {
            previousSideBtn.interactable = false;
            if(InfinityWardrobeManager.Instance.totalSides > 0)
                nextSideBtn.interactable = true;
        }
        else if(currentVisibleSide == 1)
        {
            previousSideBtn.interactable = true;
            if(InfinityWardrobeManager.Instance.totalSides > 1)
                nextSideBtn.interactable = true;
            else
                nextSideBtn.interactable = false;
        }
        else if(currentVisibleSide == 2)
        {
            nextSideBtn.interactable = false;
            previousSideBtn.interactable = true;
        }
    }

    public void MoveToNextSide()
    {
        InfinityWardrobeManager.Instance.ShowSide(currentVisibleSide + 1);
    }

    public void MoveToPreviousSide()
    {
        InfinityWardrobeManager.Instance.ShowSide(currentVisibleSide - 1);
    }

    public void ClearCompositionBtnAction()
    {
        InfinityWardrobeManager.Instance.ClearWardrobe();
        InfinityWardrobeManager.Instance.ResetCamera();

        shouldMoveScroll = false;
        scrollToMovePosition = new Vector2(0,0);

        ManipulateShowHideBtn();
        ManipulateAddProductBtn(true);
        CloseBtnAction();
    }

    public void FinishCompositionBtnAction()
    {
        ManipulateFinishCompositionBtn(false);

        InfinityWardrobeManager.Instance.CompleteComposition();

        nextSideBtn.interactable = false;
        previousSideBtn.interactable = false;
        ManipulateAddProductBtn(false);

        CloseBtnAction();
    }

    void ManipulateClearCompositionBtn(bool isInteractable)
    {
        clearCompositionBtn.interactable = isInteractable;
    }

    public void ManipulateFinishCompositionBtn(bool isInteractable)
    {
        finishBtn.interactable = isInteractable;
    }

    #endregion
    /// <summary>
    /// Updates the total price.
    /// </summary>
    /// <param name="value">Value.</param>
    /// <param name="shouldAdd">If set to <c>true</c> should add.</param>
    public void UpdateTotalPrice(string value, bool shouldAdd)
    {
        float floatValue = (float)Convert.ToDouble(value);
        if(shouldAdd)
        {
            totalPriceValue += floatValue;
        }
        else
        {
            totalPriceValue -= floatValue;
        }

        if(totalPriceValue < 0) {

            totalPrice.text = "0";
            totalPriceValue = 0;
        }

        totalPrice.text = "AED "+totalPriceValue.ToString();
    }

    /// <summary>
    /// Updates the total dimension.
    /// Not implemented yet.
    /// </summary>
    public void UpdateTotalDimension()
    {
//        totalDimension.text = "100 x 100 x 100";
    }

    /// <summary>
    /// Determines the presence of door by checking the very first frame
    /// </summary>
    /// <returns><c>true</c> if this instance is door available; otherwise, <c>false</c>.</returns>
    bool IsDoorAvailable()
    {
        if(InfinityWardrobeManager.Instance.frameList.Count > 0)
        {
            Frame firstFrame = InfinityWardrobeManager.Instance.frameList[0];
            if(firstFrame.currentDoorCount > 0)
                return true;
        }
        return false;
    }
}
