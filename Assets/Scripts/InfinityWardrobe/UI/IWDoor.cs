﻿using UnityEngine;
using System.Collections;

public class IWDoor : MonoBehaviour {

    public InfinityWardrobeManager.DoorTypes type;

    void OnEnable()
    {
//        InfinityWardrobeManager.Instance.DeactivatePreviousPage();
//        InfinityWardrobeManager.Instance.AddPage(gameObject);
    }

    [EnumActionAttribute(typeof(InfinityWardrobeManager.ProductTypes))]
    public void AddDoor(int id)
    {
        InfinityWardrobeManager.ProductTypes color = (InfinityWardrobeManager.ProductTypes)id;
        InfinityWardrobeManager.Instance.AddDoor(color);

        InfinityWardrobeManager.Instance.panelUI.DeactivatePreviousPage();
        InfinityWardrobeManager.Instance.panelUI.CloseAllPages();
    }
}
