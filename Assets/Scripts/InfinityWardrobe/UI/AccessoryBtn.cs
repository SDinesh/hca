﻿using UnityEngine;
using System.Collections;

public class AccessoryBtn : MonoBehaviour {

    public InfinityWardrobeManager.ProductTypes type;

    public void OnClick()
    {
        InfinityWardrobeManager.Instance.AddAccessory(type);
    }
}
