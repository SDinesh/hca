﻿using UnityEngine;
using System.Collections;

public class FrameHeightSelection : MonoBehaviour {

    void OnEnable()
    {
        if(!InfinityWardrobeManager.Instance.panelUI.pageStack.Contains(gameObject))
        {
            InfinityWardrobeManager.Instance.panelUI.AddPage(gameObject);
        }
    }

    [EnumActionAttribute(typeof(InfinityWardrobeManager.FrameHeights))]
    public void Navigate(int index)
    {
        InfinityWardrobeManager.FrameHeights frameHeight = (InfinityWardrobeManager.FrameHeights)index;
        InfinityWardrobeManager.Instance.selectedFrameHeight = frameHeight;

        InfinityWardrobeManager.Instance.panelUI.framesPage.SetActive(true);
    }
}
