﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FramesPanel : MonoBehaviour {

    public Button k_45CMBtn;
    public Button k_90CMBtn;
    public Button k_180CMBtn;
    public Button k_270CMBtn;
    public Button k_LShapeBtn;

    void OnEnable()
    {
        if(!InfinityWardrobeManager.Instance.panelUI.pageStack.Contains(gameObject))
        {
            InfinityWardrobeManager.Instance.panelUI.DeactivatePreviousPage();
            InfinityWardrobeManager.Instance.panelUI.AddPage(gameObject);    
        
            Reset();
            CheckPossibleFramesToBeAdded();
        }
    }

    void Reset()
    {
        k_45CMBtn.interactable = true;
        k_90CMBtn.interactable = true;
        k_180CMBtn.interactable = true;
        k_270CMBtn.interactable = true;
        k_LShapeBtn.interactable = true;
    }

    /// <summary>
    /// Checks the possible type of frames can be added.
    /// </summary>
    void CheckPossibleFramesToBeAdded() 
    {
        k_45CMBtn.interactable = InfinityWardrobeManager.Instance.CanAddFrameInRow(InfinityWardrobeManager.ProductTypes.kFrame45CM);
        k_90CMBtn.interactable = InfinityWardrobeManager.Instance.CanAddFrameInRow(InfinityWardrobeManager.ProductTypes.kFrame90CM);
        k_180CMBtn.interactable = InfinityWardrobeManager.Instance.CanAddFrameInRow(InfinityWardrobeManager.ProductTypes.kFrame180CM);
        k_270CMBtn.interactable = InfinityWardrobeManager.Instance.CanAddFrameInRow(InfinityWardrobeManager.ProductTypes.kFrame270CM);
        k_LShapeBtn.interactable = InfinityWardrobeManager.Instance.CanAddFrameInRow(InfinityWardrobeManager.ProductTypes.kFrameLShape);
    }

    [EnumActionAttribute(typeof(InfinityWardrobeManager.ProductTypes))]
    public void SelectItem(int index)
    {
        InfinityWardrobeManager.ProductTypes type = (InfinityWardrobeManager.ProductTypes)index;
        InfinityWardrobeManager.Instance.AddFrame(type);

        InfinityWardrobeManager.Instance.panelUI.DeactivatePreviousPage();
        InfinityWardrobeManager.Instance.panelUI.CloseAllPages();
    }
}
