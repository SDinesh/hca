﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// This class represents the instance of Accessory
/// </summary>
public class Accessory : MonoBehaviour {

    public InfinityWardrobeManager.ProductTypes type;

    BoxCollider thisCollider;
    private List<Collider> otherColliders = new List<Collider>();

    private bool isAccessoryAttached = false;
    private GameObject uiScrollRef = null;
    private AccessoryRef accessoryRef = null;

    void OnEnable()
    {
        InfinityWardrobeManager.OnMouseUp += CheckCollision;
    }

    void OnDisable()
    {
        InfinityWardrobeManager.OnMouseUp -= CheckCollision;
    }

    void Start()
    {
        thisCollider = gameObject.GetComponent<BoxCollider>();
    }

    /// <summary>
    /// Checks the collision with Accessory reference slots
    /// </summary>
    /// <param name="other">Other.</param>
    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("AccessoryRef"))
        {
            if(!other.gameObject.GetComponent<AccessoryRef>().IsOccupied && other.gameObject.GetComponent<AccessoryRef>().BlockCount == 0)
                otherColliders.Add(other);
            else
            {
                
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.gameObject.CompareTag("AccessoryRef"))
        {
            otherColliders.Remove(other);
        }
    }

    /// <summary>
    /// Returns the percentage of intersection between two Bounds
    /// </summary>
    /// <returns>The contained percentage.</returns>
    /// <param name="obj">Object.</param>
    /// <param name="region">Region.</param>
    private float BoundsContainedPercentage( Bounds obj, Bounds region )
    {
        var total = 1f;

        for ( var i = 0; i < 3; i++ )
        {
            var dist = obj.min[i] > region.center[i] ?
                obj.max[i] - region.max[i] :
                region.min[i] - obj.min[i];

            total *= Mathf.Clamp01(1f - dist / obj.size[i]);
        }

        return total;
    }

    /// <summary>
    /// Checks for the collision here.
    /// And makes the decision to fit.
    /// </summary>
    void CheckCollision()
    {
        if(!isAccessoryAttached)
        {
            AccessoryRef higherPercentageObj = null;
            float higherPercentage = 0;
            foreach(Collider otherCollider in otherColliders)
            {
                AccessoryRef accessoryRef = otherCollider.gameObject.GetComponent<AccessoryRef>();
                if(CanAttachToSlot(accessoryRef))
                {
                    float percentage = BoundsContainedPercentage(otherCollider.bounds,thisCollider.bounds);
                    if(percentage > 0 && higherPercentage < percentage)
                    {
                        higherPercentage = percentage;
                        higherPercentageObj = accessoryRef;
                    }
                }
            }

            if(higherPercentageObj)
            {
                AttachAccessory(higherPercentageObj);
            }

            if(!higherPercentageObj)
            {
                Destroy(gameObject);
            }
        }
    }

    bool CanAttachToSlot(AccessoryRef accRef)
    {
        bool canAttach = false;
        switch(accRef.type)
        {
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMShelf:
                if(type == InfinityWardrobeManager.ProductTypes.kAccessory90CMPulloutShelf ||
                    type == InfinityWardrobeManager.ProductTypes.kAccessory90CMShelf ||
					type == InfinityWardrobeManager.ProductTypes.kAccessory90CMTieBeltRack ||
					type == InfinityWardrobeManager.ProductTypes.kAccessory90CMMetalBasket)
                {
                    canAttach = true;
                }
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CM3DChest:
                if(type == InfinityWardrobeManager.ProductTypes.kAccessory90CM3DChest || 
                    type == InfinityWardrobeManager.ProductTypes.kAccessory90CM3DGlassChest)
                    canAttach = true;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory90CMPantHanger:
                if(type == InfinityWardrobeManager.ProductTypes.kAccessory90CMPantHanger)
                    canAttach = true;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory45CM3DChest:
                if(type == InfinityWardrobeManager.ProductTypes.kAccessory45CM3DChest || 
                    type == InfinityWardrobeManager.ProductTypes.kAccessory45CM3DGlassChest)
                    canAttach = true;
                break;
            case InfinityWardrobeManager.ProductTypes.kAccessory45CMShelf:
				if(type == InfinityWardrobeManager.ProductTypes.kAccessory45CMShelf || 
					type == InfinityWardrobeManager.ProductTypes.kAccessory45CMPulloutShelf ||
					type == InfinityWardrobeManager.ProductTypes.kAccessory45CMMetalBasket )
                    canAttach = true;
                break;
			case InfinityWardrobeManager.ProductTypes.kAccessoryLShapedShelf:
				if(type == InfinityWardrobeManager.ProductTypes.kAccessoryLShapedShelf)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessoryLShapedHangingRod:
				if(type == InfinityWardrobeManager.ProductTypes.kAccessoryLShapedHangingRod)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory45CMHangingRod:
				if(type == InfinityWardrobeManager.ProductTypes.kAccessory45CMHangingRod)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMHangingRod:
				if(type == InfinityWardrobeManager.ProductTypes.kAccessory90CMHangingRod)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMPullDownHanger:
				if(type == InfinityWardrobeManager.ProductTypes.kAccessory90CMPullDownHanger)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory90CMGlassShelves:
				if(type == InfinityWardrobeManager.ProductTypes.kAccessory90CMGlassShelves)
					canAttach = true;
				break;
			case InfinityWardrobeManager.ProductTypes.kAccessory100CMDivider:
				if(type == InfinityWardrobeManager.ProductTypes.kAccessory100CMDivider)
					canAttach = true;
				break;
        }

        return canAttach;
    }

    void AttachAccessory(AccessoryRef _accessoryRef)
    {
        accessoryRef = _accessoryRef;
        InfinityWardrobeManager.Instance.generatedAccessoryObj = null;

        accessoryRef.IsOccupied = true;
        accessoryRef.BlockSlots(true);
        transform.position = _accessoryRef.transform.position;
        transform.parent = _accessoryRef.transform;
		if (type == InfinityWardrobeManager.ProductTypes.kAccessory100CMDivider)
		{
			//Debug.Log ("CHANGE IN ANGLE");
			transform.eulerAngles = new Vector3(270, 0, 0);
			transform.position = new Vector3(_accessoryRef.transform.position.x, _accessoryRef.transform.position.y, _accessoryRef.transform.position.z + 1.26f);
			GetMainParent (transform.gameObject, accessoryRef.dividerSlotCount);
		}
		else if (type == InfinityWardrobeManager.ProductTypes.kAccessoryLShapedShelf)
		{
			Debug.Log ("CHANGE IN ANGLE");
			transform.localRotation = Quaternion.Euler (180, 180, 180);
		}
		else if (type == InfinityWardrobeManager.ProductTypes.kAccessoryLShapedHangingRod)
		{
			Debug.Log ("CHANGE IN ANGLE");
			transform.localRotation = Quaternion.Euler (180, 90, 180);
		}
		uiScrollRef = InfinityWardrobeManager.Instance.panelUI.AddProductInScroll(type, gameObject, accessoryRef.dividerSlotCount, GetFrameObject(transform.gameObject));

        isAccessoryAttached = true;
        otherColliders.Clear();
        otherColliders = null;
        thisCollider.enabled = false;
    }

	void GetMainParent(GameObject _obj, int count)
	{
		Transform parent = _obj.transform.parent;
		Transform refParent = parent.transform.parent;
		Transform MainFrame = refParent.transform.parent;
		//Debug.Log ("CHANGE IN ANGLE ----- >"+ MainFrame.name);

		Frame frame = MainFrame.GetComponent<Frame> ();
		switch (count) {
		case 1:
			foreach (AccessoryRef obj in frame.dividerSlotsList1) {
				obj.gameObject.SetActive (true);
			}
			break;
		case 2:
			foreach (AccessoryRef obj in frame.dividerSlotsList2) {
				obj.gameObject.SetActive (true);
			}
			break;
		case 3:
			foreach (AccessoryRef obj in frame.dividerSlotsList3) {
				obj.gameObject.SetActive (true);
			}
			break;
		}

	}

	Frame GetFrameObject(GameObject _obj)
	{
		Transform parent = _obj.transform.parent;
		Transform refParent = parent.transform.parent;
		Transform MainFrame = refParent.transform.parent;
		//Debug.Log ("CHANGE IN ANGLE ----- >"+ MainFrame.name);

		Frame frame = MainFrame.GetComponent<Frame> ();
		return frame;
	}



    public void ReleaseBlockedAccessorySlots()
    {
        accessoryRef.IsOccupied = false;
        accessoryRef.BlockSlots(false);
    }

    void OnDestroy()
    {
        if(uiScrollRef)
        {
            Destroy(uiScrollRef);
        }
    }
}
