﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Used in accessory reference points in the frame
/// </summary>
public class AccessoryRef : MonoBehaviour
{
    public InfinityWardrobeManager.ProductTypes type;
    private bool _isOccupied = false;
	[SerializeField]
    private int blockCount = 0;

	public List<AccessoryRef> slotsToBlock;

	public MeshRenderer[] meshRenderToHighlight;

	public int dividerSlotCount;

    public bool IsOccupied
    {
        get 
        { 
            return _isOccupied;
        }
        set 
        { 
            _isOccupied = value;
        }
    }

    public int BlockCount
    {
        get
        {
            return blockCount;
        }
		set
		{
			blockCount = value;
		}
    }

    /// <summary>
    /// This enables the object position
    /// </summary>
    /// <param name="shouldShow">If set to <c>true</c> should show.</param>
    public void ShowAvailability(bool shouldShow)
    {
        //gameObject.GetComponent<MeshRenderer>().enabled = shouldShow;
		for (int i = 0; i < meshRenderToHighlight.Length; i++) 
		{
			meshRenderToHighlight [i].enabled = shouldShow;
		}
    }

    /// <summary>
    /// Disables the slots which is occupied by this accessory.
    /// </summary>
    public void BlockSlots(bool shouldBlock)
    {
        for(int i = 0; i < slotsToBlock.Count; i++)
        {
            AccessoryRef accRef = slotsToBlock[i];

            if(shouldBlock)
                accRef.blockCount++;
            else
                accRef.blockCount--;
        }
    }
}
