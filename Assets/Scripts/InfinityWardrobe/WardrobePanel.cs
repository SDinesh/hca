﻿using UnityEngine;
using System.Collections;

public class WardrobePanel : MonoBehaviour {

    public InfinityWardrobeManager.ProductTypes productType;
    public int productSideInFrame = 0;

    private GameObject uiScrollRef = null;

    public void SetProductDetails(int side)
    {
        productSideInFrame = side;

        if(!uiScrollRef)
            uiScrollRef = InfinityWardrobeManager.Instance.panelUI.AddProductInScroll(productType,gameObject,0);    
    }

    public void GotoSide()
    {
        InfinityWardrobeManager.Instance.ShowSide(productSideInFrame);
    }

    void OnDestroy()
    {
        Destroy(uiScrollRef);
    }
}
