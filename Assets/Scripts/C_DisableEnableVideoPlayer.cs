﻿using UnityEngine;
using System.Collections;

public class C_DisableEnableVideoPlayer : MonoBehaviour {

	private static C_DisableEnableVideoPlayer instance;
	public static C_DisableEnableVideoPlayer Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_DisableEnableVideoPlayer> ();

			return instance;
		}
	}

	public GameObject VideoPlayer;

	public void f_DisableEnablePlayer()
	{
		VideoPlayer.SetActive (false);
		VideoPlayer.SetActive (true);
	}
}
