﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class C_MainpageUIManager : MonoBehaviour {


	public Image g_StoreMapToolBarBtn;
	public Image g_CatalogueToolBarBtn;
	public Image g_ModularProgramToolBarBtn;
	public Image g_OffersToolBarBtn;
	public Image g_ServicesToolBarBtn;
	public Image g_ShukranToolBarBtn;
	public Image g_WebsiteToolBarBtn;
	public Image g_SocialMediaToolBarBtn;
	public Image g_ComingSoonToolBarBtn;

	public GameObject g_ModularProgramButton;
	public GameObject g_OffersButton;



	void OnEnable () 
	{
		C_GlobalVariables.instance.g_SelectedArea = E_Area.Sleeping;
		if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Entrance) 
		{
			g_ModularProgramButton.SetActive (false);
			g_OffersButton.SetActive (true);
		} 
		else 
		{
			g_ModularProgramButton.SetActive (true);
			g_OffersButton.SetActive (false);
			if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Living) {
				g_ModularProgramButton.transform.GetChild (0).gameObject.GetComponent<Text> ().text = "SIGNATURE SOFA PROGRAM";
			} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Dining) {
				g_ModularProgramButton.transform.GetChild (0).gameObject.GetComponent<Text> ().text = "MIX AND MATCH DINING PROGRAM";
			} else if (C_GlobalVariables.instance.g_SelectedArea == E_Area.Sleeping) {
				g_ModularProgramButton.transform.GetChild (0).gameObject.GetComponent<Text> ().text = "SLEEPING PROGRAM";
			}
		}
	}

	public void f_ManageToolbarButtons(int l_ServiceCode)
	{
		g_StoreMapToolBarBtn.color 			= Color.white;
		g_CatalogueToolBarBtn.color 		= Color.white;
		g_ModularProgramToolBarBtn.color 	= Color.white;
		g_ServicesToolBarBtn.color 			= Color.white;
		g_ShukranToolBarBtn.color 			= Color.white;
		g_WebsiteToolBarBtn.color 			= Color.white;
		g_SocialMediaToolBarBtn.color 		= Color.white;
		g_ComingSoonToolBarBtn.color 		= Color.white;
		g_OffersToolBarBtn.color 			= Color.white;

		switch(l_ServiceCode)
		{
		case 1:
				g_StoreMapToolBarBtn.color 			= new Color (193.0f/255,216.0f/255,47.0f/255);
			break;

		case 2:
				g_CatalogueToolBarBtn.color 		= new Color (193.0f/255,216.0f/255,47.0f/255);
			break;

		case 3:
				g_ModularProgramToolBarBtn.color 	= new Color (193.0f/255,216.0f/255,47.0f/255);
			break;

		case 4:
				g_ServicesToolBarBtn.color 			= new Color (193.0f/255,216.0f/255,47.0f/255);
			break;

		case 5:
				g_ShukranToolBarBtn.color 			= new Color (193.0f/255,216.0f/255,47.0f/255);
			break;

		case 6:
				g_WebsiteToolBarBtn.color 			= new Color (193.0f/255,216.0f/255,47.0f/255);
			break;

		case 7:
				g_SocialMediaToolBarBtn.color 		= new Color (193.0f/255,216.0f/255,47.0f/255);
			break;

		case 8:
				g_OffersToolBarBtn.color 			= new Color (193.0f/255,216.0f/255,47.0f/255);
			break;

		case 9:
				g_ComingSoonToolBarBtn.color 		= new Color (193.0f/255,216.0f/255,47.0f/255);
			break;


		}
	}
}
