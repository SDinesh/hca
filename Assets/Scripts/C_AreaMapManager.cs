using UnityEngine;
using System.Collections;


public class C_AreaMapManager : MonoBehaviour {

	private static C_AreaMapManager instance;
	public static C_AreaMapManager Instance{
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_AreaMapManager> ();

			return instance;
		}
	}
	public Color g_ResetColor;
	public GameObject[] g_AreaObjs;
	public GameObject g_BlurPlane;
	public GameObject g_SimpleMap;
	public GameObject g_DetailMap;
	// Use this for initialization
	public GameObject g_SelectedObj;
	bool g_IsAnimationActive = false;
	bool g_IsAreaObjActive = false;
	float g_AnimationSpeed = 0;
	E_Area g_NextArea;

	public Material g_LivingFurniture;
	public Material g_FurnitureCollection;
	public Material g_ShopOnline;
	public Material g_HomeOffice;
	public Material g_Cushions;
	public Material g_Garden_Balcony;
	public Material g_BarCabin;
	public Material g_CashTills;
	public Material g_CustomerServices;

	public Material g_Lighting;
	public Material g_Gallery;
	public Material g_FloorCovering;
	public Material g_CandleShop;
	public Material g_HomeDecor_Gifts;
	public Material g_Curtains;

	public Material g_Crockery;
	public Material g_Glassware;
	public Material g_KitchenAccessories;
	public Material g_DiningFurniture;
	public Material g_Dreamroom;

	public Material g_BedroomFurniture;
	public Material g_Bedding;
	public Material g_Pillow;
	public Material g_MattressShope;
	public Material g_Wardrobes;
	public Material g_Bath;
	public Material g_Laundry;
	public Material g_Headboards;

	public Material g_Prayer;
	public Material g_KidsFurniture;
	public Material g_Nursery;
	public Material g_KidsMattress;
	public Material g_KidsHousehold;


	void OnEnable () 
	{
		g_AnimationSpeed = 5;
		g_IsAnimationActive = false;
		g_IsAreaObjActive = false;
		g_BlurPlane.SetActive (false);
		for(int i =0;i<9;i++)
		{
			g_AreaObjs [i].SetActive (false);
		}
	}

	Ray g_Ray;
	RaycastHit g_RaycastHit;
	void Update()
	{
		if(Input.GetMouseButtonDown(0))
		{
			if (!g_IsAnimationActive && g_IsAreaObjActive)
			{
				g_Ray = Camera.main.ScreenPointToRay (Input.mousePosition);

				if(Physics.Raycast(g_Ray,out g_RaycastHit))
				{
					StartCoroutine (HighlightObjAfterDelay(g_RaycastHit.transform.name));
					StartCoroutine (e_ScaleDownAnimation());
					C_WaypointManager.Instance.f_SetWayPoints (g_RaycastHit.transform.name);
					Debug.Log (g_RaycastHit.transform.name);
				}
			}


		}

	}

	public void f_HighlightArea(string l_AreaName)
	{
		//C_GlobalVariables.instance.g_LastTouchTime = Time.time;
		C_StoreMapUIManager.Instance.f_Reset_Images ();
		StartCoroutine (HighlightObjAfterDelay(l_AreaName));
		C_WaypointManager.Instance.f_SetWayPoints (l_AreaName);
	}

	public void f_ActivateArea(E_Area l_Area)
	{

		switch (l_Area) 
		{
		case E_Area.Living:
			C_StoreMapUIManager.Instance.f_AreaSelectionHandler (1);
			break;

		case E_Area.Decorating:
			C_StoreMapUIManager.Instance.f_AreaSelectionHandler (2);
			break;

		case E_Area.Dining:
			C_StoreMapUIManager.Instance.f_AreaSelectionHandler(3);
			break;

		case E_Area.Dreamroom:
			C_StoreMapUIManager.Instance.f_AreaSelectionHandler(4);
			break;

		case E_Area.Playing:
			C_StoreMapUIManager.Instance.f_AreaSelectionHandler(6);
			break;

		case E_Area.Sleeping:
			C_StoreMapUIManager.Instance.f_AreaSelectionHandler(5);
			break;

		case E_Area.CustomerService:
			C_StoreMapUIManager.Instance.f_AreaSelectionHandler(7);
			break;

		case E_Area.Prayerroom:
			C_StoreMapUIManager.Instance.f_AreaSelectionHandler(8);
			break;

		case E_Area.CashTills:
			C_StoreMapUIManager.Instance.f_AreaSelectionHandler(9);
			break;

		

		}


		/*if (g_IsAnimationActive)
		{
			return;
		}
		if(g_IsAreaObjActive)
		{
			StartCoroutine (e_ScaleDownAnimation());
			g_NextArea = l_Area;
			return;
		}


		for(int i =0;i<9;i++)
		{
			g_AreaObjs [i].SetActive (false);
		}

		g_IsAnimationActive = true;
		g_IsAreaObjActive = false;
		
		switch(l_Area)
		{
		case E_Area.Living:
			g_SelectedObj = g_AreaObjs [0];
			break;

		case E_Area.Decorating:
			g_SelectedObj = g_AreaObjs [1];
			break;

		case E_Area.Dining:
			g_SelectedObj = g_AreaObjs [2];
			break;

		case E_Area.Dreamroom:
			g_SelectedObj = g_AreaObjs [3];
			break;

		case E_Area.Sleeping:
			g_SelectedObj = g_AreaObjs [4];
			break;

		case E_Area.Playing:
			g_SelectedObj = g_AreaObjs [5];
			break;

		case E_Area.CustomerService:
			g_SelectedObj = g_AreaObjs [6];
			break;

		case E_Area.Prayerroom:
			g_SelectedObj = g_AreaObjs [7];
			break;

		case E_Area.CashTills:
			g_SelectedObj = g_AreaObjs [8];
			break;
		}
		g_SelectedObj.SetActive (true);
		g_SelectedObj.transform.localScale = Vector3.zero;
		g_BlurPlane.SetActive (true);
		StartCoroutine (e_ScaleUpAnimation());
		*/
	}


		IEnumerator e_ScaleUpAnimation()
		{
			float l_LerpVal = 0;
			while(l_LerpVal<1)
			{
				yield return null;
				g_SelectedObj.transform.localScale = Vector3.Lerp (Vector3.zero, Vector3.one, l_LerpVal);
				l_LerpVal += g_AnimationSpeed * Time.deltaTime;
				if(l_LerpVal >=1)
				{
					g_SelectedObj.transform.localScale = Vector3.one;
					g_IsAreaObjActive = true;
					g_IsAnimationActive = false;
					g_NextArea = E_Area.None;
				}
			}
		}

	IEnumerator e_ScaleDownAnimation()
	{
		float l_LerpVal = 0;
		g_IsAnimationActive = true;
		while(l_LerpVal<1)
		{
			yield return null;
			g_SelectedObj.transform.localScale = Vector3.Lerp (Vector3.one, Vector3.zero, l_LerpVal);
			l_LerpVal += g_AnimationSpeed * Time.deltaTime;
			if(l_LerpVal >=1)
			{
				g_SelectedObj.transform.localScale = Vector3.zero;
				g_IsAreaObjActive = false;
				g_IsAnimationActive = false;
				if(g_NextArea != E_Area.None)
				{
					f_ActivateArea (g_NextArea);
				}
			}
		}
	}

	public void f_ResetAllMaterial()
	{
		g_LivingFurniture.color = g_ResetColor;
		g_FurnitureCollection.color 	= g_ResetColor;
		g_ShopOnline.color 				= g_ResetColor;
		g_HomeOffice.color 				= g_ResetColor;
		g_Cushions.color 				= g_ResetColor;
		g_Garden_Balcony.color 			= g_ResetColor;
		g_BarCabin.color 				= g_ResetColor;
		g_CashTills.color 				= g_ResetColor;
		g_CustomerServices.color 		= g_ResetColor;
		g_Lighting.color 				= g_ResetColor;
		g_Gallery.color 				= g_ResetColor;
		g_FloorCovering.color 			= g_ResetColor;
		g_CandleShop.color 				= g_ResetColor;
		g_HomeDecor_Gifts.color 		= g_ResetColor;
		g_Curtains.color 				= g_ResetColor;
		g_Crockery.color 				= g_ResetColor;
		g_Glassware.color 				= g_ResetColor;
		g_KitchenAccessories.color 		= g_ResetColor;
		g_DiningFurniture.color 		= g_ResetColor;
		g_Dreamroom.color 				= g_ResetColor;
		g_BedroomFurniture.color 		= g_ResetColor;
		g_Bedding.color 				= g_ResetColor;
		g_Pillow.color 					= g_ResetColor;
		g_MattressShope.color 			= g_ResetColor;
		g_Wardrobes.color 				= g_ResetColor;
		g_Bath.color 					= g_ResetColor;
		g_Laundry.color 				= g_ResetColor;
		g_Headboards.color 				= g_ResetColor;
		g_Prayer.color 					= g_ResetColor;
		g_KidsFurniture.color 			= g_ResetColor;
		g_Nursery.color 				= g_ResetColor;
		g_KidsMattress.color 			= g_ResetColor;
		g_KidsHousehold.color 			= g_ResetColor;
	}




	 IEnumerator HighlightObjAfterDelay(string l_ObjName)
	{
		yield return new WaitForSeconds (0.25f);
		C_StoreMapUIManager.Instance.g_GetMeThereBtn.SetActive (true);
		g_BlurPlane.SetActive (false);
		g_SimpleMap.SetActive (false);
		g_DetailMap.SetActive (true);
		f_HighlightSelectedArea (l_ObjName);
	}

	void f_HighlightSelectedArea(string l_ObjName)
	{
		f_ResetAllMaterial ();
		switch (l_ObjName) {
		case "LivingFurniture":
			//print ("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
			g_LivingFurniture.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "FurnitureCollection":
			g_FurnitureCollection.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "ShopOnline":
			g_ShopOnline.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "HomeOffice":
			g_HomeOffice.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Cushions":
			g_Cushions.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Garden_Balcony":
			g_Garden_Balcony.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "BarCabin":
			g_BarCabin.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "CashTills":
			g_CashTills.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "CustomerServices":
			g_CustomerServices.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Curtains":
			g_Curtains.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "HomeDecor_Gifts":
			g_HomeDecor_Gifts.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "CandleShop":
			g_CandleShop.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Lighting":
			g_Lighting.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Gallery":
			g_Gallery.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "FloorCovering":
			g_FloorCovering.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "DiningFurniture":
			g_DiningFurniture.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "KitchenAccessories":
			g_KitchenAccessories.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Glassware":
			g_Glassware.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Crockery":
			g_Crockery.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Dreamroom":
			g_Dreamroom.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "KidsHousehold":
			g_KidsHousehold.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "KidsMattress":
			g_KidsMattress.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Nursery":
			g_Nursery.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "KidsFurniture":
			g_KidsFurniture.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Prayer":
			g_Prayer.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Headboards":
			g_Headboards.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Laundry":
			g_Laundry.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Bath":
			g_Bath.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Wardrobes":
			g_Wardrobes.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "MattressShope":
			g_MattressShope.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Pillow":
			g_Pillow.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "Bedding":
			g_Bedding.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
		case "BedroomFurniture":
			g_BedroomFurniture.color = new Color (193.0f / 255, 216.0f / 255, 47.0f / 255);
			break;
                
                
		}
	}
    
     
    
}
