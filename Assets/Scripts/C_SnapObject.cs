﻿using UnityEngine;
using System.Collections;

public class C_SnapObject : MonoBehaviour {

	public GameObject g_SnappedObj;


	void OnTriggerEnter(Collider l_GameObject){

		if (g_SnappedObj == null) {
			g_SnappedObj = l_GameObject.gameObject;
		}
	}

	void OnTriggerStay(Collider l_GameObject){
		if (g_SnappedObj != null) {
			g_SnappedObj.transform.position = this.gameObject.transform.position;
		}
	}

	void OnTriggerExit(Collider l_GameObject){
		if (g_SnappedObj != null) {
			if (string.Compare (g_SnappedObj.transform.name, l_GameObject.name) == 0) {
				g_SnappedObj = null;
			}
		}
	}
}
