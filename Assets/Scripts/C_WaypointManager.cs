﻿using UnityEngine;
using System.Collections;

public class C_WaypointManager : MonoBehaviour {

	private static C_WaypointManager instance;
	public static C_WaypointManager Instance{
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<C_WaypointManager> ();

			return instance;
		}
	}

	public GameObject[] g_ActiveWaypointObjs;
	public GameObject[] g_WayPoints;

	public void f_SetWayPoints(string l_AreaName)
	{
		if (l_AreaName == "LivingFurniture") {
			//Debug.Log (l_AreaName);
			g_ActiveWaypointObjs = new GameObject[3];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [19];
		} else if (l_AreaName == "Garden_Balcony") {
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [21];
			g_ActiveWaypointObjs [3] = g_WayPoints [22];
		} else if(l_AreaName == "ShopOnline" || l_AreaName == "HomeOffice"){
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [21];
			g_ActiveWaypointObjs [3] = g_WayPoints [26];
		} else if(l_AreaName == "Cushions"){
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [12];
			g_ActiveWaypointObjs [3] = g_WayPoints [27];
		} else if (l_AreaName == "BarCabin") {
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [3];
			g_ActiveWaypointObjs [2] = g_WayPoints [12];
			g_ActiveWaypointObjs [3] = g_WayPoints [13];
		} else if (l_AreaName == "FurnitureCollection") {
			g_ActiveWaypointObjs = new GameObject[2];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [14];
		} else if (l_AreaName == "HomeDecor_Gifts") {
			g_ActiveWaypointObjs = new GameObject[2];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [1];
		} else if (l_AreaName == "FloorCovering" || l_AreaName == "Gallery") {
			g_ActiveWaypointObjs = new GameObject[3];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [18];
		} else if (l_AreaName == "Lighting") {
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [18];
			g_ActiveWaypointObjs [3] = g_WayPoints [17];
		} else if (l_AreaName == "Curtains") {
			g_ActiveWaypointObjs = new GameObject[2];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [12];
		} else if (l_AreaName == "Crockery" || l_AreaName == "KitchenAccessories" || l_AreaName == "Glassware" || l_AreaName == "DiningFurniture") {
			g_ActiveWaypointObjs = new GameObject[3];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [12];
			g_ActiveWaypointObjs [2] = g_WayPoints [16];
		} else if (l_AreaName == "Dreamroom") {
			g_ActiveWaypointObjs = new GameObject[2];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [3];
		} else if (l_AreaName == "Bedding" || l_AreaName == "BedroomFurniture") {
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [6];
			g_ActiveWaypointObjs [3] = g_WayPoints [7];
		} else if (l_AreaName == "Pillow" || l_AreaName == "Headboards"){
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [6];
			g_ActiveWaypointObjs [3] = g_WayPoints [28];
		} else if (l_AreaName == "MattressShope" || l_AreaName == "Wardrobes") {
			g_ActiveWaypointObjs = new GameObject[5];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [6];
			g_ActiveWaypointObjs [3] = g_WayPoints [7];
			g_ActiveWaypointObjs [4] = g_WayPoints [8];
		} else if (l_AreaName == "Bath") {
			g_ActiveWaypointObjs = new GameObject[6];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [6];
			g_ActiveWaypointObjs [3] = g_WayPoints [7];
			g_ActiveWaypointObjs [4] = g_WayPoints [8];
			g_ActiveWaypointObjs [5] = g_WayPoints [9];
		} else if (l_AreaName == "Laundry") {
			g_ActiveWaypointObjs = new GameObject[6];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [6];
			g_ActiveWaypointObjs [3] = g_WayPoints [7];
			g_ActiveWaypointObjs [4] = g_WayPoints [8];
			g_ActiveWaypointObjs [5] = g_WayPoints [29];
		} else if (l_AreaName == "KidsHousehold") {
			g_ActiveWaypointObjs = new GameObject[3];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [6];
		} else if (l_AreaName == "Nursery") {
			g_ActiveWaypointObjs = new GameObject[3];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [4];
		} else if (l_AreaName == "KidsFurniture") {
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [4];
			g_ActiveWaypointObjs [3] = g_WayPoints [31];
		} else if (l_AreaName == "KidsMattress") {
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [32];
			g_ActiveWaypointObjs [3] = g_WayPoints [30];
		} else if (l_AreaName == "CustomerServices") {
			g_ActiveWaypointObjs = new GameObject[5];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [12];
			g_ActiveWaypointObjs [2] = g_WayPoints [10];
			g_ActiveWaypointObjs [3] = g_WayPoints [23];
			g_ActiveWaypointObjs [4] = g_WayPoints [24];
		} else if (l_AreaName == "Prayer") {
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [12];
			g_ActiveWaypointObjs [2] = g_WayPoints [10];
			g_ActiveWaypointObjs [3] = g_WayPoints [11];
		} else if (l_AreaName == "CashTills") {
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [4];
			g_ActiveWaypointObjs [3] = g_WayPoints [5];
		} else if (l_AreaName == "CandleShop") {
			g_ActiveWaypointObjs = new GameObject[4];
			g_ActiveWaypointObjs [0] = g_WayPoints [0];
			g_ActiveWaypointObjs [1] = g_WayPoints [2];
			g_ActiveWaypointObjs [2] = g_WayPoints [4];
			g_ActiveWaypointObjs [3] = g_WayPoints [25];
		}
	}
}
