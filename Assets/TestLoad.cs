﻿using UnityEngine;
using System.Collections;

public class TestLoad : MonoBehaviour {

	public C_IdleTimeReseter c_IdleTimeReseter;
	public C_StellarBed_Manager c_StellarBed_Manager;

	//TestingPurpose

	// Use this for initialization
	void OnEnable () {
		c_IdleTimeReseter.f_ResetIdleTime ();
		c_StellarBed_Manager.f_ActivateSelectBedProducts (2);
		c_StellarBed_Manager.f_DisablePreviewBtns (0);
	}

}
